﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using FunQuizCore.Model;
using FunQuizWeb.Class;
using FunQuizWeb.Class.Http;
using FunQuizWeb.Language;
using FunQuizWeb.Models;

namespace FunQuizWeb.Controllers
{
    public class AccountController : BaseController
    {
        private static log4net.ILog Log { get; set; } = log4net.LogManager.GetLogger(typeof(AccountController));

        WsEquizz.WsEQuizClient wsClient;

        public AccountController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }

        // GET: Account
        public ActionResult Index()
        {
            if (!CheckAuthToken())
            {
                return Redirect("/Account/Login");
            }

            // load the profile and services
            ReloadProfileInfo(wsClient);
            ReloadSubInfo(wsClient);

            AccountUser profile = Session["profile"] as AccountUser;
            List<String> services = Session["packetReg"] as List<String>;

            AccountModel model = new AccountModel();
            model.profile = profile;
            model.services = services;
            return View("Index", model);
        }
        public ActionResult Login()
        {
            return View("Login");
        }
        public ActionResult Register()
        {
            return View("Register");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return Redirect("/Home");
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoginAction(String phonenumber, string password)
        {
            string msisdn = validateMsisdn(phonenumber);
            if (msisdn == "")
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.INVALID_MSISDN,
                    error_content = Error.invalidMsisdn,
                });
            }
            //// login by mps
            //AccountRequest request = new AccountRequest();
            //request.users = msisdn;
            //request.msisdn = msisdn;
            //request.pass = password;
            //request.command = "LOGIN";
            //request.channel = "WEB";

            //String rs = SendPost(request, Session.SessionID, UtilsController.WsType.UserLogin);
            //AppResponse res = new AppResponse(rs);
            //if (res.status == UtilsController.Constant.SUCCESS)
            //{
            //    // create new auth
            //    CreateAuthToken();

            //    // login success --> store session
            //    Session["msisdn"] = msisdn;

            //    // get profile
            //    ReloadProfileInfo();

            //    // get sub
            //    ReloadSubInfo();
            //}
            //return Json(new
            //{
            //    error_code = res.status,
            //    error_content = UtilsController.GetErrorCodeCharging(res.status)
            //});

            WsEquizz.response res = wsClient.wsLogin(wsUser, wsPassword, msisdn, password, ServiceId);

            if (res.errorCode == UtilsController.Constant.SUCCESS)
            {
                // create new auth
                CreateAuthToken();

                // login success --> store session
                Session["msisdn"] = msisdn;

                // get profile
                ReloadProfileInfo(wsClient);

                // get sub
                ReloadSubInfo(wsClient);

                return Json(new
                {
                    error_code = "0",
                    error_content = "Register successful"
                });
            }
            else
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult RegisterAction(String captcha, String phonenumber, string email, string packettype, string activateCode)
        {
            string msisdn;
            if (activateCode == null)
            {
                if (captcha == null && phonenumber == null)
                {
                    if (!CheckAuthToken())
                    {
                        return Json(new
                        {
                            error_code = UtilsController.Constant.AUTHEN_FAIL,
                            error_content = Error.mustLogin,
                            href = "/Account/Login"
                        });
                    }
                    msisdn = Session["msisdn"] as String;
                    Session["isLogin"] = "true";
                }
                else
                {
                    Session["isLogin"] = "false";
                    msisdn = validateMsisdn(phonenumber);
                    if (msisdn == "")
                    {
                        return Json(new
                        {
                            error_code = UtilsController.Constant.INVALID_MSISDN,
                            error_content = Error.invalidMsisdn
                        });
                    }
                    if (captcha == "" || captcha == null)
                    {
                        return Json(new
                        {
                            error_code = UtilsController.Constant.AUTHEN_FAIL,
                            error_content = Lang.invalidCaptcha,
                        });
                    }
                    //else if (!ReCaptcha.Validate(captcha))
                    //{
                    //    return Json(new
                    //    {
                    //        error_code = UtilsController.Constant.CAPTCHA_INVALID,
                    //        error_content = "Invalid captcha"
                    //    });
                    //}
                }


                Session["msisdn"] = msisdn;
                Session["packet-type"] = packettype;

                WsEquizz.response res = wsClient.wsRegisterSubOtp(wsUser, wsPassword, msisdn, packettype, ServiceId);

                // save charging
                Charging charging = new Charging(res);
                Session["charging"] = charging;

                String isLogin = Session["isLogin"] as String;
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode),
                });
            }
            else if (activateCode != null)
            {
                String isLogin = Session["isLogin"] as String;
                msisdn = Session["msisdn"] as String;
                string packetType = Session["packet-type"] as String;
                Charging charging = Session["charging"] as Charging;

                WsEquizz.response res = wsClient.wsRegisterSubConfirm(wsUser, wsPassword, msisdn, packetType, charging.resultCode, activateCode, ServiceId);

                if (res.errorCode == UtilsController.Constant.SUCCESS)
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(res.errorCode),
                        href = isLogin == "true" ? "/Home" : "/Account/Login",
                    });
                }
                else if (res.errorCode == UtilsController.Constant.SUCCESS_FOR_THE_FIRST_TIME_REGISTER)
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = Lang.freeFirstTime,
                        href = isLogin == "true" ? "/Home" : "/Account/Login",
                    });
                }
                else
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                    });
                }
            }
            else
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = "Exception"
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult CreateFreeAction(String captcha, String phonenumber, String email)
        {
            String msisdn = validateMsisdn(phonenumber);
            if (msisdn == "")
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.INVALID_MSISDN,
                    error_content = Error.invalidMsisdn,
                });
            }

            WsEquizz.response res = wsClient.wsCreateFree(wsUser, wsPassword, msisdn, UtilsController.Constant.CHANNEL, BaseController.ServiceId);

            if (res.errorCode == UtilsController.Constant.SUCCESS)
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
            else
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }

        }

        [ValidateAntiForgeryToken]
        public JsonResult BuyCodeAction(String captcha)
        {
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            if (captcha == "" || captcha == null)
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Lang.invalidCaptcha,
                });
            }
            String msisdn = Session["msisdn"] as String;
            if (msisdn != null)
            {
                Session["msisdn"] = msisdn;
                Session["packet-type"] = UtilsController.Constant.CHARGE1;

                WsEquizz.response res = wsClient.wsBuyCode(wsUser, wsPassword, msisdn, UtilsController.Constant.CHARGE1, ServiceId);

                // save res
                Session["buy-code-request"] = res.resultCode;

                return Json(new
                {
                    error_code = UtilsController.Constant.WATING_OTP,
                    error_content = Lang.buySuccess,
                });

                if (res.errorCode == UtilsController.Constant.WATING_OTP)
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = Lang.buySuccess,
                    });
                }
                else
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(res.errorCode),
                    });

            }
            else
            {
                Log.Error("Can not complete invalid msisdn " + msisdn);
                return Json(new
                {
                    error_code = UtilsController.Constant.HAVE_A_PROBLEM,
                    error_content = Lang.weHaveAProblem,
                    href = "/Account/Login"
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult BuyCodeConfirmAction(String otp)
        {
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            String msisdn = Session["msisdn"] as String;
            if (msisdn != null)
            {
                Session["msisdn"] = msisdn;
                Session["packet-type"] = UtilsController.Constant.CHARGE1;

                String req = Session["buy-code-request"] as String;

                WsEquizz.response res = wsClient.wsBuyCodeConfirm(wsUser, wsPassword, msisdn, UtilsController.Constant.CHARGE1, req, otp, ServiceId);

                //return Json(new
                //{
                //    error_code = UtilsController.Constant.SUCCESS,
                //    error_content = Lang.buySuccess,
                //});

                //if (res.errorCode == UtilsController.Constant.SUCCESS)
                //{
                //    return Json(new
                //    {
                //        error_code = res.errorCode,
                //        error_content = Lang.buySuccess,
                //    });
                //}
                //else
                //{
                //    return Json(new
                //    {
                //        error_code = res.errorCode,
                //        error_content = UtilsController.GetErrorCodeCharging(res.status),
                //    });
                //}

                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode),
                });
            }
            else
            {
                Log.Error("Can not complete invalid msisdn " + msisdn);
                return Json(new
                {
                    error_code = UtilsController.Constant.HAVE_A_PROBLEM,
                    error_content = Lang.weHaveAProblem,
                    href = "/Account/Login"
                });
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdateAction(String fullname, String birthday, HttpPostedFileBase image)
        {
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            AccountUser profile = Session["profile"] as AccountUser;
            String msisdn = Session["msisdn"] as String;
            string fullnameUpdate = fullname != null && fullname != "" ? fullname : profile.fullname;
            string birthdayConvert = birthday != null && birthday != "" ? birthday : profile.birthday;


            DateTime oDateBirthday = DateTime.Parse(birthdayConvert);
            string birthdayUpdate = oDateBirthday.Month + "/" + oDateBirthday.Day + "/" + oDateBirthday.Year;

            String imageUpdate;

            // check image upload
            if (image != null && (image.FileName.EndsWith(".png") || image.FileName.EndsWith(".jpg")))
            {
                String ranStr = RandomString(10, true);
                string pic = ranStr + "_" + Path.GetFileName(image.FileName);
                string path = Path.Combine(Server.MapPath(UtilsController.GetContentPath.AccountPath), pic);
                // file is uploaded
                image.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    image.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                imageUpdate = pic;
            }
            else
            {
                imageUpdate = profile.realPicture;
            }

            // update to server
            //ProfileRequest request = new ProfileRequest();
            //request.MSISDN = profile.msisdn;
            //request.users = profile.msisdn;
            //request.msisdn = profile.msisdn;
            //request.PICTURE = imageUpdate;
            //request.FULLNAME = fullnameUpdate;
            //request.BIRTHDAY = birthdayUpdate;
            //String rs = SendPost(request, Session.SessionID, UtilsController.WsType.UserUpdateProfile);
            //UserProfileResponse res = new UserProfileResponse(rs);

            WsEquizz.response res = wsClient.wsUpdateUserProfile(wsUser, wsPassword, msisdn, fullnameUpdate, imageUpdate, birthdayUpdate, ServiceId);
            if (res.errorCode == UtilsController.Constant.SUCCESS)
            {
                // update success --> store session
                profile.birthday = birthdayUpdate;
                profile.fullname = fullnameUpdate;
                profile.picture = imageUpdate;
                Session["profile"] = profile;
                return Json(new
                {
                    error_code = UtilsController.Constant.SUCCESS,
                    error_content = Lang.updateSuccessful
                });
            }
            else
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.FAILURE,
                    error_content = Error.weHaveAProblem
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(String captcha, String phonenumber)
        {
            Log.Debug("GetPassword : " + phonenumber);
            if (captcha == "" || captcha == null)
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Lang.invalidCaptcha,
                });
            }
            String msisdn = validateMsisdn(phonenumber);
            if (msisdn == "")
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.INVALID_MSISDN,
                    error_content = Error.invalidMsisdn,
                });
            }

            WsEquizz.response res = wsClient.wsResetPass(wsUser, wsPassword, msisdn, ServiceId);

            if (res.errorCode == UtilsController.Constant.SUCCESS)
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
            else
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult ChangePassword(String captcha, String newPassword, String oldPassword)
        {
            Log.Debug("ChangePassword ");
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            if (captcha == "" || captcha == null)
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Lang.invalidCaptcha,
                });
            }
            string msisdn = Session["msisdn"] as String;

            WsEquizz.response res = wsClient.wsChangePass(wsUser, wsPassword, msisdn, oldPassword, newPassword, ServiceId);

            if (res.errorCode == UtilsController.Constant.MPS_SUCCESS)
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
            else
            {
                return Json(new
                {
                    error_code = res.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult InviteAction(String captcha, string phonenumber)
        {
            Log.Debug("InviteAction : " + phonenumber);
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            if (captcha == "" || captcha == null)
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Lang.invalidCaptcha,
                });
            }
            String friendMsisdn = validateMsisdn(phonenumber);
            if (friendMsisdn != "")
            {
                Log.Debug("InviteAction ");
                String msisdn = Session["msisdn"] as String;

                WsEquizz.response res = wsClient.wsInviteFriends(wsUser, wsPassword, msisdn, friendMsisdn, "WEB", ServiceId);

                if (res.errorCode == UtilsController.Constant.MPS_SUCCESS)
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                    });
                }
                else
                {
                    return Json(new
                    {
                        error_code = res.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                    });
                }
            }
            else
            {
                Log.Error("Wrong friendMsisdn: " + friendMsisdn);
                return Json(new
                {
                    error_code = UtilsController.Constant.INVALID_MSISDN,
                    error_content = Error.invalidMsisdn,
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult ExchangeAction(String captcha, string package)
        {
            Log.Debug("ExchangeAction : " + package);
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            if (captcha == "" || captcha == null)
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Lang.invalidCaptcha,
                });
            }
            String msisnd = Session["msisdn"] as String;
            AccountUser profile = Session["profile"] as AccountUser;

            //return Json(new
            //{
            //    error_code = UtilsController.Constant.FAILURE,
            //    error_content = Error.SystemErr,
            //});

            if (msisnd != "" && package != "")
            {
                // check enough points
                String point = UtilsController.GetPointByPackage(package);
                if (int.Parse(profile.point) < int.Parse(point))
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.MPS_NOT_ENOUGH_POINT,
                        error_content = Lang.notEnoughPoint
                    });
                }
                else
                {
                    String msisdn = Session["msisdn"] as String;
                    String fee = UtilsController.GetFeeByPackage(package);

                    WsEquizz.response res = wsClient.wsUserExchangeMoney(wsUser, wsPassword, msisdn, package, point, fee, ServiceId);

                    if (res.errorCode == UtilsController.Constant.MPS_SUCCESS)
                    {
                        return Json(new
                        {
                            error_code = res.errorCode,
                            error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            error_code = res.errorCode,
                            error_content = UtilsController.GetErrorCodeCharging(res.errorCode)
                        });
                    }
                }
            }
            else
            {
                Log.Error("Wrong package: " + package);
                return Json(new
                {
                    error_code = UtilsController.Constant.FAILURE,
                    error_content = Error.SystemErr,
                });
            }
        }
    }
}