﻿using FunQuizWeb.Class;
using FunQuizWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunQuizCore.Model;
using FunQuizWeb.Language;
using System.Configuration;

namespace FunQuizWeb.Controllers
{
    public class GameController : BaseController
    {
        WsEquizz.WsEQuizClient wsClient;

        public GameController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }

        // GET: Game
        public ActionResult Index(String gameId, String questionNow, String questionId, String answerId, String finish)
        {
            if (!CheckAuthToken())
            {
                return Redirect("/Account/Login");
            }
            if (gameId != null)
            {
                // save gameId
                // get game by id
                Games games = Session["games-now"] as Games;
                Games categories = Session["categories"] as Games;

                Game game = games.games.Find(x => x.id == gameId);
                // get all games related to home category

                GamesModel model = new GamesModel();
                model.questions = new Games();
                model.questions.games = new List<Game>();
                model.game = game;
                model.categories = categories;
                model.games = games;
                model.moreGames = games;

                // get question for the fisrt time and update play time to server
                if (questionNow == null || questionNow == "0")
                {
                    //GetRequest request = new GetRequest();
                    //request.parentId = gameId;
                    //String rs = SendPost(request, Session.SessionID, UtilsController.WsType.GetDataByParentId);
                    //AppResponse res = new AppResponse(rs);

                    WsEquizz.response res = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", gameId, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);


                    if (res.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        Games questionsGet = new Games(res.listGames);
                        questionsGet.games.Sort(delegate (Game x, Game y)
                        {
                            if (x.id == null && y.id == null) return 0;
                            else if (x.id == null) return -1;
                            else if (y.id == null) return 1;
                            else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                        });
                        model.questions = questionsGet;
                        Session["questions"] = questionsGet;
                    }
                    if (finish != "1")
                    {
                        Session["answers-of-user"] = new List<Answer>();
                        Session["finished"] = "false";
                    }
                }

                Games questions = Session["questions"] as Games;
                model.questions = questions;

                // get answer by answerId
                //Games answerList = Session["answer-now"] as Games;
                //answerList.games.Find(x => x.id == answerId);

                if (questionNow != null && int.Parse(questionNow) >= questions.games.Count())
                {
                    // Finish ???
                    // send to server to get the result
                    // get answer to get type of question
                    Games answersTake = Session["answer-now"] as Games;
                    Game answer = answersTake.games.Find(x => x.id == answerId);

                    List<Answer> listAnswer = Session["answers-of-user"] as List<Answer>;
                    Answer answerUser = new Answer();
                    answerUser.answer = answer;
                    answerUser.rightAnswer = answersTake.games.Find(x => x.typeOf == UtilsController.Constant.ANSWER_RIGHT);
                    if (listAnswer == null)
                    {
                        listAnswer = new List<Answer>();
                        listAnswer.Add(answerUser);
                    }
                    else
                    {
                        // update list answer
                        listAnswer.RemoveAll(x => x.answer.id == answerId);
                        listAnswer.Add(answerUser);
                    }
                    Session["answers-of-user"] = listAnswer;
                    if (answer.typeOf == UtilsController.Constant.ANSWER_POPUP)
                    {
                        AnswerPopupModel modelPopup = new AnswerPopupModel();
                        modelPopup.answer = answer;
                        modelPopup.questions = questions;
                        return PartialView("AnswerPopup", modelPopup);
                    }
                    String finished = Session["finished"] as String;

                    ResultPopupModel modelResult = new ResultPopupModel();
                    modelResult.listAnswers = listAnswer;
                    modelResult.questions = questions;
                    // user have already taken the result
                    if (finished == "true")
                    {
                        modelResult.finished = true;
                    }
                    else
                    {
                        modelResult.finished = false;
                        Session["finished"] = "true";
                    }
                    return PartialView("ResultPopup", modelResult);

                }
                else
                {
                    //save answer to Session
                    List<Answer> listAnswer = Session["answers-of-user"] as List<Answer>;
                    if (questionId != null && answerId != "" && answerId != null)
                    {
                        Games answersTake = Session["answer-now"] as Games;
                        Game answer = answersTake.games.Find(x => x.id == answerId);
                        Answer answerUser = new Answer();
                        answerUser.answer = answer;
                        answerUser.rightAnswer = answersTake.games.Find(x => x.typeOf == UtilsController.Constant.ANSWER_RIGHT);

                        if (listAnswer == null || listAnswer.Count == 0)
                        {
                            listAnswer = new List<Answer>();
                            listAnswer.Add(answerUser);
                        }
                        else
                        {
                            // update list answer
                            listAnswer.RemoveAll(x => x.answer.id == answerId);
                            listAnswer.Add(answerUser);
                        }
                    }
                    Session["answers-of-user"] = listAnswer;

                    // questionNow == null => the first question
                    model.question = model.questions.games[questionNow != null ? int.Parse(questionNow) : 0];

                    // get answer for this question
                    model.answers = new Games();
                    model.answers.games = new List<Game>();
                    //get all questions and answer of this content

                    Games answers = new Games();
                    //GetRequest requestQuestion = new GetRequest();
                    //requestQuestion.parentId = model.question.id;
                    //String rsQuestion = SendPost(requestQuestion, Session.SessionID, UtilsController.WsType.GetDataByParentId);
                    //AppResponse resQuestion = new AppResponse(rsQuestion);

                    WsEquizz.response res = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", model.question.id, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

                    if (res.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        answers = new Games(res.listGames);
                        answers.games.ForEach(q => model.answers.games.Add(q));
                    }
                    model.questionNow = questionNow != null ? questionNow : "0";
                    Session["answer-now"] = model.answers;
                    return View("Index", model);
                }
            }
            else
            {
                return Redirect("/Home");
            }
        }

        public JsonResult CheckToPlay()
        {
            if (!CheckAuthToken())
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.AUTHEN_FAIL,
                    error_content = Error.mustLogin,
                    href = "/Account/Login"
                });
            }
            Session["answers-of-user"] = null;
            String msisdn = Session["msisdn"] as String;
            // update playing to server
            //AccountRequest updateRequest = new AccountRequest();
            //updateRequest.msisdn = msisdn;
            //updateRequest.users = msisdn;
            //String rsUpdate = SendPost(updateRequest, Session.SessionID, UtilsController.WsType.UserUpdatePlaying);
            //AppResponse resUpdate = new AppResponse(rsUpdate);

            WsEquizz.response resUpdate = wsClient.wsUpdateUserPlaying(wsUser, wsPassword, msisdn, ServiceId);

            if (resUpdate.errorCode == UtilsController.Constant.SUCCESS)
            {
                // check is out of turn to play 
                if (resUpdate.status == UtilsController.Constant.OUT_OF_TURN)
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.USER_OUT_OF_TURN,
                        error_content = Lang.outOfTurn
                    });
                }
                else
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.SUCCESS,
                    });
                }
            }
            else
            {
                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = Error.weHaveAProblem
                });
            }
        }
    }
}