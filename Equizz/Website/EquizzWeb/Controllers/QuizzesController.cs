﻿using FunQuizWeb.Class;
using FunQuizWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FunQuizWeb.Controllers
{
    public class QuizzesController : BaseController
    {
        WsEquizz.WsEQuizClient wsClient;

        public QuizzesController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }
        // GET: Quizzes
        public ActionResult Index()
        {
            if (!CheckAuthToken())
            {
                return Redirect("/Account/Login");
            }
            Games categories = new Games();
            categories.games = new List<Game>();
            Games types = new Games();
            types.games = new List<Game>();
            Games games = new Games();
            games.games = new List<Game>();

            QuizzesModel model = new QuizzesModel();
            model.games = new Games();
            model.games.games = new List<Game>();
            model.categories = new Games();
            model.categories.games = new List<Game>();

            // get all cateories
            categories = Session["categories"] as Games;
            model.categories = categories;
            Session["categories"] = categories;

            Game category_quizzes = categories.games.Find(x => x.id == UtilsController.Constant.QUIZZES_CATEGORY);
            Game category_home = categories.games.Find(x => x.id == UtilsController.Constant.HOME_CATEGORY);

            Session["category-now"] = category_quizzes != null ? category_quizzes : category_home;

            // get all type of games related to home category
            //GetRequest request2 = new GetRequest();
            //request2.parentId = categories.games[int.Parse(UtilsController.Constant.QUIZZES_CATEGORY) - 1].id;
            //String rsType = SendPost(request2, Session.SessionID, UtilsController.WsType.GetDataByParentId);
            //AppResponse resType = new AppResponse(rsType);

            WsEquizz.response resType = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", UtilsController.Constant.QUIZZES_CATEGORY, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

            if (resType.errorCode == UtilsController.Constant.SUCCESS)
            {
                Games typesGet = new Games(resType.listGames);
                typesGet.games.ForEach(x => types.games.Add(x));
                types.games.Sort(delegate (Game x, Game y)
                {
                    if (x.id == null && y.id == null) return 0;
                    else if (x.id == null) return -1;
                    else if (y.id == null) return 1;
                    else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                });


                // get some games for some types in the top of the list
                for (int i = 0; i < types.games.Count(); i++)
                {
                    //GetRequest request3 = new GetRequest();
                    //request3.parentId = types.games[i].id;
                    //String rsGames = SendPost(request3, Session.SessionID, UtilsController.WsType.GetDataByParentId);
                    //AppResponse resGames = new AppResponse(rsGames);

                    WsEquizz.response resGames = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", types.games[i].id, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

                    if (resGames.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        Games gamesGet = new Games(resGames.listGames);
                        gamesGet.games.ForEach(x => games.games.Add(x));
                        Session["games-now"] = games;
                    }
                }
            }
            model.types = types;
            Session["types"] = types;
            model.games = games;

            return View("Index", model);
        }
    }
}