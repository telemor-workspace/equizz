﻿using FunQuizWeb.Language;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FunQuizWeb.Controllers
{
    public class UtilsController : Controller
    {
        // GET: Utils
        public static String GetWsClient(String type)
        {
            return ConfigurationManager.AppSettings[type];
        }

        public class WsType
        {
            public const String GetDataByParentId = "GetDataByParentId";
            public const String GetDataById = "GetDataById";
            public const String GetNewestGames = "GetNewestGames";
            public const String GetMiniGames = "GetMiniGames";
            public const String UserCreateAccount = "UserCreateAccount";
            public const String UserLogin = "UserLogin";
            public const String UserGetProfile = "UserGetProfile";
            public const String UserGetListService = "UserGetListService";
            public const String GetRanking = "GetRanking";
            public const String UserUpdatePlaying = "UserUpdatePlaying";
            public const String UserUpdateProfile = "UserUpdateProfile";


            //public const String UsersLogin = "usersRegister";
            //public const String UsersCreateFree = "usersRegister";
            //public const String UsersGetPassord = "usersRegister";
            //public const String UsersChangePass = "usersRegister";
            //public const String UsersUpdateProfile = "usersUpdateProfile";

            //public const String UsersGetProfile = "usersGetProfile";
            //public const String SubGetListsubServiceCode = "subGetListsubServiceCode";
            //public const String UsersMPSRequest = "usersMPSRequest";
            //public const String NewsUpdateRead = "newsUpdateRead";
            //public const String NewsGetById = "newsGetById";
            //public const String NewsGetTop = "newsGetTop";
            //public const String NewsBuy = "newsBuy";

            //public const String UsersGetProvice = "usersGetProvice";
            //public const String UsersGetTopic = "usersGetTopic";
            //public const String NewsSearchProTopDate = "newsSearchProTopDate";
            //public const String NewsGetByUsers = "newsGetByUsers";
            //public const String NewsSendMt = "newsSendMt";



            // Admin
            public const String NewsUpdate = "newsUpdate";
            public const String NewsInsert = "newsInsert";
            public const String UsersAdminLogin = "usersAdminLogin";

            public const String SubDomain = "SubDomain";
            public const String GoogleCaptcha = "GoogleCaptcha";


        }

        public class Constant
        {
            // from Procedure server
            public const String WAITING_OTP = "100";
            public const String USER_EXISTED = "2";
            public const String NO_DATA = "1";
            public const String SUCCESS = "0";
            public const String INVALID_MSISDN = "-1";
            public const String EXCEPTION = "-2";
            public const String AUTHEN_FAIL = "-10";
            public const String USER_NOT_REGISTER = "-16";
            public const String USER_REGISTED = "700";
            public const String USER_LOGIN_FAIL = "-17";
            public const String USER_GET_PROFILE_FAIL = "-18";
            public const String USER_GET_LIST_SERVICE_FAIL = "-19";
            public const String USER_GET_RANKING_FAIL = "-20";
            public const String USER_OUT_OF_TURN = "-21";
            public const String WATING_OTP = "100";

            public const String ALREADY_ANSWERED = "105";

            // from MPS server
            public const String MPS_SUCCESS = "0";
            public const String MPS_FAILURE = "1";
            public const String SUCCESS_FOR_THE_FIRST_TIME_REGISTER = "2";
            public const String MPS_UPDATE_PASSWORD_FAILURE = "-101";

            public const String MPS_NOT_REGISTERED = "100";
            public const String MPS_QUERY_ERROR = "200";
            public const String MPS_NOMORE_SPIN = "300";
            public const String MPS_TOPUP_ERROR = "400";
            public const String MPS_AUTHENTICATE_FAIL = "500";
            public const String MPS_INVALID_PINCODE = "600";
            public const String MPS_ACCOUNT_EXISTED = "700";
            public const String MPS_MSISDN_EXISTED = "800";
            public const String MPS_ACCOUNT_ACTIVATED = "900";
            public const String MPS_ACCOUNT_NOT_EXISTED = "1000";
            public const String MPS_ACCOUNT_IS_LOCKED = "1100";
            public const String MPS_ACCOUNT_NOT_ACTIVATED = "1200";
            public const String MPS_ACCOUNT_NOT_REGISTERED = "1300";
            public const String MPS_WRONG_REGISTER_CODE = "1400";
            public const String MPS_NOT_ENOUGH_MONEY = "1500";
            public const String MPS_MSISDN_BLOCKED = "1600";
            public const String MPS_DISABLED_AUTO_EXTEND = "1700";
            public const String MPS_ENABLED_AUTO_EXTEND = "1800";
            public const String MPS_CHARGE_ERROR = "1900";
            public const String MPS_ALREADY_REGISTER = "2000";
            public const String MPS_NOT_ENOUGH_POINT = "2100";
            public const String OVER_LIMIT_EXCHANGE = "2101";
            public const String MPS_ALREADY_INVITED = "2200";
            public const String MPS_OVER_MAX_POINT_CHANGE = "2300";

            // code
            public const String FAILURE = "-1";
            public const String CAPTCHA_INVALID = "-3";


            // service constant
            public const String VOCABULARY = "VOCABULARY";
            public const String GRAMMAR = "GRAMMAR";
            public const String LISTEN = "LISTEN";

            public const String PARENT_ID = "-1";

            // type
            public const String DELETE = "0";
            public const String EDITING = "1";
            public const String ADDING = "2";

            public const String LEVEL_ONE = "1";
            public const String LEVEL_TWO = "2";
            public const String LEVEL_THREE = "3";
            public const String LEVEL_FOUR = "4";
            public const String LEVEL_FIVE = "5";

            public const String GET_CONTENT = "1";
            public const String NOT_GET_CONTENT = "0";

            public const String CREATE_FREE = "CREATE_FREE";
            public const String BUY_CODE = "BUY_CODE";
            public const String ADD_MONEY = "ADD_MONEY";
            public const String REGISTER_DAILY_5 = "REGISTER_DAILY_5";
            public const String REGISTER_DAILY_3 = "REGISTER_DAILY_3";
            public const String REGISTER_DAILY_2 = "REGISTER_DAILY_2";

            public const String FUNNY_QUIZZ10 = "EQUIZZ_10_PACKAGE";
            public const String FUNNY_QUIZZ15 = "EQUIZZ_15_PACKAGE";
            //public const String FUNNY_QUIZZ5 = "FUNNY_QUIZZ5";

            public const String ADD2 = "ADDBALANCE_2";
            public const String ADD4 = "ADDBALANCE_4";
            public const String ADD10 = "ADDBALANCE_10";
            public const String ADD50 = "ADDBALANCE_50";
            public const String ADD1000 = "ADDBALANCE_1000";
            public const String ADD1500 = "ADDBALANCE_1500";
            public const String CHARGE1 = "charge";


            public const String CHANNEL = "WEB";


            public const String REGISTER_DAILY_5_CONST = "5";
            public const String REGISTER_DAILY_3_CONST = "3";
            public const String REGISTER_DAILY_2_CONST = "2";

            public const String USER_NOT_LOGIN = "-1";
            public const String HAVE_A_PROBLEM = "-20";


            public const String RESULT_SEARCH_NULL = "0";
            public const String RESULT_SEARCH = "1";

            public const String LANGUAGE_GLOBAL = "0";
            public const String LANGUAGE_LOCAL = "1";
            public const String UNKNOWN_NAME = "-1";
            public const String MIN_ROWS_ON_PAGE = "5";
            public const String NORMAL_ROWS_ON_PAGE = "10";
            public const String MAX_ROWS_ON_PAGE = "20";


            public const String ADMIN_TYPE = "1";
            public const String USER_TYPE = "0";

            // status
            public const String DRAFT = "0";
            public const String PUBLISH = "1";

            // isPlayed
            public const String PLAYED = "1";
            public const String NOT_PLAYED = "0";

            // typeOf
            public const String NORMAL = "0";
            public const String QUESTION = "1";
            // lower level of question
            public const String ANSWER = "2";
            public const String ANSWER_RIGHT = "3";
            public const String ANSWER_POPUP = "4";


            // category type
            public const String HOME_CATEGORY = "1";
            public const String QUIZZES_CATEGORY = "2";
            public const String TESTS_CATEGORY = "3";
            public const String WINS_CATEGORY = "4";
            public const String MINIGAMES_CATEGORY = "5";

            public const String HOME_CATEGORY_PATH = "Home";
            public const String QUIZZES_CATEGORY_PATH = "Quizzes";
            public const String TESTS_CATEGORY_PATH = "Tests";
            public const String WINS_CATEGORY_PATH = "Wins";
            public const String MINIGAMES_CATEGORY_PATH = "Minigames";

            // can play?
            public const String OUT_OF_TURN = "0";
            public const String IN_TURN = "1";

        }
        public static String GetCategoryPath(String id)
        {
            switch (id)
            {
                case UtilsController.Constant.HOME_CATEGORY:
                    return UtilsController.Constant.HOME_CATEGORY_PATH;
                case UtilsController.Constant.QUIZZES_CATEGORY:
                    return UtilsController.Constant.QUIZZES_CATEGORY_PATH;
                case UtilsController.Constant.TESTS_CATEGORY:
                    return UtilsController.Constant.TESTS_CATEGORY_PATH;
                case UtilsController.Constant.WINS_CATEGORY:
                    return UtilsController.Constant.WINS_CATEGORY_PATH;
                case UtilsController.Constant.MINIGAMES_CATEGORY:
                    return UtilsController.Constant.MINIGAMES_CATEGORY_PATH;
                default:
                    return UtilsController.Constant.HOME_CATEGORY_PATH;
            }
        }

        public static string ConvertContentType(String type)
        {
            switch (type)
            {
                case "1": return "Text type";
                case "2": return "Audio link type";
                case "3": return "Video link type";
                case "4": return "Audio file type";
                case "5": return "Video file type";
                case "6": return "Picture file type";
                default: return type + " UNKNOWN CODE";
            }
        }

        public static string ConvertStatus(String type)
        {
            switch (type)
            {
                case "0": return "Inactive";
                case "1": return "Active";
                default: return "Unknown";
            }
        }

        public static string ConvertTypeOf(String type)
        {
            switch (type)
            {
                case "0": return "Game";
                case "1": return "Question";
                case "2": return "Answer";
                case "3": return "Answer right";
                case "4": return "Answer popup";
                default: return "Unknown";
            }
        }

        public static string ConvertIsPlayed(String type)
        {
            switch (type)
            {
                case "0": return "Not played";
                case "1": return "Played";
                default: return "Unknown";
            }
        }


        public class GetContentPath
        {
            public const String AccountPath = "../Content/assets/images/";
            public const String GamesPath = "../Content/assets/images/games/";
        }

        public class GetContentType
        {
            //1=text,2=link aidio,3=link video,4=file audio,5=file viedeo,6=file picture
            public const String TEXT_TYPE = "1";
            public const String AUDIO_LINK = "2";
            public const String VIDEO_LINK = "3";
            public const String AUDIO_FILE = "4";
            public const String VIDEO_FILE = "5";
            public const String PICTURE_FILE = "6";
        }

        //public static String GetPacketType(string type)
        //{
        //    switch (type)
        //    {
        //        case (UtilsController.Constant.REGISTER_DAILY_2_CONST):
        //            return UtilsController.Constant.REGISTER_DAILY_2;
        //        case (UtilsController.Constant.REGISTER_DAILY_3_CONST):
        //            return UtilsController.Constant.REGISTER_DAILY_3;
        //        case (UtilsController.Constant.REGISTER_DAILY_5_CONST):
        //            return UtilsController.Constant.REGISTER_DAILY_5;
        //        default:
        //            return "Unknown";
        //    }
        //}

        public static String GetFeeByPackage(String package)
        {
            switch (package)
            {
                case (UtilsController.Constant.CHARGE1):
                    return "1";
                case (UtilsController.Constant.ADD2):
                    return "2";
                case (UtilsController.Constant.ADD4):
                    return "4";
                case (UtilsController.Constant.ADD10):
                    return "10";
                case (UtilsController.Constant.ADD50):
                    return "50";
                case (UtilsController.Constant.ADD1000):
                    return "1000";
                case (UtilsController.Constant.ADD1500):
                    return "1500";
                default:
                    return "Unknown";
            }
        }

        public static String GetPointByPackage(String package)
        {
            switch (package)
            {
                case (UtilsController.Constant.ADD2):
                    return "10";
                case (UtilsController.Constant.ADD4):
                    return "20";
                case (UtilsController.Constant.ADD10):
                    return "50";
                case (UtilsController.Constant.ADD50):
                    return "250";
                case (UtilsController.Constant.ADD1000):
                    return "5000";
                default:
                    return "Unknown";
            }
        }

        public static String ConvertAction(int action)
        {
            switch (action)
            {
                case 0:
                    return "Disable";
                default:
                    return "Enable";
            }
        }

        public static string GetErrorCodeCharging(string code)
        {
            switch (code)
            {
                // Procedures
                case "0": return Error.Success;
                case "-1": return Error.Unknown;
                case "-10": return Error.SystemErr;
                case "-11": return Error.ErrUnknown;
                case "-12": return Error.NotEnoughMoney;
                case "-13": return Error.WrongOTP;
                case "-14": return Error.InvalidIOTP;
                case "-15": return Error.TimeoutOTP;
                case "1": return Error.LoginFailure;
                case "2": return Error.UserExisted;
                case "-2": return Error.RegisterFailure;
                case "3": return Error.PasswordInvalid;
                case "-3": return Error.TypeErr;
                case "4": return Error.WrongOldPass;
                case "-4": return Error.ErrTypePackage;
                case "5": return Error.CoinInvalid;
                case "-5": return Error.WrongServiceID;
                case "6": return Error.TranCodeInvalid;
                case "7": return Error.UserLocked;
                case "-16": return Error.UserNotExisted;

                // MPS
                case "-101": return Error.UpdatePassFailure;
                case "-102": return Error.LoginFailure;
                case "-103": return Lang.friendExisted;
                case "700": return Error.UserExisted;
                case "1500": return Error.NotEnoughMoney;
                case "1900": return Error.InvalidIOTP;
                case "2000": return Error.UserExisted;
                case "2100": return Lang.notEnoughPoint;
                case "2101": return Lang.overLimitExchange;
                default:
                    return code + " UNKNOWN CODE";
            }
        }
    }
}