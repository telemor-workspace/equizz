﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunQuizWeb.Class;
using FunQuizWeb.Models;

namespace FunQuizWeb.Controllers
{
    public class WinsController : BaseController
    {
        WsEquizz.WsEQuizClient wsClient;

        public WinsController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }
        // GET: Wins
        public ActionResult Index(string seqPage)
        {
            if (!CheckAuthToken())
            {
                return Redirect("/Account/Login");
            }
            String seq = seqPage != null ? seqPage : "1";

            // get all cateories
            Games categories = new Games();
            categories.games = new List<Game>();
            categories = Session["categories"] as Games;
            Session["categories"] = categories;

            Game category_wins = categories.games.Find(x => x.id == UtilsController.Constant.WINS_CATEGORY);
            Game category_home = categories.games.Find(x => x.id == UtilsController.Constant.HOME_CATEGORY);

            Session["category-now"] = category_wins != null ? category_wins : category_home;

            AccountUser profile = Session["profile"] as AccountUser;

            WinsModel model = new WinsModel();

            // get ranking
            //GetRequest request = new GetRequest();
            //request.users = profile.msisdn;
            //request.msisdn = profile.msisdn;
            //request.seqPage = seqPage != null ? seqPage : "1";
            //request.rowsOnPage = UtilsController.Constant.ROWS_ON_PAGE;
            //String rsProfiles = SendPost(request, Session.SessionID, UtilsController.WsType.GetRanking);
            //AppResponse resProfiles = new AppResponse(rsProfiles);

            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime end = start.AddDays(6);

            String fromDate = start.Month + "/" + start.Day + "/" + start.Year;
            String toDate = end.Month + "/" + end.Day + "/" + end.Year;

            WsEquizz.response resProfiles = wsClient.wsGetRanking(wsUser, wsPassword, 
                profile.msisdn, 
                fromDate,
                toDate,
                seq, 
                UtilsController.Constant.NORMAL_ROWS_ON_PAGE, 
                ServiceId);

            if (resProfiles.errorCode == UtilsController.Constant.SUCCESS)
            {
                AccountUsers accountPlayedTime = new AccountUsers(resProfiles.topPlayedTime);
                AccountUsers accountWonMiniGame = new AccountUsers(resProfiles.topWonMiniGame);

                //AccountUser recent = profiles.accounts.Find(x => x.msisdn == profile.msisdn);
                //// if recent is in the top 3 => not remove from profiles and set null to reccent
                //if (int.Parse(recent.ranking) > 3)
                //{
                //    // remove recent from list profiles
                //    profiles.accounts.RemoveAll(x => x.msisdn == profile.msisdn);
                //    model.recent = recent;
                //}

                AccountUsers accounts = new AccountUsers();
                accounts.accounts = new List<AccountUser>();
                model.topPlay = accountPlayedTime != null ? accountPlayedTime : accounts;
                model.topWon = accountWonMiniGame != null ? accountWonMiniGame : accounts;
            }
            model.categories = categories;
            return View("Index", model);
        }
    }
}