﻿using FunQuizWeb.Class;
using FunQuizWeb.Models;
using FunQuizWeb.WsEquizz;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FunQuizWeb.Controllers
{
    public class AdminController : BaseController
    {
        // GET: Admin
        //private readonly IJwtHandler _jwtHandler;
        WsEquizz.WsEQuizClient wsClient;

        public AdminController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }

        private static log4net.ILog Log { get; set; } = log4net.LogManager.GetLogger(typeof(AdminController));

        // GET: Admin
        // LEVEL ONE
        public ActionResult Config()
        {
            if (!CheckAuthTokenAdmin())
            {
                return Redirect("/Admin/Login");
            }
            AdminConfigModel model = new AdminConfigModel();
            model.configurations = new Configurations();
            model.configurations.configurations = new List<Configuration>();
            // get all prayers which are saved in Session from Home page

            WsEquizz.response resLoad = wsClient.wsAdminGetConfiguration(wsUser, wsPassword, ServiceId);
            if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
            {
                model.configurations = new Configurations(resLoad.listConfigurations);
                model.rewardEnable = model.configurations.configurations.Find(x => x.para_key == "PLAYING_TIMES_ENABLE");
            }
            return View("Config", model);
        }

        [ValidateAntiForgeryToken]
        public JsonResult ConfigAction(String status)
        {
            Log.Info("status: " + status);
            try
            {
                if (!CheckAuthTokenAdmin())
                {
                    return Json(new
                    {
                        href = "/Admin/Login"
                    });
                }

                WsEquizz.response resDelete = wsClient.wsAdminUpdateConfiguration(wsUser, wsPassword, "PLAYING_TIMES_ENABLE", status, ServiceId);
                if (resDelete.errorCode == UtilsController.Constant.SUCCESS)
                {
                    return Json(new
                    {
                        error_code = resDelete.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(resDelete.errorCode),
                        href = Session["href-now"] as String
                    });
                }
                else
                {
                    Log.Error("EditingAction exception");
                    return Json(new
                    {
                        error_code = resDelete.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(resDelete.errorCode),
                        href = "/Common/Error"
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error("EditingAction exception");
                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = UtilsController.GetErrorCodeCharging("-10"),
                });
            }
        }

        public ActionResult Index()
        {
            if (!CheckAuthTokenAdmin())
            {
                return Redirect("/Admin/Login");
            }

            // inti name of level parents
            List<String> parentName = new List<string>();
            parentName.Add("Menus");
            parentName.Add("Topics");
            parentName.Add("Games");
            parentName.Add("Questions");
            parentName.Add("Answers");
            Session["parents-name"] = parentName;

            AdminHomeModel model = new AdminHomeModel();
            Games topics = new Games();
            topics.games = new List<Game>();
            model.recent = topics;
            model.level = UtilsController.Constant.LEVEL_ONE;

            // get all prayers which are saved in Session from Home page

            WsEquizz.response resLoad = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", UtilsController.Constant.PARENT_ID, "1", "10", ServiceId);
            if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
            {
                topics = new Games(resLoad.listGames);
                model.level = UtilsController.Constant.LEVEL_ONE;
                // remove all unneccessary 
                topics.games.RemoveAll(x => x.id == UtilsController.Constant.HOME_CATEGORY);
                topics.games.RemoveAll(x => x.id == UtilsController.Constant.WINS_CATEGORY);
                model.recent = topics;
                Session["topics"] = topics;
                Session["href-now"] = "/Admin";
            }
            return View("Index", model);
        }

        // GET: LEVEL
        // parentID la id cua level truoc do
        public ActionResult Child(String levelNow, String levelChoose, String parentID, String page)
        {
            if (!CheckAuthTokenAdmin())
            {
                return Redirect("/Admin/Login");
            }
            Log.Info("levelNow: " + levelNow + " levelChoose: " + levelChoose + " parentID: " + parentID);

            Session["levelNow"] = levelNow;
            Session["levelChoose"] = levelChoose;
            Session["parentID"] = parentID;

            String pageNow = page != null ? page : "1";

            //convert level to get data
            // please note that if you change the level of parent id, child id must be clear

            if (levelNow == UtilsController.Constant.LEVEL_ONE)
            {
                return Redirect("/Admin");
            }
            else
            {
                // get the first level for all 
                if (parentID == null || levelChoose == null)
                // this case only happen when user choose this level in the first click
                {
                    // get all parents in this level
                    WsEquizz.response resLoad = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", UtilsController.Constant.PARENT_ID, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);
                    if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        Games topics = new Games(resLoad.listGames);
                        Parent parent = new Parent();
                        parent.level = UtilsController.Constant.LEVEL_ONE;
                        parent.index = UtilsController.Constant.LEVEL_ONE;

                        topics.games.RemoveAll(x => x.id == UtilsController.Constant.HOME_CATEGORY);
                        topics.games.RemoveAll(x => x.id == UtilsController.Constant.WINS_CATEGORY);

                        parent.data = topics;
                        List<Parent> parentsNew = new List<Parent>();
                        parentsNew.Add(parent);
                        Session["parents"] = parentsNew;

                        AdminHomeModel model = new AdminHomeModel();
                        model.recent = null;
                        model.parents = parentsNew;
                        model.level = levelNow;
                        Session["href-now"] = "/Admin/Child?levelNow=" + levelNow;

                        return View("Child", model);
                    }
                    else
                    {
                        Log.Error("Child parentID == null || levelChoose == null res.status # success");
                        return Redirect("/Common/Error");
                    }
                }
                else if (parentID != null && levelChoose != null)
                // happen when user choosed the first level
                {
                    // make sure that the number of element in Session[parents] must be equared to levelChoose
                    List<Parent> parents = Session["parents"] as List<Parent>;
                    // save name of this parents
                    Parent parentSetName = parents.Find(x => int.Parse(x.index) == int.Parse(levelChoose));
                    Game choose = parentSetName.data.games.Find(x => x.id == parentID);
                    String name = choose.nameLocal;
                    parents.Single(x => x.index == levelChoose).name = name;
                    parents.Single(x => x.index == levelChoose).choose = choose;

                    if (int.Parse(levelNow) > int.Parse(levelChoose) + 1)
                    {
                        // remove all parents that has index > levelChoose
                        parents.RemoveAll(x => int.Parse(x.index) > int.Parse(levelChoose));

                        // get this parents
                        WsEquizz.response resLoad = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", parentID, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);
                        if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
                        {
                            Games games = new Games(resLoad.listGames);

                            //update Session
                            Parent parentUpdate = parents.Find(x => int.Parse(x.index) == int.Parse(levelChoose) + 1);
                            List<Parent> parentNew = new List<Parent>();

                            if (parentUpdate == null)
                            {
                                Parent parent = new Parent();
                                parent.data = games;
                                parent.level = (int.Parse(levelChoose) + 1).ToString();
                                parent.index = (int.Parse(levelChoose) + 1).ToString();
                                // save to session
                                parentNew.AddRange(parents);
                                parentNew.Add(parent);
                                Session["parents"] = parentNew;
                            }
                            else
                            {
                                // update
                                parentUpdate.data = games;
                                // save to session
                                parentNew.AddRange(parents);
                                Session["parents"] = parentNew;
                            }
                            AdminHomeModel model = new AdminHomeModel();
                            model.recent = null;
                            model.level = levelNow;
                            model.parents = new List<Parent>();
                            model.parents = parentNew;

                            Session["href-now"] = "/Admin/Child?levelChoose=" + levelChoose + "&levelNow=" + levelNow + "&parentID=" + parentID + "&page=" + pageNow;

                            return View("Child", model);
                        }
                        else
                        {
                            Log.Error("Child parentID != null && levelChoose != null res.status # success");
                            return Redirect("/Admin");
                        }
                    }
                    else
                    {
                        AdminHomeModel model = new AdminHomeModel();
                        Games games = new Games();
                        games.games = new List<Game>();
                        model.level = levelNow;
                        model.parents = parents;
                        model.recent = new Games();
                        model.recent.games = new List<Game>();

                        Session["href-now"] = "/Admin/Child?levelChoose=" + levelChoose + "&levelNow=" + levelNow + "&parentID=" + parentID + "&page=" + pageNow;

                        // get this parents
                        WsEquizz.response resLoad = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", parentID, pageNow, UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);
                        if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
                        {
                            games = new Games(resLoad.listGames);
                            model.recent = games;
                            model.pageNow = pageNow;
                            model.pageTotal = resLoad.totalPage;
                        }
                        return View("Child", model);
                    }
                }
                else
                {
                    Log.Error("Child exception case # success res.status # NO_DATA");
                    return Redirect("/Common/Error");
                }
            }
        }

        // ACTION ------------------------------------------------------------------------------------------------------------------------
        public ActionResult Login()
        {
            // admin login here
            return View("Login");
        }

        [ValidateAntiForgeryToken]
        public ActionResult LoginAction(String account, String password)
        {
            try
            {
                // admin login here
                if (account != null && password != null)
                {
                    WsEquizz.response resLogin = wsClient.wsAdminLogin(wsUser, wsPassword, account, password, ServiceId);
                    if (resLogin.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        // create new auth
                        CreateAuthTokenAdmin();
                        return Redirect("/Admin");
                    }
                    else
                    {
                        return Redirect("/Admin/Login");
                    }
                }
                else
                {
                    return Redirect("/Admin/Login");
                }
            }
            catch (Exception ex)
            {
                Log.Error("EditingAction exception " + ex);
                return Redirect("/Common/AdminError");
            }
        }
        // POST 
        public ActionResult Editing(String id, String level)
        {
            Log.Info("id: " + id + " level: " + level);

            if (!CheckAuthTokenAdmin())
            {
                return Redirect("/Admin/Login");
            }

            if (level == UtilsController.Constant.LEVEL_ONE)
            {
                Games topics = Session["topics"] as Games;
                Game topic = topics.games.Find(x => x.id == id);

                AdminEditingModel model = new AdminEditingModel();
                model.game = topic;
                Session["editing"] = model.game;
                model.previous_href = Session["href-now"] as String;
                return View("Editing", model);
            }
            else
            {
                AdminEditingModel model = new AdminEditingModel();
                model.game = null;
                // get unit by id
                WsEquizz.response resLoad = wsClient.wsGetDataById(wsUser, wsPassword, "", id, "1", "10", ServiceId);
                if (resLoad.errorCode == UtilsController.Constant.SUCCESS)
                {
                    Games games = new Games(resLoad.listGames);
                    model.game = games.games[0];
                    Session["editing"] = model.game;
                    model.previous_href = Session["href-now"] as String;
                    return View("Editing", model);
                }
                else
                {
                    Log.Error("Editing res.status # SUCCESS");
                    return Redirect("/Common/Error");
                }
            }
        }
        // POST 
        public ActionResult Adding(String level)
        {
            if (!CheckAuthTokenAdmin())
            {
                return Redirect("/Admin/Login");
            }
            Log.Info(" level: " + level);

            if (level != null)
            {
                AdminAddingModel model = new AdminAddingModel();
                model.level = level;
                // get parents
                model.types = new List<string>();
                model.types.Add(UtilsController.GetContentType.AUDIO_FILE);
                model.types.Add(UtilsController.GetContentType.AUDIO_LINK);
                model.types.Add(UtilsController.GetContentType.PICTURE_FILE);
                model.types.Add(UtilsController.GetContentType.TEXT_TYPE);
                model.types.Add(UtilsController.GetContentType.VIDEO_FILE);
                model.types.Add(UtilsController.GetContentType.VIDEO_LINK);

                List<Parent> parents = Session["parents"] as List<Parent>;

                if (level == UtilsController.Constant.LEVEL_ONE)
                {
                    model.parent = null;
                    model.previous_href = Session["href-now"] as String;
                    return View("Adding", model);
                }
                else
                {
                    Parent parent = parents.Find(x => int.Parse(x.index) == int.Parse(level) - 1);
                    if (parent == null || parent.data.games.Count == 0)
                    {
                        Log.Error("Adding parent == null || parent.data.prayers.Count == 0");
                        return Redirect("/Common/Error");
                    }
                    else
                    {
                        model.parent = parent.choose;
                        model.previous_href = Session["href-now"] as String;
                        return View("Adding", model);
                    }
                }
            }
            else
            {
                Log.Error("Adding level == null");
                return Redirect("/Common/Error");
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult EditingAction(Game data, HttpPostedFileBase icon, HttpPostedFileBase logo)
        {
            try
            {
                if (!CheckAuthTokenAdmin())
                {
                    return Json(new
                    {
                        href = "/Admin/Login"
                    });
                }
                Log.Info("packet: " + data);

                // check image upload
                if (icon != null && (icon.FileName.EndsWith(".png") || icon.FileName.EndsWith(".jpg")))
                {
                    string iconName = Path.GetFileName(icon.FileName);
                    //if (iconName != data.icon)
                    //{
                    String path = Path.Combine(Server.MapPath("~/Content/assets/images/games/"), iconName);

                    // file is uploaded
                    icon.SaveAs(path);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    using (MemoryStream ms = new MemoryStream())
                    {
                        icon.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                    //}
                }
                else if (icon != null && !icon.FileName.EndsWith(".png") && !icon.FileName.EndsWith(".jpg"))
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.FAILURE,
                        error_content = "An error happens with your icon",
                        href = Session["href-now"] as String
                    });
                }

                // check image upload
                if (logo != null && (logo.FileName.EndsWith(".png") || logo.FileName.EndsWith(".jpg")))
                {
                    string logoName = Path.GetFileName(logo.FileName);
                    //if (logoName != data.logo)
                    //{
                    String path = Path.Combine(Server.MapPath("~/Content/assets/images/games/"), logoName);

                    // file is uploaded
                    logo.SaveAs(path);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    using (MemoryStream ms = new MemoryStream())
                    {
                        logo.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
                else if (logo != null && !logo.FileName.EndsWith(".png") && !logo.FileName.EndsWith(".jpg"))
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.FAILURE,
                        error_content = "An error happens with your logo",
                        href = Session["href-now"] as String
                    });
                }
                Game gameLoad = Session["editing"] as Game;
                Game game = data;

                WsEquizz.response resAdd = wsClient.wsAdminUpdateNewGame(wsUser, wsPassword,
                    game.id != null ? game.id : gameLoad.id,
                    game.code != null ? game.code : gameLoad.code,
                    game.nameGlobal != null ? game.nameGlobal : gameLoad.nameGlobal != null ? gameLoad.nameGlobal : "",
                    game.nameLocal != null ? game.nameLocal : gameLoad.nameLocal != null ? gameLoad.nameLocal : "",
                    game.descriptionGlobal != null ? game.descriptionGlobal : gameLoad.descriptionGlobal != null ? gameLoad.descriptionGlobal : "",
                    game.descriptionLocal != null ? game.descriptionLocal : gameLoad.descriptionLocal != null ? gameLoad.descriptionLocal : "",
                    game.introductionGlobal != null ? game.introductionGlobal : gameLoad.introductionGlobal != null ? gameLoad.introductionGlobal : "",
                    game.introductionLocal != null ? game.introductionLocal : gameLoad.introductionLocal != null ? gameLoad.introductionLocal : "",
                    game.icon != null && game.icon != "undefined" ? game.icon : gameLoad.realIcon,
                    game.logo != null && game.logo != "undefined" ? game.logo : gameLoad.realLogo,
                    game.content != null ? game.content : "",
                    game.contentType != null ? game.contentType : gameLoad.contentType != null ? gameLoad.contentType : "-1",
                    game.fromDate != null ? convertToDateTimeServer(game.fromDate) : gameLoad.fromDate,
                    game.toDate != null ? convertToDateTimeServer(game.toDate) : gameLoad.toDate,
                    game.status != null ? game.status : gameLoad.status,
                    game.isPlayed != null ? game.isPlayed : gameLoad.isPlayed,
                    game.typeOf != null ? game.typeOf : gameLoad.typeOf,
                    game.note != null ? game.note : gameLoad.note != null ? gameLoad.note : "",
                    ServiceId);
                return Json(new
                {
                    error_code = resAdd.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(resAdd.errorCode),
                    href = Session["href-now"] as String,

                });
            }
            catch (Exception ex)
            {
                Log.Error("EditingAction exception " + ex);
                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = UtilsController.GetErrorCodeCharging("-10"),
                    href = "/Common/Error",
                });
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult DeleteAction(String id)
        {
            Log.Info("id: " + id);

            try
            {
                if (!CheckAuthTokenAdmin())
                {
                    return Json(new
                    {
                        href = "/Admin/Login"
                    });
                }

                WsEquizz.response resDelete = wsClient.wsAdminDeleteGame(wsUser, wsPassword, id, ServiceId);
                if (resDelete.errorCode == UtilsController.Constant.SUCCESS)
                {
                    return Json(new
                    {
                        error_code = resDelete.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(resDelete.errorCode),
                        href = Session["href-now"] as String
                    });
                }
                else
                {
                    Log.Error("EditingAction exception");
                    return Json(new
                    {
                        error_code = resDelete.errorCode,
                        error_content = UtilsController.GetErrorCodeCharging(resDelete.errorCode),
                        href = "/Common/Error"
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error("EditingAction exception");
                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = UtilsController.GetErrorCodeCharging("-10"),
                });
            }
        }
        // GET: Login
        [ValidateAntiForgeryToken]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddingAction(Game data, HttpPostedFileBase icon, HttpPostedFileBase logo)
        {
            Log.Info("packet: " + data);

            try
            {
                if (!CheckAuthTokenAdmin())
                {
                    return Json(new
                    {
                        href = "/Admin/Login"
                    });
                }

                // check image upload
                if (icon != null && (icon.FileName.EndsWith(".png") || icon.FileName.EndsWith(".jpg")))
                {

                    string iconName = Path.GetFileName(icon.FileName);
                    String path = Path.Combine(Server.MapPath("~/Content/assets/images/games/"), iconName);
                    // file is uploaded
                    icon.SaveAs(path);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    using (MemoryStream ms = new MemoryStream())
                    {
                        icon.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
                else
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.FAILURE,
                        error_content = "An error happens with your icon",
                        href = "/Common/Error"
                    });
                }

                // check image upload
                if (logo != null && (logo.FileName.EndsWith(".png") || logo.FileName.EndsWith(".jpg")))
                {

                    string logoName = Path.GetFileName(logo.FileName);
                    String path = Path.Combine(Server.MapPath("~/Content/assets/images/games/"), logoName);
                    // file is uploaded
                    logo.SaveAs(path);

                    // save the image path path to the database or you can send image 
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    using (MemoryStream ms = new MemoryStream())
                    {
                        logo.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }
                }
                else
                {
                    return Json(new
                    {
                        error_code = UtilsController.Constant.FAILURE,
                        error_content = "An error happens with your logo",
                        href = "/Common/Error"
                    });
                }

                Game game = data;
                WsEquizz.response resAdd = wsClient.wsAdminAddNewGame(wsUser, wsPassword,
                    game.parentId,
                    game.code,
                    game.nameGlobal,
                    game.nameLocal,
                    game.descriptionGlobal,
                    game.descriptionLocal,
                    game.introductionGlobal,
                    game.introductionLocal,
                    game.icon,
                    game.logo,
                    game.content,
                    game.contentType,
                    game.level,
                    convertToDateTimeServer(game.fromDate),
                    convertToDateTimeServer(game.toDate),
                    game.status,
                    game.isPlayed,
                    game.typeOf,
                    game.note,
                    ServiceId);
                return Json(new
                {
                    error_code = resAdd.errorCode,
                    error_content = UtilsController.GetErrorCodeCharging(resAdd.errorCode),
                    href = Session["href-now"] as String,

                });
            }
            catch (Exception ex)
            {
                Log.Error("AddingAction exception " + ex);

                return Json(new
                {
                    error_code = UtilsController.Constant.EXCEPTION,
                    error_content = UtilsController.GetErrorCodeCharging("-10"),
                });
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult UploadAction(HttpPostedFileBase file, String level, String view)
        {
            Log.Info("level: " + level + " file: " + file);

            if (!CheckAuthTokenAdmin())
            {
                return Json(new
                {
                    href = "/Admin/Login"
                });
            }
            // check type of file
            if (file != null && file.ContentLength > 0)
                try
                {
                    var supportedTypes = new[] { "jpg", "png" };
                    var fileExt = Path.GetExtension(file.FileName).Substring(1);
                    System.Diagnostics.Debug.WriteLine("fileExt: " + fileExt);
                    System.Diagnostics.Debug.WriteLine("file.ContentLength: " + file.ContentLength);
                    if (!supportedTypes.Contains(fileExt))
                    {
                        //return Json(new
                        //{
                        //    error_content = "File Extension Is InValid - Only Upload WORD/PDF/EXCEL/TXT File",
                        //});
                        ViewBag.Message = "File Extension Is InValid - Only Upload PNG/JPG File";
                        Log.Error("Upload fail");

                        return Json(new
                        {
                            href = "/Admin/Login"
                        });
                    }
                    else if (file.ContentLength > (5 * 1024 * 1024))
                    {
                        //return Json(new
                        //{
                        //    error_content = "File size Should Be UpTo " + 5 + "MB",
                        //});
                        ViewBag.Message = "File size Should Be UpTo 5MB";
                        Log.Error("Upload fail");

                        return Json(new
                        {
                            href = "/Admin/Login"
                        });
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("FileName: " + Path.GetFileName(file.FileName));
                        var fileName = Path.GetFileName(file.FileName);
                        String path = Path.Combine(Server.MapPath("~/Content/assets/images/games/"), fileName);

                        file.SaveAs(path);

                        Log.Info("Upload success");

                        return Json(new
                        {
                            href = "/Admin/Login"
                        });
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    Log.Error("You have not specified a file " + ex);
                    return Json(new
                    {
                        href = "/Admin/Login"
                    });
                }
            else
            {
                Log.Error("You have not specified a file.");

                ViewBag.Message = "You have not specified a file.";
                return Json(new
                {
                    href = "/Admin/Login"
                });
            }
        }

        private void CreateAuthTokenAdmin()
        {
            // create session authen
            SessionIDManager manager = new SessionIDManager();
            string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);
            Response.Cookies["AuthTokenAdmin"].Value = newSessionId;
            Session["AuthTokenAdmin"] = newSessionId;
        }

        private bool CheckAuthTokenAdmin()
        {
            if (Session["AuthTokenAdmin"] != null && Request.Cookies["AuthTokenAdmin"] != null)
            {
                if (!Session["AuthTokenAdmin"].ToString().Equals(Request.Cookies["AuthTokenAdmin"].Value))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}