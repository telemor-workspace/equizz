﻿using Antlr.Runtime;
using FunQuizCore.Model;
using FunQuizWeb.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FunQuizWeb.Controllers
{
    public class BaseController : Controller
    {
        private static log4net.ILog Log { get; set; } = log4net.LogManager.GetLogger(typeof(BaseController));
        public static String ServiceId = ConfigurationManager.AppSettings["service_id"];
        public static String timeMiniGame = ConfigurationManager.AppSettings["timeMiniGame"];
        public static String timeCountDown = ConfigurationManager.AppSettings["timeCountDown"];

        //private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static readonly DateTime Epoch = DateTime.Now;
        public String wsUser = ConfigurationManager.AppSettings["wsUser"];
        public String wsPassword = ConfigurationManager.AppSettings["wsPassword"];
        public static string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                         "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                         "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                         "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                         "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
                         "MM/d/yyyy HH:mm:ss.ffffff" };
        // GET: Base

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        protected string convertToDateTimeServer(String date)
        {
            // date: 
            DateTime oDateFrom = DateTime.Parse(date);
            string hour = oDateFrom.Hour < 10 ? "0" + oDateFrom.Hour : oDateFrom.Hour.ToString();
            string minute = oDateFrom.Minute < 10 ? "0" + oDateFrom.Minute : oDateFrom.Minute.ToString();
            string second = oDateFrom.Second < 10 ? "0" + oDateFrom.Second : oDateFrom.Second.ToString();
            string month = oDateFrom.Month < 10 ? "0" + oDateFrom.Month : oDateFrom.Month.ToString();
            string day = oDateFrom.Day < 10 ? "0" + oDateFrom.Day : oDateFrom.Day.ToString();
            string fromCheck = oDateFrom.Year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
            return fromCheck; //yyyy-MM-dd HH24:mm:ss
        }

        public class ReCaptcha
        {
            public bool Success { get; set; }
            public List<string> ErrorCodes { get; set; }

            public static bool Validate(string encodedResponse)
            {
                if (string.IsNullOrEmpty(encodedResponse)) return false;

                var client = new System.Net.WebClient();
                var secret = ConfigurationManager.AppSettings[UtilsController.WsType.GoogleCaptcha];

                if (string.IsNullOrEmpty(secret)) return false;

                var googleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, encodedResponse));

                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                var reCaptcha = serializer.Deserialize<ReCaptcha>(googleReply);

                return reCaptcha.Success;
            }
        }

        public static String validateMsisdn(String input)
        {
            //75, 76
            if (input == null || input.Length == 0 || !long.TryParse(input, out long temp))
            {
                return "";
            }
            else
            {
                if (input.StartsWith("670") && input.Length == 11)
                {
                    String realInput = input;
                    String convertInput = input.Substring(3);

                    if (!convertInput.StartsWith("75")
                        && !convertInput.StartsWith("76")
                             )
                    {
                        Log.Debug("msisdn invalid: " + realInput);
                        return "";
                    }
                    Log.Debug("msisdn valid: " + realInput);
                    return realInput;
                }
                else if (input.StartsWith("75") && input.Length == 8)
                {
                    input = "670" + input;
                    Log.Debug("msisdn valid: " + input);
                    return input;
                }
                else if (input.StartsWith("76") && input.Length == 8)
                {
                    input = "670" + input;
                    Log.Debug("msisdn valid: " + input);
                    return input;
                }
                else
                {
                    Log.Debug("msisdn valid: ");
                    return "";
                }
            }
        }

        protected void SetWsClient(ref WsEquizz.WsEQuizClient wsClient, String sessionId)
        {
            int lastNum = sessionId[sessionId.Length - 1];
            if (lastNum % 2 == 0)
            {
                // check
                Boolean check = CheckConnection(ConfigurationManager.AppSettings["wsUrl1"]);
                if(check)
                {
                    wsClient.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["wsUrl1"]);
                }
                else
                {
                    wsClient.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["wsUrl2"]);
                }
            }
            else
            {
                // check
                Boolean check = CheckConnection(ConfigurationManager.AppSettings["wsUrl2"]);
                if (check)
                {
                    wsClient.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["wsUrl2"]);
                }
                else
                {
                    wsClient.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["wsUrl1"]);
                }
            }
        }

        private Boolean CheckConnection(String url)
        {
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url);

                var response = (HttpWebResponse)myRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //  it's at least in some way responsive
                    //  but may be internally broken
                    //  as you could find out if you called one of the methods for real
                    //Log.Info(string.Format("{0} Available", url));
                    return true;
                }
                //else
                //{
                //    //  well, at least it returned...
                //    Log.Info(string.Format("{0} Returned, but with status: {1}", url, response.StatusDescription));
                //}
                return false;
            }
            catch (Exception ex)
            {
                //  not available at all, for some reason
                //Log.Info(string.Format("{0} unavailable: {1}", url, ex.Message));
                return false;
            }
        }
        public static String SendPost(Posting obj, String sessionId, String type)
        {
            obj.serviceId = BaseController.ServiceId;
            obj.SV_ID = BaseController.ServiceId;

            var json = JsonConvert.SerializeObject(obj);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var url = UtilsController.GetWsClient(type);

            Log.Debug("URL: " + url);
            Log.Debug("Request: " + json);

            using (var client = new HttpClient())
            {
                var response = client.PostAsync(url, data).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    // by calling .Result you are synchronously reading the result
                    string responseString = responseContent.ReadAsStringAsync().Result;
                    Log.Debug("Response: " + responseString);
                    return responseString;
                }
                else
                {
                    Log.Error("Response: " + response.StatusCode.ToString());
                    return response.StatusCode.ToString();
                }
            }
        }

        // load profile of user
        protected void ReloadProfileInfo(WsEquizz.WsEQuizClient wsClient)
        {
            Log.Debug("ReloadProfileInfo");

            String msisdn = Session["msisdn"] as String;
            if (msisdn != null)
            {
                //AccountRequest req = new AccountRequest();
                //req.users = msisdn;
                //req.msisdn = msisdn;

                //String rs = SendPost(req, Session.SessionID, UtilsController.WsType.UserGetProfile);
                //AppResponse response = new AppResponse(rs);

                WsEquizz.response res = wsClient.wsGetUserProfile(wsUser, wsPassword, msisdn, ServiceId);
                // only one profile would return, if not, an error happens
                if (res.errorCode == UtilsController.Constant.SUCCESS)
                {
                    AccountUsers profiles = new AccountUsers(res.listAccounts);

                    if (profiles.accounts != null && profiles.accounts.Count == 1)
                        Session["profile"] = profiles.accounts[0];
                }
            }
        }

        // load sub info
        protected void ReloadSubInfo(WsEquizz.WsEQuizClient wsClient)
        {
            Log.Debug("ReloadSubInfo");

            String msisdn = Session["msisdn"] as String;
            if (msisdn != null)
            {
                //AccountRequest reqSub = new AccountRequest();
                //reqSub.msisdn = msisdn;
                //reqSub.users = msisdn;

                //String rs = SendPost(reqSub, Session.SessionID, UtilsController.WsType.UserGetListService);
                //AppResponse response = new AppResponse(rs);

                WsEquizz.response res = wsClient.wsGetUserServices(wsUser, wsPassword, msisdn, ServiceId);

                if (res.errorCode == UtilsController.Constant.SUCCESS)
                {
                    if (res.listServices != null && res.listServices.Length != 0)
                    {
                        Session["isSub"] = "true";
                        List<String> listServices = new List<string>();
                        foreach (String service in res.listServices)
                        {
                            listServices.Add(service);
                        }
                        Session["packetReg"] = listServices;
                    }
                    else
                    {
                        Session["isSub"] = "false";
                        Session["packetReg"] = new List<String>();
                    }
                }
                else
                {
                    Session["isSub"] = "false";
                    Session["packetReg"] = new List<String>();
                }
            }
        }

        protected void CreateAuthToken()
        {
            // create session authen
            SessionIDManager manager = new SessionIDManager();
            string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);
            Response.Cookies["AuthToken"].Value = newSessionId;
            Session["AuthToken"] = newSessionId;
        }
        protected bool CheckAuthToken()
        {
            if (Session["AuthToken"] != null && Request.Cookies["AuthToken"] != null)
            {
                if (!Session["AuthToken"].ToString().Equals(Request.Cookies["AuthToken"].Value))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        // dateTime : MM/dd/yyyy
        protected long ConvertToTimestamp(String dateTime)
        {
            // 2020-10-06 00:00:00.0 -> dateTime

            // convert to Datetime
            DateTime oDateTo = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss.f", CultureInfo.InvariantCulture);
            DateTime endTime = new DateTime(oDateTo.Year, oDateTo.Month, oDateTo.Day, oDateTo.Hour, oDateTo.Minute, 0);
            TimeSpan elapsedTime = endTime - DateTime.Now;
            return (long)elapsedTime.TotalSeconds;
        }

        protected DateTime getCountTimeToDateTime(String datetime)
        {
            DateTime convert = DateTime.ParseExact(datetime, "yyyy-MM-dd HH:mm:ss.f", CultureInfo.InvariantCulture);
            DateTime endTime = new DateTime(convert.Year, convert.Month, convert.Day, convert.Hour, convert.Minute, 0);
            return endTime;
        }
    }
}