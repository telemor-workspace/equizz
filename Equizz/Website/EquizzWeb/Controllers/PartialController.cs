﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FunQuizWeb.Class;
using FunQuizWeb.Models;

namespace FunQuizWeb.Controllers
{
    public class PartialController : Controller
    {
        // GET: Partial
        public ActionResult ForgotPass()
        {
            PartialModel model = new PartialModel();
            return PartialView("ResetPassword", model);
        }
        public ActionResult ChangePass()
        {
            PartialModel model = new PartialModel();
            return PartialView("ChangePassword", model);
        }
        public ActionResult Exchange()
        {
            PartialModel model = new PartialModel();
            AccountUser profile = Session["profile"] as AccountUser;
            model.profile = profile;
            return PartialView("Exchange", model);
        }

        public ActionResult InviteFriend()
        {
            PartialModel model = new PartialModel();
            AccountUser profile = Session["profile"] as AccountUser;
            model.profile = profile;
            return PartialView("InviteFriend", model);
        }
        public ActionResult BuyCode()
        {
            PartialModel model = new PartialModel();
            AccountUser profile = Session["profile"] as AccountUser;
            model.profile = profile;
            return PartialView("BuyCode", model);
        }
        public ActionResult ActiveCode(String href)
        {
            PartialModel model = new PartialModel();
            model.href = href;
            AccountUser profile = Session["profile"] as AccountUser;
            model.profile = profile;
            return PartialView("ActiveCode", model);
        }

        public ActionResult Response(String href, String response)
        {
            PartialModel model = new PartialModel();
            model.href = href;
            model.response = response;
            AccountUser profile = Session["profile"] as AccountUser;
            model.profile = profile;
            return PartialView("Response", model);
        }
    }
}