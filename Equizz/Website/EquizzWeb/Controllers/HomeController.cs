﻿using FunQuizWeb.Class;
using FunQuizWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FunQuizWeb.Controllers
{
    public class HomeController : BaseController
    {
        WsEquizz.WsEQuizClient wsClient;

        public HomeController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }

        public ActionResult Index()
        {
            // load all categories
            GetRequest request = new GetRequest();
            request.parentId = UtilsController.Constant.PARENT_ID;

            Games categories = new Games();
            categories.games = new List<Game>();
            Games subjects = new Games();
            subjects.games = new List<Game>();
            Games games = new Games();
            games.games = new List<Game>();
            HomeModel model = new HomeModel();
            model.games = new Games();
            model.games.games = new List<Game>();
            model.categories = new Games();
            model.categories.games = new List<Game>();

            // get all cateories

            WsEquizz.response res = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", UtilsController.Constant.PARENT_ID, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

            //String rs = SendPost(request, Session.SessionID, UtilsController.WsType.GetDataByParentId);
            //AppResponse res = new AppResponse(rs);
            if (res.errorCode == UtilsController.Constant.SUCCESS)
            {
                categories = new Games(res.listGames);
                categories.games.Sort(delegate (Game x, Game y)
                {
                    if (x.id == null && y.id == null) return 0;
                    else if (x.id == null) return -1;
                    else if (y.id == null) return 1;
                    else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                });
                model.categories = categories;
                Session["categories"] = categories;
                Session["category-now"] = categories.games[0];
                // get all type of games related to all categories
                for (int i = 0; i < categories.games.Count; i++)
                {
                    if (categories.games[i].id != UtilsController.Constant.HOME_CATEGORY &&
                        categories.games[i].id != UtilsController.Constant.MINIGAMES_CATEGORY)
                    {
                        // get subjects
                        //GetRequest request2 = new GetRequest();
                        //request2.parentId = categories.games[i].id;
                        //request2.seqPage = "1";
                        //request2.rowsOnPage = UtilsController.Constant.ROWS_ON_PAGE;
                        //String rsSubjects = SendPost(request2, Session.SessionID, UtilsController.WsType.GetDataByParentId);

                        WsEquizz.response resGames2 = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", categories.games[i].id, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);
                        if (resGames2.errorCode == UtilsController.Constant.SUCCESS)
                        {
                            Games subjectsGet = new Games(resGames2.listGames);
                            subjectsGet.games.Sort(delegate (Game x, Game y)
                            {
                                if (x.id == null && y.id == null) return 0;
                                else if (x.id == null) return -1;
                                else if (y.id == null) return 1;
                                else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                            });
                            subjectsGet.games.ForEach(x => subjects.games.Add(x));
                            // get some newest games
                            for (int j = 0; j < subjectsGet.games.Count; j++)
                            {
                                //GetRequest request3 = new GetRequest();
                                //request3.parentId = subjectsGet.games[j].id;
                                //request3.seqPage = "1";
                                //request3.rowsOnPage = UtilsController.Constant.ROWS_ON_PAGE;
                                //String rsGames = SendPost(request3, Session.SessionID, UtilsController.WsType.GetDataByParentId);
                                //AppResponse resGames = new AppResponse(rsGames);

                                WsEquizz.response resGames3 = wsClient.wsGetDataByParentId(wsUser, wsPassword, "", subjectsGet.games[j].id, "1", UtilsController.Constant.MIN_ROWS_ON_PAGE, ServiceId);

                                if (resGames3.errorCode == UtilsController.Constant.SUCCESS)
                                {
                                    Games gamesGet = new Games(resGames3.listGames);
                                    gamesGet.games.Sort(delegate (Game x, Game y)
                                    {
                                        if (x.id == null && y.id == null) return 0;
                                        else if (x.id == null) return -1;
                                        else if (y.id == null) return 1;
                                        else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                                    });
                                    gamesGet.games.ForEach(x => games.games.Add(x));
                                }
                            }
                        }
                    }
                }
            }
            model.subjects = subjects;
            model.games = games;
            Session["games-now"] = games;
            return View("Index", model);
        }
    }
}