﻿using FunQuizWeb.Class;
using FunQuizWeb.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FunQuizWeb.Controllers
{
    public class MiniGamesController : BaseController
    {
        WsEquizz.WsEQuizClient wsClient;

        public MiniGamesController()
        {
            WsEquizz.WsEQuizClient wsClient = new WsEquizz.WsEQuizClient();
            SetWsClient(ref wsClient, Session.SessionID);
        }

        // GET: MiniGames
        public ActionResult Index(String gameId, String questionNow, String questionId, String answerId)
        {
            if (!CheckAuthToken())
            {
                return Redirect("/Account/Login");
            }

            // reset 
            Session["answers-of-user"] = null;
            String msisdn = Session["msisdn"] as String;
            // get all categories
            Games categories = Session["categories"] as Games;
            Game category = categories.games.Find(x => x.id == UtilsController.Constant.MINIGAMES_CATEGORY);
            Session["category-now"] = category;
            // get all mini games 
            Games mini = new Games();
            mini.games = new List<Game>();

            Games topics = new Games();
            topics.games = new List<Game>();

            Games games = new Games();
            games.games = new List<Game>();

            MinigamesModel model = new MinigamesModel();
            model.games = new Games();
            model.games.games = new List<Game>();

            //GetRequest request = new GetRequest();
            //request.parentId = category.id;
            //request.isPlayed = UtilsController.Constant.NOT_PLAYED;

            DayOfWeek day = DateTime.Now.DayOfWeek;
            int days = day - DayOfWeek.Monday;
            DateTime start = DateTime.Now.AddDays(-days);
            DateTime end = start.AddDays(6);
            string startMonth = start.Month < 10 ? "0" + start.Month : start.Month.ToString();
            string startDay = start.Day < 10 ? "0" + start.Day : start.Day.ToString();
            string endMonth = end.Month < 10 ? "0" + end.Month : end.Month.ToString();
            string endDay = end.Day < 10 ? "0" + end.Day : end.Day.ToString();

            String fromDate = startMonth + "/" + startDay + "/" + start.Year;
            String toDate = endMonth + "/" + endDay + "/" + end.Year;
            // convert to mm/dd/yyyy

            // get topic
            WsEquizz.response resType = wsClient.wsGetMiniGames(wsUser, wsPassword, category.id, UtilsController.Constant.NOT_PLAYED, fromDate, toDate, ServiceId);

            // the right response is only one game will return
            if (resType.errorCode == UtilsController.Constant.SUCCESS)
            {
                Games miniGet = new Games(resType.listGames);
                if (miniGet.games.Count > 0)
                {
                    miniGet.games.FindAll(x => x.isPlayed == UtilsController.Constant.NOT_PLAYED).Sort(delegate (Game x, Game y)
                    {
                        if (x.id == null && y.id == null) return 0;
                        else if (x.id == null) return -1;
                        else if (y.id == null) return 1;
                        else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                    });
                    model.topics = miniGet;
                    model.topic = miniGet.games.Count > 0 ? miniGet.games[0] : null;

                    // get game
                    //FunQuizMps.response resGames = wsClient.wsGetDataByParentId(wsUser, wsPassword, msisdn, model.topic.id, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);
                    //if (resGames.errorCode == UtilsController.Constant.SUCCESS)
                    //{
                    //    games = new Games(resGames.listGames);
                    //    games.games.FindAll(x => x.isPlayed == UtilsController.Constant.NOT_PLAYED).Sort(delegate (Game x, Game y)
                    //    {
                    //        if (x.id == null && y.id == null) return 0;
                    //        else if (x.id == null) return -1;
                    //        else if (y.id == null) return 1;
                    //        else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                    //    });
                    //}

                    // get game
                    WsEquizz.response resGames = wsClient.wsGetMiniGames(wsUser, wsPassword, model.topic.id, UtilsController.Constant.NOT_PLAYED, fromDate, toDate, ServiceId);
                    if (resGames.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        games = new Games(resGames.listGames);
                        games.games.FindAll(x => x.isPlayed == UtilsController.Constant.NOT_PLAYED).Sort(delegate (Game x, Game y)
                        {
                            if (x.id == null && y.id == null) return 0;
                            else if (x.id == null) return -1;
                            else if (y.id == null) return 1;
                            else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                        });
                    }
                    model.game = games.games.Count > 0 ? games.games[0] : null;
                    model.games = games;
                    model.timeLeft = ConvertToTimestamp(model.game.toDate);
                    model.timeExpired = getCountTimeToDateTime(miniGet.games[0].toDate).ToString("MM/dd/yyy HH:mm:ss");
                    Session["games-now"] = model.game;
                }
                else
                {
                    model.timeExpired = null;
                    model.games = miniGet;
                    model.game = null;
                    model.timeLeft = 0;
                }
                // get all questions related to this games
            }

            // get account last week
            List<AccountUser> accountWon = new List<AccountUser>();
            WsEquizz.response resAccounts = wsClient.wsGetUsersWonMiniGames(wsUser, wsPassword, fromDate, toDate, ServiceId);
            if (resAccounts.errorCode == UtilsController.Constant.SUCCESS)
            {
                AccountUsers accountUsers = new AccountUsers(resAccounts.listAccounts);
                accountUsers.accounts.ForEach(x => accountWon.Add(x));
            }
            model.accounts = accountWon;
            model.timeNow = DateTime.Now.ToString("MM/dd/yyy HH:mm:ss");
            return View("Index", model);
        }

        // questionId : question before
        // answerID: answer before
        // questionNow: question now
        public ActionResult PlayGame(String gameId, string questionId, string answerId, string questionNow)
        {
            if (gameId != null)
            {

                Games questions = new Games();
                questions.games = new List<Game>();


                String msisdn = Session["msisdn"] as String;

                Game game = Session["games-now"] as Game;

                MinigamePartialModel model = new MinigamePartialModel();

                model.game = game;
                // get the first question of this game
                model.questions = new Games();
                model.questions.games = new List<Game>();
                model.answers = new Games();
                model.answers.games = new List<Game>();

                model.questionNow = questionNow != null ? questionNow : "0";

                questions = Session["minigame-questions"] as Games;

                // for the first time to get question
                if (questionNow == null || questionNow == "0")
                {

                    // get questions
                    WsEquizz.response resQuestions = wsClient.wsGetDataByParentId(wsUser, wsPassword, msisdn, gameId, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

                    if (resQuestions.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        questions = new Games(resQuestions.listGames);
                        questions.games.Sort(delegate (Game x, Game y)
                        {
                            if (x.id == null && y.id == null) return 0;
                            else if (x.id == null) return -1;
                            else if (y.id == null) return 1;
                            else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                        });
                        questions.games.ForEach(x => model.questions.games.Add(x));
                        model.question = model.questions.games[0];
                        Session["minigame-questions"] = model.questions;
                    }
                }

                if (questionNow != null && int.Parse(questionNow) >= questions.games.Count())
                {
                    Games answersTake = Session["answer-now"] as Games;
                    Game answer = answersTake.games.Find(x => x.id == answerId);

                    List<Answer> listAnswer = Session["answers-of-user"] as List<Answer>;
                    Answer answerUser = new Answer();
                    answerUser.answer = answer;
                    answerUser.rightAnswer = answersTake.games.Find(x => x.typeOf == UtilsController.Constant.ANSWER_RIGHT);
                    if (listAnswer == null)
                    {
                        listAnswer = new List<Answer>();
                        listAnswer.Add(answerUser);
                    }
                    else
                    {
                        // update list answer
                        listAnswer.RemoveAll(x => x.answer.id == answerId);
                        listAnswer.Add(answerUser);
                    }
                    Session["answers-of-user"] = listAnswer;
                    String finished = Session["finished"] as String;
                    ResultPopupModel modelResult = new ResultPopupModel();
                    modelResult.listAnswers = listAnswer;
                    modelResult.questions = questions;
                    modelResult.finished = true;
                    Session["finished"] = "true";

                    // check the answer
                    int check = 1;
                    for (int c = 0; c < listAnswer.Count; c++)
                    {
                        if (listAnswer[c].answer.content == listAnswer[c].rightAnswer.content)
                        {
                            check *= 1;
                        }
                        else
                        {
                            check *= 0;
                        }
                    }
                    if (check == 1)
                    {
                        modelResult.result = true;
                        // update to the DB to add money for this user
                        WsEquizz.response resAddMoney = wsClient.wsUpdateMiniGameUserPlayAnswer(wsUser, wsPassword, gameId, msisdn, ServiceId);
                        modelResult.charge = resAddMoney.errorCode;
                    }

                    return PartialView("ResultMiniPopup", modelResult);
                }
                else
                {
                    Game question = questions.games[questionNow != null ? int.Parse(questionNow) : 0];
                    model.question = question;
                    model.questions = questions;
                    List<Answer> listAnswer = Session["answers-of-user"] as List<Answer>;
                    if (questionId != null && answerId != "" && answerId != null)
                    {
                        Games answersTake = Session["answer-now"] as Games;
                        Game answer = answersTake.games.Find(x => x.id == answerId);
                        Answer answerUser = new Answer();
                        answerUser.answer = answer;
                        answerUser.rightAnswer = answersTake.games.Find(x => x.typeOf == UtilsController.Constant.ANSWER_RIGHT);
                        if (listAnswer == null)
                        {
                            listAnswer = new List<Answer>();
                            listAnswer.Add(answerUser);
                        }
                        else
                        {
                            // update list answer
                            listAnswer.RemoveAll(x => x.answer.id == answerId);
                            listAnswer.Add(answerUser);
                        }
                    }
                    Session["answers-of-user"] = listAnswer;

                    // get answers
                    WsEquizz.response resAnwers = wsClient.wsGetDataByParentId(wsUser, wsPassword, msisdn, model.question.id, "1", UtilsController.Constant.MAX_ROWS_ON_PAGE, ServiceId);

                    if (resAnwers.errorCode == UtilsController.Constant.SUCCESS)
                    {
                        Games answersGet = new Games(resAnwers.listGames);
                        answersGet.games.Sort(delegate (Game x, Game y)
                        {
                            if (x.id == null && y.id == null) return 0;
                            else if (x.id == null) return -1;
                            else if (y.id == null) return 1;
                            else return int.Parse(x.id).CompareTo(int.Parse(y.id));
                        });
                        answersGet.games.ForEach(x => model.answers.games.Add(x));
                        Session["answer-now"] = model.answers;
                    }
                    return PartialView("MiniGamePartialView", model);
                }
            }
            else
            {
                return Redirect("/Home");
            }
        }

    }
}