﻿var imageIcon = document.getElementById("previewIcon").src.replace(/^.*[\\\/]/, '');
var imageLogo = document.getElementById("previewLogo").src.replace(/^.*[\\\/]/, '');

$("#iconName").val(imageIcon);
$("#logoName").val(imageLogo);

// console.log("iconName: ", $("#iconName").val(imageIcon));
// console.log("logoName: ", $("#logoName").val(imageLogo));


$(document).ready(function () {
    $('#imageIcon').change(function (e) {
        var fileLoad = document.getElementById("imageIcon").files[0];
        //  console.log("fileLoad: ", fileLoad.name);
        document.getElementById("iconName").value = fileLoad.name;

        var fileExt = fileLoad.name.match(/(.*)\??/i).shift().replace(/\?.*/, '').split('.').pop();
        if (fileExt != "png" && fileExt != "jpg") {
            alert("File Extension Is InValid - Only Upload PNG/JPG File")
        } else if (fileLoad.size >= 5 * 1024 * 1024 || fileLoad.size < 100) {
            alert("File size Should Be UpTo 5MB and min size is 100KB")
        } else {
            $('#submitIcon').css('display', "block");
            $('#imageIcon').css('display', "none");
            var oFReader = new FileReader();
            oFReader.readAsDataURL(fileLoad);
            oFReader.onload = function (oFREvent) {
                // console.log("oFREvent: ", oFREvent);

                document.getElementById("previewIcon").src = oFREvent.target.result;
            };
            var control = $("#imageIcon");
            control.replaceWith(control = control.clone(true));
        }
    });

    $('#imageLogo').change(function (e) {
        var fileLoad = document.getElementById("imageLogo").files[0];
        // console.log("fileLoad: ", fileLoad);
        document.getElementById("logoName").value = fileLoad.name;

        var fileExt = fileLoad.name.match(/(.*)\??/i).shift().replace(/\?.*/, '').split('.').pop();
        if (fileExt != "png" && fileExt != "jpg") {
            alert("File Extension Is InValid - Only Upload PNG/JPG File")
        } else if (fileLoad.size >= 5 * 1024 * 1024 || fileLoad.size < 100) {
            alert("File size Should Be UpTo 5MB and min size is 100KB")
        } else {
            $('#submitLogo').css('display', "block");
            $('#imageLogo').css('display', "none");
            var oFReader = new FileReader();
            oFReader.readAsDataURL(fileLoad);
            oFReader.onload = function (oFREvent) {
                //  console.log("oFREvent: ", oFREvent);

                document.getElementById("previewLogo").src = oFREvent.target.result;
            };
            var control = $("#imageLogo");
            control.replaceWith(control = control.clone(true));
        }
    });

});

function checkEditing() {
    var nameGlobal = $("#nameGlobal").val();
    var nameLocal = $("#nameLocal").val();
    var introductionGlobal = $("#introductionGlobal").val();
    var introductionlocal = $("#introductionLocal").val();
    var descriptionGlobal = $("#descriptionGlobal").val();
    var descriptionLocal = $("#descriptionLocal").val();
    var imageIcon = $("#iconName").val();
    var imageLogo = $("#logoName").val();
    var createdDate = $("#createdDate").val();
    var updateDate = $("#updateDate").val();

    var status = document.getElementById("status").value;
    var typeOf = document.getElementById("typeOf").value;
    var isPlayed = document.getElementById("isPlayed").value;

    var regX = /(<([^>]+)>)/ig;
    var content = editorEdit.getData().replace(regX, "");
    var contentType = document.getElementById("contentType").value;

    console.log("content: ", content);
    console.log("contentType ", contentType);

    //var imageIcon = document.getElementById("previewIcon").src.replace(/^.*[\\\/]/, '');
    //var imageLogo = document.getElementById("previewLogo").src.replace(/^.*[\\\/]/, '');
    var check = 1;

    $('#message-dialog').modal('show');

    // reset
    resetModal("nameGlobalModal");
    resetModal("nameLocalModal");
    //resetModal("introductionGlobalModal");
    //resetModal("introductionLocalModal");
    //resetModal("descriptionGlobalModal");
    //resetModal("descriptionLocalModal");
    resetModal("imageIconModal");
    resetModal("imageLogoModal");
    resetModal("createdDateModal");
    resetModal("updateDateModal");
    resetModal("contentModal");
    resetModal("contentTypeModal");
    resetModal("codeModal");
    resetModal("statusModal");
    resetModal("typeOfModal");
    resetModal("isPlayedModal");

    resetModal("statusModal");
    resetModal("typeOfModal");
    resetModal("isPlayedModal");

    console.log("imageIcon:", imageIcon + " " + imageIcon.toString().replace(/\s/g, '').length);
    // console.log("imageLogo:", imageLogo + " " + imageLogo.toString().replace(/\s/g, '').length);

    if (nameGlobal.toString().replace(/\s/g, '').length == 0) {
        // console.log("nameGlobal:", nameGlobal.trim() + " " + nameGlobal.toString().replace(/\s/g, '').length);
        fillModal("nameGlobalModal", "nameGlobal must be filled");
        check *= 0;
    }
    if (nameLocal.toString().replace(/\s/g, '').length == 0) {
        //  console.log("nameLocal:", nameLocal.trim() + " " + nameLocal.toString().replace(/\s/g, '').length);
        fillModal("nameLocalModal", "nameLocalModal must be filled");
        check *= 0;
    }

    if (contentType == "- UNKNOWN CODE -") {
        console.log("contentType:", contentType);
        fillModal("contentTypeModal", "contentType must be filled");
        check *= 0;
    }

    if (status == -1) {
        // console.log("contentType:", contentType);
        fillModal("statusModal", "status must be filled");
        check *= 0;
    }
    if (typeOf == -1) {
        // console.log("contentType:", contentType);
        fillModal("typeOfModal", "typeOf must be filled");
        check *= 0;
    }
    if (isPlayed == -1) {
        // console.log("contentType:", contentType);
        fillModal("isPlayedModal", "isPlayed must be filled");
        check *= 0;
    }

    //if (introductionGlobal.toString().replace(/\s/g, '').length == 0) {
    //    // console.log("introductionGlobal:", introductionGlobal.trim() + " " + introductionGlobal.toString().replace(/\s/g, '').length);
    //    fillModal("introductionGlobalModal", "introductionGlobalModal must be filled");
    //    check *= 0;
    //}
    //if (introductionlocal.toString().replace(/\s/g, '').length == 0) {
    //    // console.log("introductionlocal:", introductionlocal.trim() + " " + introductionlocal.toString().replace(/\s/g, '').length);
    //    fillModal("introductionLocalModal", "introductionLocalModal must be filled");
    //    check *= 0;
    //}
    //if (descriptionGlobal.toString().replace(/\s/g, '').length == 0) {
    //    // console.log("descriptionGlobal:", descriptionGlobal.trim() + " " + descriptionGlobal.toString().replace(/\s/g, '').length);
    //    fillModal("descriptionGlobalModal", "descriptionGlobalModal must be filled");
    //    check *= 0;
    //}
    //if (descriptionLocal.toString().replace(/\s/g, '').length == 0) {
    //    // console.log("descriptionLocal:", descriptionLocal.trim() + " " + descriptionLocal.toString().replace(/\s/g, '').length);
    //    fillModal("descriptionLocalModal", "descriptionLocalModal must be filled");
    //    check *= 0;
    //}
    //if (imageIcon.toString().replace(/\s/g, '').length == 0 || imageIcon.toString() == "0") {
    //    // console.log("imageIcon:", imageIcon + " " + imageIcon.toString().replace(/\s/g, '').length);
    //    fillModal("imageIconModal", "imageIconModal must be filled");
    //    check *= 0;
    //}
    //if (imageLogo.toString().replace(/\s/g, '').length == 0 || imageLogo.toString() == "0") {
    //    // console.log("imageLogo:", imageLogo + " " + imageLogo.toString().replace(/\s/g, '').length);
    //    fillModal("imageLogoModal", "imageLogoModal must be filled");
    //    check *= 0;
    //}
    //if (createdDate.toString().replace(/\s/g, '').length == 0) {
    //    //  console.log("createdDate:", createdDate + " " + createdDate.toString().replace(/\s/g, '').length);
    //    fillModal("createdDateModal", "createdDateModal must be filled");
    //    check *= 0;
    //}
    //if (updateDate.toString().replace(/\s/g, '').length == 0) {
    //    // console.log("updateDate:", updateDate + " " + updateDate.toString().replace(/\s/g, '').length);
    //    fillModal("updateDateModal", "updateDateModal must be filled");
    //    check *= 0;
    //}
    //if (content.toString().replace(/\s/g, '').length == 0) {
    //    //  console.log("content:", content + " " + content.toString().replace(/\s/g, '').length);
    //    fillModal("contentModal", "contentModal must be filled");
    //    check *= 0;
    //}

    if (check == 1) {
        $("#checkBtn").addClass("hide");
        $("#saveBtn").removeClass("hide");
    }

}

function closeEditing() {
    //window.location.href = "/Admin";
    console.log($("#previous-href").val());
    //window.history.back();
    window.location.href = $("#previous-href").val();
}

function fillModal(name, message) {
    $('#' + name).html(message);
    $("#" + name).removeClass("pass");
    $("#" + name).addClass("fail");
    $("." + name).removeClass("pass");
    $("." + name).addClass("fail");
}

function resetModal(name) {
    $('#' + name).html("OK");
    $("#" + name).removeClass("fail");
    $("#" + name).addClass("pass");
    $("." + name).removeClass("fail");
    $("." + name).addClass("pass");
}

function saveEditing() {
    var nameGlobal = $("#nameGlobal").val();
    var nameLocal = $("#nameLocal").val();
    var introductionGlobal = $("#introductionGlobal").val();
    var introductionlocal = $("#introductionLocal").val();
    var descriptionGlobal = $("#descriptionGlobal").val();
    var descriptionLocal = $("#descriptionLocal").val();
    var imageIcon = $("#iconName").val();
    var imageLogo = $("#logoName").val();
    var createdDate = $("#createdDate").val();
    var updateDate = $("#updateDate").val();

    var regX = /(<([^>]+)>)/ig;
    var content = editorEdit.getData().replace(regX, "");

    var contentType = document.getElementById("contentType").value;

    var status = document.getElementById("status").value;
    var typeOf = document.getElementById("typeOf").value;
    var isPlayed = document.getElementById("isPlayed").value;

    var elem = $("<div>" + content + "</div>");
    var p = elem.find("p");
    p.remove();
    console.log(elem);

    console.log("content type: ", contentType);
    // unchanged paras
    var id = $("#ID").val();
    var parentID = $("#PARENT_ID").val();
    var svId = $("#SV_ID").val();
    var code = $("#CODE").val();
    //var contentType = $("#CONTENT_TYPE").val();
    var level = $("#LEVEL").val();
    var isRead = $("#IS_READ").val();

    //var prayer = new Object();
    //prayer.NAME_GLOBAL = nameGlobal;
    //prayer.NAME_LOCAL = nameLocal;
    //prayer.DESCRIPTION_GLOBAL = descriptionGlobal;
    //prayer.DESCRIPTION_LOCAL = descriptionLocal;
    //prayer.INTRODUCTION_GLOBAL = introductionGlobal;
    //prayer.INTRODUCTION_LOCAL = introductionlocal;
    //prayer.ICON = imageIcon;
    //prayer.LOGO = imageLogo;
    //prayer.CREATED_DATE = createdDate;
    //prayer.UPDATE_DATE = updateDate;
    //prayer.CONTENT = content.toString();

    //// unchanged paras
    //prayer.ID = id;
    //prayer.PARENT_ID = parentID;
    //prayer.SV_ID = svId;
    //prayer.CODE = code;
    //prayer.CONTENT_TYPE = contentType;
    //prayer.LEVEL = level;
    //prayer.IS_READ = isRead;
    //prayer.ACTION = "editing";

    var formData = new FormData();
    formData.append('__RequestVerificationToken', $('input[name=__RequestVerificationToken]').val());
    formData.append('icon', $('#imageIcon')[0].files[0]);
    formData.append('logo', $('#imageLogo')[0].files[0]);

    formData.append('nameGlobal', nameGlobal);
    formData.append('nameLocal', nameLocal);
    formData.append('descriptionGlobal', descriptionGlobal);
    formData.append('descriptionLocal', descriptionLocal);
    formData.append('introductionGlobal', introductionGlobal);
    formData.append('introductionlocal', introductionlocal);
    formData.append('icon', imageIcon);
    formData.append('logo', imageLogo);
    formData.append('fromDate', createdDate);
    formData.append('toDate', updateDate);
    formData.append('content', content);

    formData.append('id', id);
    formData.append('parentId', parentID);
    formData.append('serviceId', svId);
    formData.append('code', code);
    formData.append('contentType', contentType);
    formData.append('level', level);
    formData.append('isRead', isRead);
    formData.append('action', "editing");

    formData.append('status', status);
    formData.append('typeOf', typeOf);
    formData.append('isPlayed', isPlayed);

    $.ajax({
        url: urlConfig("/Admin/EditingAction"),
        type: "POST",
        processData: false,
        contentType: false,
        //data: {
        //    __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        //    //packet: JSON.stringify(prayer),
        //    packet: formData,

        //}
        data: formData
    }).success(function (data) {
        console.log(data);
        $('#response-dialog').modal('show');
        $('#response-content').html(data.error_content);
        $("#redirection").val(data.href);
    });
}