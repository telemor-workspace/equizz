﻿$(".modify-action").on("click", function () {
    var id = $(this).attr("value");
    // console.log("ID: ", id);
    $("#admin-editing-from-" + id).submit();
})

$(".adding-action").on("click", function () {
    // console.log("adding");
    $("#admin-adding-from").submit();
})

$(".delete-action").on("click", function () {
    $("#delete-dialog").modal("show");
    $("#delete-content").html("Are you sure?");

    var id = $(this).attr("value");
    $("#idForDelete").val(id);
   //  console.log("ID: ", id);
    //$("#admin-delete-from-" + id).submit();
})

$("#acceptDeleteBtn").on("click", function () {
    var id = $("#idForDelete").val();
    // console.log("ID: ", id);
    //$("#admin-delete-from-" + id).submit();
    $("#delete-dialog").modal("hide");

    $.ajax({
        url: urlConfig("/Admin/DeleteAction"),
        type: "POST",
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
            id: id,
        }
    }).success(function (data) {
       //  console.log(data);
        $('#response-dialog').modal('show');
        $('#response-content').html(data.error_content);
        location.reload();
    });
})