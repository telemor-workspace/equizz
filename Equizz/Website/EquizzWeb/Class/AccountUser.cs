﻿using FunQuizWeb.Controllers;
using FunQuizWeb.WsEquizz;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class AccountUser
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("msisdn")]
        public string msisdn { get; set; }

        [JsonProperty("users")]
        public string users { get; set; }

        [JsonProperty("serviceId")]
        public string serviceId { get; set; }

        [JsonProperty("createdDate")]
        public string createdDate { get; set; }
        [JsonProperty("updateDate")]
        public string updateDate { get; set; }
        [JsonProperty("isActive")]
        public string isActive { get; set; }
        [JsonProperty("channel")]
        public string channel { get; set; }
        [JsonProperty("freeSpin")]
        public string freeSpin { get; set; }

        [JsonProperty("totalSpin")]
        public string totalSpin { get; set; }
        [JsonProperty("role")]
        public string role { get; set; }
        [JsonProperty("language")]
        public string language { get; set; }

        [JsonProperty("isLock")]
        public string isLock { get; set; }

        [JsonProperty("totalFalse")]
        public string totalFalse { get; set; }

        [JsonProperty("timeLock")]
        public string timeLock { get; set; }

        [JsonProperty("lastUpdate")]
        public string lastUpdate { get; set; }

        [JsonProperty("lastLogin")]
        public string lastLogin { get; set; }

        [JsonProperty("point")]
        public string point { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("ranking")]
        public string ranking { get; set; }

        [JsonProperty("picture")]
        public string picture { get; set; }
        public string realPicture { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("birthday")]
        public string birthday { get; set; }

        [JsonProperty("fullname")]
        public string fullname { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public AccountUser() { }

        public AccountUser(string json) : this(JObject.Parse(json))
        { }

        public AccountUser(JObject jObject)
        {
            if (jObject != null)
            {
                id = (string)jObject["id"];
                msisdn = (string)jObject["msisdn"];
                users = (string)jObject["user"];
                serviceId = (string)jObject["sv_id"];
                createdDate = (string)jObject["created_date"];
                updateDate = (string)jObject["update_date"];
                isActive = (string)jObject["is_active"];
                channel = (string)jObject["channel"];
                freeSpin = (string)jObject["free_spin"];
                totalSpin = (string)jObject["total_spin"];

                role = (string)jObject["role"];
                language = (string)jObject["language"];

                isLock = (string)jObject["is_lock"];
                totalFalse = (string)jObject["total_false"];
                timeLock = (string)jObject["time_lock"];

                lastUpdate = (string)jObject["last_update"];
                lastLogin = (string)jObject["last_login"];
                point = (string)jObject["point"];
                status = (string)jObject["status"];
                birthday = (string)jObject["birthday"];
                fullname = (string)jObject["fullname"];
                email = (string)jObject["email"];
                picture = UtilsController.GetContentPath.AccountPath + (string)jObject["picture"];
                realPicture = (string)jObject["picture"];
                ranking = (string)jObject["ranking"];

            }
        }

        public AccountUser(accountInfo account)
        {
            if (account != null)
            {
                id = account.accountId;
                msisdn = account.msisdn;
                users = account.users;
                serviceId = account.serviceId;
                createdDate = account.createDate;
                updateDate = account.updateDate;
                isActive = account.isActive;
                channel = account.channel;
                freeSpin = account.freeSpin;
                totalSpin = account.totalSpin;

                role = account.role;
                language = account.language;

                isLock = account.isLock;
                totalFalse = account.totalFalse;
                timeLock = account.timeLock;

                lastUpdate = account.lastUpdate;
                lastLogin = account.lastLogin;
                point = account.point;
                status = account.status;
                birthday = account.birthday;
                fullname = account.fullName;
                email = account.email;
                picture = UtilsController.GetContentPath.AccountPath + account.picture;
                realPicture = account.picture;
                ranking = account.ranking;

            }
        }
    }
    public class AccountUserResponse
    {
        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }


        [JsonProperty("data")]
        public List<AccountUser> data { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public AccountUserResponse() { }
        public AccountUserResponse(string json) : this(JObject.Parse(json)) { }
        public AccountUserResponse(JObject jObject)
        {
            if (jObject != null)
            {
                var list = jObject["data"];
                if (list != null && list.HasValues)
                {
                    data = new List<AccountUser>();
                    JArray a = (JArray)list;
                    foreach (JObject o in a.Children<JObject>())
                    {
                        data.Add(new AccountUser(o));
                    }
                }
            }
        }
    }

    public class AccountUsers
    {
        public List<AccountUser> accounts { get; set; }

        public AccountUsers() { }
        public AccountUsers(accountInfo[] accountsData)
        {
            accounts = new List<AccountUser>();
            if (accountsData != null)
            {
                foreach (accountInfo acc in accountsData)
                {
                    accounts.Add(new AccountUser(acc));
                }
            }
        }
    }
}