﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class Answer
    {
        public Game answer { get; set; }
        public Game rightAnswer { get; set; }
    }
}