﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class Account
    {
        public String serviceId { get; set; }
        public String username { get; set; }
        public String password { get; set; }

        public UserProfile profile { get; set; }
        public String responsibility { get; set; }
    }
}