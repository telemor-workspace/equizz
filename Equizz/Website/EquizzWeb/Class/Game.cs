﻿using FunQuizWeb.Controllers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FunQuizWeb.WsEquizz;

namespace FunQuizWeb.Class
{
    public class Game
    {
        public String id { get; set; }
        public String parentId { get; set; }
        public String serviceId { get; set; }
        public String code { get; set; }
        public String name { get; set; }
        public String nameGlobal { get; set; }
        public String nameLocal { get; set; }
        public String description { get; set; }
        public String descriptionGlobal { get; set; }
        public String descriptionLocal { get; set; }
        public String introduction { get; set; }
        public String introductionGlobal { get; set; }
        public String introductionLocal { get; set; }
        public String icon { get; set; }
        public String logo { get; set; }
        public String content { get; set; }
        public String contentType { get; set; }
        public String createdDate { get; set; }
        public String updateDate { get; set; }
        public String fromDate { get; set; }
        public String toDate { get; set; }
        public String provinceId { get; set; }
        public String topicId { get; set; }
        public String level { get; set; }
        public String isPlayed { get; set; }

        // 1 publish - 0 draft
        public String status { get; set; }

        // to distinguish (1) admin and (0) user
        public String typeOf { get; set; }
        public String note { get; set; }

        public String realIcon { get; set; }
        public String realLogo { get; set; }



        public Game() { }

        public Game(string json) : this(JObject.Parse(json))
        { }

        public Game(JObject jObject)
        {
            if (jObject != null)
            {
                id = (string)jObject["id"];
                parentId = (string)jObject["parentId"];
                serviceId = (string)jObject["serviceId"];
                code = (string)jObject["code"];
                name = (string)jObject["name"];
                nameGlobal = (string)jObject["nameGlobal"];
                nameLocal = (string)jObject["nameLocal"];
                description = (string)jObject["description"];
                descriptionGlobal = (string)jObject["descriptionGlobal"];
                descriptionLocal = (string)jObject["descriptionLocal"];
                introduction = (string)jObject["introduction"];
                introductionGlobal = (string)jObject["introductionGlobal"];
                introductionLocal = (string)jObject["introductionLocal"];
                level = (string)jObject["level"];

                icon = UtilsController.GetContentPath.GamesPath + (string)jObject["icon"];
                logo = UtilsController.GetContentPath.GamesPath + (string)jObject["logo"];

                content = (string)jObject["content"];
                contentType = (string)jObject["contentType"];


                fromDate = (string)jObject["fromDate"];
                toDate = (string)jObject["toDate"];
                createdDate = (string)jObject["createdDate"];
                updateDate = (string)jObject["updateDate"];

                provinceId = (string)jObject["provinceId"];
                topicId = (string)jObject["topicId"];

                // check user played or not
                isPlayed = (string)jObject["isPlayed"];

                // draft || publish
                status = (string)jObject["status"];

                // answer || result || game add coin || normal
                typeOf = (string)jObject["typeOf"];
                note = (string)jObject["note"];

            }
        }

        public Game(game gameData)
        {
            if (gameData != null)
            {
                id = gameData.id;
                parentId = gameData.parentId;
                serviceId = gameData.serviceId;
                code = gameData.code;
                name = gameData.name;
                nameGlobal = gameData.nameGlobal;
                nameLocal = gameData.nameLocal;
                description = gameData.description;
                descriptionGlobal = gameData.descriptionGlobal;
                descriptionLocal = gameData.descriptionLocal;
                introduction = gameData.introduction;
                introductionGlobal = gameData.introductionGlobal;
                introductionLocal = gameData.introductionLocal;
                level = gameData.level;


                icon = UtilsController.GetContentPath.GamesPath + gameData.icon;
                logo = UtilsController.GetContentPath.GamesPath + gameData.logo;
                realIcon = gameData.icon;
                realLogo = gameData.logo;

                //content = gameData.content;
                contentType = gameData.contentType;

                if (contentType == UtilsController.GetContentType.PICTURE_FILE
                    || contentType == UtilsController.GetContentType.VIDEO_FILE
                    || contentType == UtilsController.GetContentType.AUDIO_FILE)
                {
                    content = UtilsController.GetContentPath.GamesPath + gameData.content;
                }
                else
                {
                    content = gameData.content;
                }

                fromDate = gameData.fromDate;
                toDate = gameData.toDate;
                createdDate = gameData.createdDate;
                updateDate = gameData.updateDate;

                // check user played or not
                isPlayed = gameData.isPlayed;

                // draft || publish
                status = gameData.status;

                // answer || result || game add coin || normal
                typeOf = gameData.typeOf;
                note = gameData.note;

            }
        }
    }
    public class Games
    {
        public List<Game> games { get; set; }
        public Games() { }
        public Games(string json) : this(JObject.Parse(json)) { }
        public Games(JObject jObject)
        {
            if (jObject != null)
            {
                var list = jObject["data"];
                if (list != null && list.HasValues)
                {
                    games = new List<Game>();
                    JArray a = (JArray)list;
                    foreach (JObject o in a.Children<JObject>())
                    {
                        games.Add(new Game(o));
                    }
                }
            }
        }
        public Games(game[] gamesData)
        {
            games = new List<Game>();
            if (gamesData != null && gamesData.Length > 0)
            {
                foreach (game game in gamesData)
                {
                    games.Add(new Game(game));
                }
            }

        }
    }
}
