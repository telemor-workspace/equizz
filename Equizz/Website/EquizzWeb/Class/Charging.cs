﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class Charging
    {
        // only for BuyCourse
        public string requestId { get; set; }

        // only for register sub
        public string responseCode { get; set; }
        public string requestID { get; set; }
        public string resultCode { get; set; }
        public string cmd { get; set; }
        public string money { get; set; }
        public string source { get; set; }
        public string otpType { get; set; }



        public Charging() { }
        public Charging(WsEquizz.response result)
        {
            if (result != null)
            {
                resultCode = result.resultCode;
            }
        }
    }
}