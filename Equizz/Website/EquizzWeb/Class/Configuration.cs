﻿using FunQuizWeb.WsEquizz;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class Configuration
    {
        public  String id { get; set; }
        public String para_key { get; set; }
        public String para_value { get; set; }
        public String service_id { get; set; }

        public Configuration() { }

        public Configuration(string json) : this(JObject.Parse(json))
        { }

        public Configuration(JObject jObject)
        {
            if (jObject != null)
            {
                id = (string)jObject["id"];
                para_key = (string)jObject["para_key"];
                para_value = (string)jObject["para_value"];
                service_id = (string)jObject["service_id"];
            }
        }

        public Configuration(configuration configurationData)
        {
            if (configurationData != null)
            {
                id = configurationData.id;
                para_key = configurationData.para_key;
                service_id = configurationData.service_id;
                para_value = configurationData.para_value;
            }
        }

    }

    public class Configurations
    {
        public List<Configuration> configurations { get; set; }
        public Configurations() { }
        public Configurations(string json) : this(JObject.Parse(json)) { }
        public Configurations(JObject jObject)
        {
            if (jObject != null)
            {
                var list = jObject["data"];
                if (list != null && list.HasValues)
                {
                    configurations = new List<Configuration>();
                    JArray a = (JArray)list;
                    foreach (JObject o in a.Children<JObject>())
                    {
                        configurations.Add(new Configuration(o));
                    }
                }
            }
        }
        public Configurations(configuration[] configurationsData)
        {
            configurations = new List<Configuration>();
            if (configurationsData != null && configurationsData.Length > 0)
            {
                foreach (configuration config in configurationsData)
                {
                    configurations.Add(new Configuration(config));
                }
            }

        }
    }
}