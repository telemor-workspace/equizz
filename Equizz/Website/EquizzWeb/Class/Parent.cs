﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class Parent
    {
        public String index { get; set; }
        public String level { get; set; }
        public String name { get; set; }
        public Game choose {get; set; }
        public Games data { get; set; }
    }
}