﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class GetRequest : Posting
    {
        public String msisdn { get; set; }
        public String users { get; set; }
        public String rowsOnPage { get; set; }
        public String seqPage { get; set; }
        public String isGetContent { get; set; }
        public String top { get; set; }
        public String level { get; set; }
        public String language { get; set; }
        public String name { get; set; }
        public String proviceId { get; set; }
        public String topicId { get; set; }

        public String fromDate { get; set; }
        public String toDate { get; set; }
        public String isPlayed { get; set; }

        // for FunQuiz
        public String id { get; set; }
        public String parentId { get; set; }

        //user or admin depend on special case
        public String type { get; set; }
    }
}