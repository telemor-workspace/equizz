﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class AppResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public string Message { get; set; }

        // only for BuyCourse
        public string requestID { get; set; }

        // only for register sub
        public string responseCode { get; set; }
        public string newsId { get; set; }
        public string rowsOnPage { get; set; }
        public string seqPage { get; set; }
        public string totalPage { get; set; }
        public string totalSub { get; set; }

        public AppResponse() { }
        public AppResponse(string json)
        {
            JObject jObject = JObject.Parse(json);
            if (jObject != null)
            {
                status = (string)jObject["status"];
                message = (string)jObject["message"];
                Message = (string)jObject["Message"];
                newsId = (string)jObject["newsId"];
                rowsOnPage = (string)jObject["rowsOnPage"];
                seqPage = (string)jObject["seqPage"];
                totalPage = (string)jObject["totalPage"];
                totalSub = (string)jObject["totalSub"];


                requestID = (string)jObject["requestId"];
                responseCode = (string)jObject["responseCode"];
            }
        }
    }
}