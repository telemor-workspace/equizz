﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class AccountRequest : Posting
    {
        public String msisdn { get; set; }
        public String users { get; set; }
        public String pass { get; set; }
        public String passnew { get; set; }
        //public String serviceid { get; set; }
        public String command { get; set; }
        public String channel { get; set; }

        //user or admin depend on special case
        public String type { get; set; }

        public String codeBuy { get; set; }
        public String requestId { get; set; }
        public String otp { get; set; }
        public String subServiceCode { get; set; }

        // for create a new account

    }
}