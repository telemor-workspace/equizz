﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class.Http
{
    public class ProfileRequest : Posting
    {
        public String msisdn { get; set; }
        public String users { get; set; }
        public String MSISDN { get; set; }
        public String FULLNAME { get; set; }
        public String ADDRESS { get; set; }
        public String EMAIL { get; set; }
        public String DESCRIPTION_ { get; set; }
        public String PICTURE { get; set; }
        public String BIRTHDAY { get; set; }
    }
}