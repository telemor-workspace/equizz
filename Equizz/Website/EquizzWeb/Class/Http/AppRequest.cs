﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Class
{
    public class AppRequest : Posting
    {
        public String msisdn { get; set; }
        public String users { get; set; }
        public String pass { get; set; }
        public String passnew { get; set; }
        //public String serviceid { get; set; }
        public String command { get; set; }
        public String channel { get; set; }
        public String rowsOnPage { get; set; }
        public String seqPage { get; set; }
        public String isGetContet { get; set; }
        public String top { get; set; }
        public String level { get; set; }
        public String language { get; set; }
        public String name { get; set; }
        public String proviceId { get; set; }
        public String topicId { get; set; }
        
        public String fromDate { get; set; }
        public String toDate { get; set; }
        public String isShow { get; set; }


        // reading
        public String NEWS_ID { get; set; }
        public String newsId { get; set; }

        // for ADD/EDIT
        public String ID { get; set; }
        // edit or update
        public String TYPE { get; set; }
        public String PARENT_ID { get; set; }
        public String CODE { get; set; }
        public String NAME_GLOBAL { get; set; }
        public String NAME_LOCAL { get; set; }
        public String DESCRIPTION_GLOBAL { get; set; }
        public String DESCRIPTION_LOCAL { get; set; }
        public String INTRODUCTION_GLOBAL { get; set; }
        public String INTRODUCTION_LOCAL { get; set; }
        public String ICON { get; set; }
        public String LOGO { get; set; }
        public String CONTENT { get; set; }
        public String CONTENT_TYPE { get; set; }
        public String PROVINCE_ID { get; set; }
        public String TOPIC_ID { get; set; }
        public String FROM_DATE { get; set; }
        public String TO_DATE { get; set; }
        public String IS_SHOW { get; set; }
        public String NOTE { get; set; }

        //user or admin depend on special case
        public String type { get; set; }



        // Old service
        //public String categoryCode { get; set; }
        //public String courseId { get; set; }
        //public String lessonId { get; set; }

        public String codeBuy { get; set; }
        public String requestId { get; set; }
        public String otp { get; set; }
        public String subServiceCode { get; set; }
    }
}