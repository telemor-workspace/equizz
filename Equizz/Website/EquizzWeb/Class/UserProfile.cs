﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FunQuizWeb.Controllers;

namespace FunQuizWeb.Class
{
    public class UserProfile : Posting
    {
        public String id { get; set; }
        public String usersId { get; set; }
        public String msisdn { get; set; }
        public String users { get; set; }
        public String fullname { get; set; }
        public String address { get; set; }
        public String email { get; set; }
        public String description { get; set; }
        public String picture { get; set; }
        public String birthday { get; set; }
        public String totalScore { get; set; }
        public String ranking { get; set; }


        public UserProfile() { }
        public UserProfile(string json) : this(JObject.Parse(json))
        { }

        public UserProfile(JObject jObject)
        {
            if (jObject != null)
            {
                id = (string)jObject["id"];
                usersId = (string)jObject["usersId"];
                users = (string)jObject["users"];
                msisdn = (string)jObject["msisdn"];
                //serviceid = (string)jObject["serviceid"];
                fullname = (string)jObject["fullname"];
                address = (string)jObject["address"];
                email = (string)jObject["email"];
                description = (string)jObject["description"];
                picture = UtilsController.GetContentPath.AccountPath + (string)jObject["picture"];
                birthday = (string)jObject["birthday"];
                totalScore = (string)jObject["totalScore"];
                ranking = (string)jObject["ranking"];

            }
        }
    }

    public class UserProfileResponse
    {
        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }


        [JsonProperty("data")]
        public List<UserProfile> data { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public UserProfileResponse() { }
        public UserProfileResponse(string json) : this(JObject.Parse(json)) { }
        public UserProfileResponse(JObject jObject)
        {
            if (jObject != null)
            {
                status = (string)jObject["status"];
                message = (string)jObject["message"];
                var list = jObject["data"];
                if (list != null && list.HasValues)
                {
                    data = new List<UserProfile>();
                    JArray a = (JArray)list;
                    foreach (JObject o in a.Children<JObject>())
                    {
                        data.Add(new UserProfile(o));
                    }
                }
            }
        }
    }

    public class Interest
    {
        public String id { get; set; }
        public String code { get; set; }
        public String name { get; set; }
        public String picture { get; set; }

        public Interest() { }
        public Interest(JObject jObject)
        {
            if (jObject != null)
            {
                id = (string)jObject["id"];
                code = (string)jObject["code"];
                name = (string)jObject["name"];
                picture = UtilsController.GetContentPath.AccountPath + (string)jObject["picture"];
            }
        }
    }
}