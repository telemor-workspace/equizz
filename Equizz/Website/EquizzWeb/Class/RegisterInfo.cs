﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FunQuizCore.Model
{
    public class RegisterInfo
    {
        [JsonProperty("register_id")]
        public string register_id { get; set; }

        [JsonProperty("msisdn")]
        public string msisdn { get; set; }

        [JsonProperty("product_name")]
        public string product_name { get; set; }
        [JsonProperty("register_time")]
        public string register_time { get; set; }
        [JsonProperty("number_spin")]
        public string number_spin { get; set; }

        [JsonProperty("extend_status")]
        public string extend_status { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("expire_time")]
        public string expire_time { get; set; }
        [JsonProperty("last_extend")]
        public string last_extend { get; set; }
        [JsonProperty("played_times")]
        public string played_times { get; set; }

        [JsonProperty("end_time")]
        public string end_time { get; set; }
        [JsonProperty("already_charged")]
        public string already_charged { get; set; }
        [JsonProperty("play_enable")]
        public string play_enable { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public RegisterInfo() { }

        public RegisterInfo(string json) : this(JObject.Parse(json))
        { }

        public RegisterInfo(JObject jObject)
        {
            if (jObject != null)
            {
                register_id = (string)jObject["register_id"];
                msisdn = (string)jObject["msisdn"];
                product_name = (string)jObject["product_name"];
                register_time = (string)jObject["register_time"];
                number_spin = (string)jObject["number_spin"];
                extend_status = (string)jObject["extend_status"];
                status = (string)jObject["status"];
                expire_time = (string)jObject["expire_time"];
                last_extend = (string)jObject["last_extend"];
                played_times = (string)jObject["played_times"];
                end_time = (string)jObject["end_time"];
                already_charged = (string)jObject["already_charged"];
                play_enable = (string)jObject["play_enable"];
            }
        }
    }

    public class RegisterInfoResponse
    {
        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }


        [JsonProperty("data")]
        public List<RegisterInfo> data { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public RegisterInfoResponse() { }
        public RegisterInfoResponse(string json) : this(JObject.Parse(json)) { }
        public RegisterInfoResponse(JObject jObject)
        {
            if (jObject != null)
            {
                status = (string)jObject["status"];
                message = (string)jObject["message"];
                var list = jObject["data"];
                if (list != null && list.HasValues)
                {
                    data = new List<RegisterInfo>();
                    JArray a = (JArray)list;
                    foreach (JObject o in a.Children<JObject>())
                    {
                        data.Add(new RegisterInfo(o));
                    }
                }
            }
        }
    }
}
