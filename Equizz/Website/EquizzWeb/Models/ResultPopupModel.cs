﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class ResultPopupModel
    {
        public Games questions { get; set; }
        public List<Answer> listAnswers { get; set; }
        public Boolean finished { get; set; }
        public String charge { get; set; }
        public Boolean result { get; set; }
    }
}