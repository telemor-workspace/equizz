﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class AnswerPopupModel
    {
        public Games questions { get; set; }
        public Game answer { get; set; }
    }
}