﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class AdminHomeModel
    {
        public String level { get; set; }
        public List<Parent> parents { get; set; }
        public Games recent { get; set; }
        public String pageNow { get; set; }
        public String pageTotal { get; set; }
    }
}