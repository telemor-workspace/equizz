﻿using FunQuizCore.Model;
using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class AccountModel
    {
        public AccountUser profile { get; set; }
        public List<String> services { get; set; }
    }
}