﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class WinsModel
    {
        public Games categories { get; set; }
        public AccountUsers topWon { get; set; }
        public AccountUsers topPlay { get; set; }
        public AccountUser recent { get; set; }
    }
}