﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class MinigamesModel
    {
        public Games topics { get; set; }
        public Game topic { get; set; }

        public Games games { get; set; }
        public Game game { get; set; }
        public String timeExpired { get; set; }
        public long timeLeft { get; set; }
        public String timeNow { get; set; }
        public String questionNow { get; set; }
        public Game question { get; set; }
        public Games questions { get; set; }
        public Games answers { get; set; }
        public List<AccountUser> accounts { get; set; }


    }
}