﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class AdminConfigModel
    {
        public Configurations configurations { get; set; }
        public Configuration rewardEnable { get; set; }

    }
}