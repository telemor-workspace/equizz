﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FunQuizWeb.Class;

namespace FunQuizWeb.Models
{
    public class PartialModel
    {
        public String response { get; set; }
        public String codeBuy { get; set; }
        public String href { get; set; }
        public AccountUser profile { get; set; }
    }
}