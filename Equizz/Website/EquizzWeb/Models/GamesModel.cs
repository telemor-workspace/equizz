﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class GamesModel
    {
        public Games categories { get; set; }
        public Games questions { get; set; }
        public Game question { get; set; }
        public String questionNow { get; set; }
        public Games answers { get; set; }
        public Game result { get; set; }
        
        public Games games { get; set; }
        public Games moreGames { get; set; }
        public Game game { get; set; }
        public String page { get; set; }
        public String type { get; set; }
    }
}