﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class TestsModel
    {
        public Games categories { get; set; }
        public Games types { get; set; }
        public Games games { get; set; }
        public Game game { get; set; }
        public String page { get; set; }
        public String type { get; set; }
    }
}