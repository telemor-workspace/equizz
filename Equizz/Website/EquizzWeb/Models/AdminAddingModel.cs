﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FunQuizWeb.Class;

namespace FunQuizWeb.Models
{
    public class AdminAddingModel
    {
        public String level { get; set; }
        public Game parent { get; set; }
        public List<String> types { get; set; }
        public String previous_href { get; set; }

    }
}