﻿using FunQuizWeb.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FunQuizWeb.Models
{
    public class MinigamePartialModel
    {
        public Game game { get; set; }
        public Games questions { get; set; }
        public Game question { get; set; }
        public Games answers { get; set; }
        public Game result { get; set; }
        public string questionNow { get; set; }
    }
}