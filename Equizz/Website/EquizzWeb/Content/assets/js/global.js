﻿resize();
$(window).resize(function () {
    resize();
});

function resize() {
    var size = $(window).width();
    //console.log("size: ", size);
    if ($(window).width() < 991) {
        $(".content").removeClass("large-list-games");
        $(".content").addClass("small-list-games");
        $(".horizon-prev").addClass("hide");
        $(".horizon-next").addClass("hide");

    } else {
        $(".content").addClass("large-list-games");
        $(".content").removeClass("small-list-games");
        $(".horizon-prev").removeClass("hide");
        $(".horizon-next").removeClass("hide");
    }
}

function checkForRedirect(a) {
    var newurl = "";
    //Or, retrieve the id
    newurl = a.getAttribute("id");
    console.log("href: ", newurl);

    // check for redirect
    $.ajax({
        url: urlConfig("/Game/CheckToPlay"),
        type: "POST",
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        },
        success: function (data) {
            console.log(data);
            if (data.error_code == "0") {
                //redirect 
                location.href = newurl;
            }
            else if (data.error_code == "-21") {
                //$("#buy-code-modal").modal("show");
                //$("#buy-code-content").html(data.error_content);
                $.ajax({
                    url: urlConfig("/Partial/BuyCode"),
                    data: {
                        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                    },
                    type: "POST"
                }).success(function (data) {
                    $('#myModalContent').html(data);
                    $('#myModal').modal({ "backdrop": "static", keyboard: true });
                    $('#myModal').modal('show');
                });
            } else {
                $("#response-modal").modal("show");
                $("#response-modal-content").html(data.error_content);
            }
        }
    });
}

$(".invite-btn").on("click", function () {
    $.ajax({
        url: urlConfig("/Partial/InviteFriend"),
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        },
        type: "POST"
    }).success(function (data) {
        $('#myModalContent').html(data);
        $('#myModal').modal({ "backdrop": "static", keyboard: true });
        $('#myModal').modal('show');
    });
})