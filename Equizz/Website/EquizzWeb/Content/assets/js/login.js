﻿// Get the input field
var input = document.getElementById("password");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();

        // Trigger the button element with a click
        document.getElementById("login_button").click();
    }
});

// Get the input field
var inputphonenumber = document.getElementById("phonenumber");

// Execute a function when the user releases a key on the keyboard
inputphonenumber.addEventListener("keyup", function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();

        // Trigger the button element with a click
        document.getElementById("login_button").click();
    }
});

$(".loginbtn").on("click", function (e) {
    e.preventDefault();

    var phonenumber = $("#phonenumber").val();
    var password = $("#password").val();

    //$("#login-form").submit();

    $.ajax({
        url: urlConfig("/Account/LoginAction"),
        type: "POST",
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
            phonenumber: phonenumber,
            password: password,
        },
        success: function (data) {
            console.log(data);
            if (data.error_code == "0") {
                window.location.href = "/Home"
            } else {
                $("#response-modal").modal("show");
                $("#response-modal-content").html(data.error_content);
            }
        }
    });
})

$("#phonenumber").on("input", function () {
    var password = $("#password").val();
    var phonenumber = $("#phonenumber").val();

    if (password != "" && phonenumber.length > 0) {
        $(".loginbtn").removeClass("button-disable");
    } else {
        $(".loginbtn").addClass("button-disable");
    }
})

$("#password").on("input", function () {
    var password = $("#password").val();
    var phonenumber = $("#phonenumber").val();

    if (phonenumber != "" && password.length > 0) {
        $(".loginbtn").removeClass("button-disable");
    } else {
        $(".loginbtn").addClass("button-disable");
    }
})


$("#forgotPassword").on("click", function (e) {
    e.preventDefault();
    //$("#login-form").submit();
    $('#watingModal').modal('hide');
    $('#watingModal').modal('show');
    $.ajax({
        url: urlConfig("/Partial/ForgotPass"),
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        },
        type: "POST"
    }).success(function (data) {
        $('#watingModal').modal('hide');
        $('#myModalContent').html(data);
        $('#myModal').modal({ "backdrop": "static", keyboard: true });
        $('#myModal').modal('show');
    });
})