﻿var onloadCallback = function () {
    var captRegister = grecaptcha.render('html_element', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            //$("#password-box").show();
            document.getElementById("captcha").value = grecaptcha.getResponse(captRegister);
            $(".signupbtn").removeClass("button-disable");
        },
        'expired-callback': function (response) {
            $("#password-box").hide();
            document.getElementById("captcha").value = "";
            $(".signupbtn").addClass("button-disable");
        },
    });

    var captInvite = grecaptcha.render('invite_html_element', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            //$("#password-box").show();
            document.getElementById("captcha").value = grecaptcha.getResponse(captInvite);
            var phonenumber = $("#friendNumber").val();
            if (phonenumber != "" && phonenumber != null) {
                $('.inviteBtn').prop('disabled', false);
            } else {
                $('.inviteBtn').prop('disabled', true);
            }
        },
        'expired-callback': function (response) {
            document.getElementById("captcha").value = "";
            $('.inviteBtn').prop('disabled', true);

        },
    });

    var captChangePass = grecaptcha.render('ChangePass_html', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            document.getElementById("captcha").value = grecaptcha.getResponse(captChangePass);
            var oldPass = $("#oldPass").val();
            var newPass = $("#newPass").val();
            var confirm = $("#confirmPass").val();

            if (oldPass != "" && newPass != "" && confirm != "")
                $('#acceptChangePassBtn').prop('disabled', false);
        },
        'expired-callback': function (response) {
            $("#password-box").hide();
            document.getElementById("captcha").value = "";
            $('#acceptChangePassBtn').prop('disabled', true);
        },
    });

    var captExchange = grecaptcha.render('exchange_html_element', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            document.getElementById("captcha").value = grecaptcha.getResponse(captExchange);
            $(".btn-accept-nav-modal").removeClass("button-disable");
        },
        'expired-callback': function (response) {
            document.getElementById("captcha").value = "";
            $(".btn-accept-nav-modal").addClass("button-disable");
        },
    });

    var captGetpass = grecaptcha.render('GetPass_html', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            //$("#password-box").show();
            document.getElementById("captcha").value = grecaptcha.getResponse(captGetpass);
            var phone = $("#phoneGetPass").val();
            if (phone != "")
                $('#acceptForgotPassBtn').prop('disabled', false);
        },
        'expired-callback': function (response) {
            $("#password-box").hide();
            document.getElementById("captcha").value = "";
            $('#acceptForgotPassBtn').prop('disabled', true);

        },
    });
};