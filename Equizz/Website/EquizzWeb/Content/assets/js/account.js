﻿//$(".choosePlan").on("click", function () {
//    var plan = $(this).attr("value");
//    console.log("plan: ", plan);

//    $.ajax({
//        url: urlConfig("/Account/RegisterAction"),
//        type: "POST",
//        data: {
//            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
//            captcha: captcha,
//            packettype: packetType,
//            email: email,
//            phonenumber: phonenumber,
//        },
//        success: function (data) {
//            console.log(data);
//            $("#choose-plan-modal").modal("hide");

//            if (data.error_code == "0") {
//                $("#active-code-modal").modal("show");
//            } else {
//                $("#response-modal").modal("show");
//                $("#response-modal-content").html(data.error_content);
//            }
//        }
//    });
//})


$(".exchangebtn").on("click", function () {
    console.log("exchange");
    $("#exchange-modal").modal("show");
    $.ajax({
        url: urlConfig("/Partial/Exchange"),
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        },
        type: "POST"
    }).success(function (data) {
        $('#myModalContent').html(data);
        $('#myModal').modal({ "backdrop": "static", keyboard: true });
        $('#myModal').modal('show');
    });
})

$(".changePass").on("click", function (e) {
    e.preventDefault();
    console.log("changePass");
    $.ajax({
        url: urlConfig("/Partial/ChangePass"),
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        },
        type: "POST"
    }).success(function (data) {
        $('#myModalContent').html(data);
        $('#myModal').modal({ "backdrop": "static", keyboard: true });
        $('#myModal').modal('show');
    });
})

$("#personal-name").on("input", function () {
    var oldname = $("#name-data").val();
    var newName = $(this).val();

    if (oldname != newName && newName != "") {
        $('#updateBtn').prop('disabled', false);
        $('#updateBtn').removeClass("check-sub-dim");

    } else {
        $('#updateBtn').prop('disabled', true);
        $('#updateBtn').addClass("check-sub-dim");

    }
})

$("#personal-birthday").on("input", function () {
    var oldbirthday = $("#birthday-data").val();
    var newbirthday = $(this).val();

    if (oldbirthday != newbirthday && newbirthday != "") {
        $('#updateBtn').prop('disabled', false);
        $('#updateBtn').removeClass("check-sub-dim");
    } else {
        $('#updateBtn').prop('disabled', true);
        $('#updateBtn').addClass("check-sub-dim");

    }
})

$('#image').change(function () {
    console.log("image change");
    $('#updateBtn').prop('disabled', false);
    $('#updateBtn').removeClass("check-sub-dim");
});


$("#updateBtn").on("click", function (e) {
    e.preventDefault();
    //var image = document.getElementById("image").files[0];
    //console.log(image);
    var fullname = $("#personal-name").val();
    console.log(name);
    var birthday = $("#personal-birthday").val();
    console.log(birthday);

    var formData = new FormData();
    formData.append('image', $('#image')[0].files[0]);
    formData.append('fullname', fullname);
    formData.append('birthday', birthday);
    formData.append('__RequestVerificationToken', $('input[name=__RequestVerificationToken]').val());
    $.ajax({
        url: urlConfig("/Account/UpdateAction"),
        type: "POST",
        processData: false,
        contentType: false,
        data: formData,
        success: function (data) {
            console.log(data);
            $("#response-modal").modal("show");
            $("#response-modal-content").html(data.error_content);
        }
    });

})