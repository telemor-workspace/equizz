﻿$(".signupbtn").on("click", function (e) {
    e.preventDefault();
    $("#choose-plan-modal").modal("show");
})

$(".cancelbtn").on("click", function () {
})

var onloadCallback = function () {
    var capt1 = grecaptcha.render('html_element', {
        'sitekey': '6LfLP8QZAAAAAOt97lEBrfUNMv6SJa1igLfKitXF',
        'callback': function (response) {
            //$("#password-box").show();
            document.getElementById("captcha").value = grecaptcha.getResponse(capt1);
            $(".freeBtn").removeClass("button-disable");
            $(".signupbtn").removeClass("button-disable");
        },
        'expired-callback': function (response) {
            $("#password-box").hide();
            document.getElementById("captcha").value = "0";
            $(".freeBtn").addClass("button-disable");
            $(".signupbtn").addClass("button-disable");
        },
    });
};

$(".freeBtn").on("click", function () {
    var captcha = $("#captcha").val();
    var phonenumber = $("#phonenumber").val();
    //var email = $("#email").val();

    console.log("create a free account: ", phonenumber);
    console.log("captcha: ", captcha);
    //console.log("email: ", email);

    $.ajax({
        url: urlConfig("/Account/CreateFreeAction"),
        type: "POST",
        data: {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
            captcha: captcha,
            phonenumber: phonenumber,
            //email: email,
        },
        success: function (data) {
            console.log(data);
            $("#choose-plan-modal").modal("hide");
            if (data.error_code == "0") {
                //location.href = "/Account/Login";
                $.ajax({
                    url: urlConfig("/Partial/Response"),
                    data: {
                        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
                        href: "/Account/Login",
                        response: data.error_content,
                    },
                    type: "POST"
                }).success(function (data) {
                    $('#myModalContent').html(data);
                    $('#myModal').modal({ "backdrop": "static", keyboard: true });
                    $('#myModal').modal('show');
                });

            } else {
                $("#response-modal").modal("show");
                $("#response-modal-content").html(data.error_content);
            }
        }
    });
})