﻿function urlConfig(link) {
    return window.location.protocol + '//' + window.location.host + '/' + link;
}
// check rule email
function checkEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

//get domain url
function url($ajaxlink) {
    return location.protocol + '//' + location.host + '/assets/assets-v2/js/' + $ajaxlink;
}
