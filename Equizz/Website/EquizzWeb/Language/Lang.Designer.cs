﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FunQuizWeb.Language {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FunQuizWeb.Language.Lang", typeof(Lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About.
        /// </summary>
        public static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accept.
        /// </summary>
        public static string accept {
            get {
                return ResourceManager.GetString("accept", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account information.
        /// </summary>
        public static string accountInformation {
            get {
                return ResourceManager.GetString("accountInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Account plan.
        /// </summary>
        public static string accountPlan {
            get {
                return ResourceManager.GetString("accountPlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        public static string accountSettings {
            get {
                return ResourceManager.GetString("accountSettings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active code.
        /// </summary>
        public static string activeCode {
            get {
                return ResourceManager.GetString("activeCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By creating an account you agree to our.
        /// </summary>
        public static string agreeOur {
            get {
                return ResourceManager.GetString("agreeOur", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You answered this minigame. Please try in the next time!.
        /// </summary>
        public static string answeredMinigame {
            get {
                return ResourceManager.GetString("answeredMinigame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Birthday.
        /// </summary>
        public static string birthday {
            get {
                return ResourceManager.GetString("birthday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buy more turn.
        /// </summary>
        public static string buyMore {
            get {
                return ResourceManager.GetString("buyMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buy one more turn successful.
        /// </summary>
        public static string buySuccess {
            get {
                return ResourceManager.GetString("buySuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change password.
        /// </summary>
        public static string changePass {
            get {
                return ResourceManager.GetString("changePass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change your plan.
        /// </summary>
        public static string changePlan {
            get {
                return ResourceManager.GetString("changePlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change your password.
        /// </summary>
        public static string changeYourPassword {
            get {
                return ResourceManager.GetString("changeYourPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose your plan.
        /// </summary>
        public static string choosePlan {
            get {
                return ResourceManager.GetString("choosePlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Monthly minigame.
        /// </summary>
        public static string circleMinigame {
            get {
                return ResourceManager.GetString("circleMinigame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string close {
            get {
                return ResourceManager.GetString("close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Coin is invalid.
        /// </summary>
        public static string CoinInvalid {
            get {
                return ResourceManager.GetString("CoinInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please come back in the next week!.
        /// </summary>
        public static string comebackNextTime {
            get {
                return ResourceManager.GetString("comebackNextTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        public static string confirm {
            get {
                return ResourceManager.GetString("confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Congratulations! You are in top 2 users answering fastest..
        /// </summary>
        public static string congratulation {
            get {
                return ResourceManager.GetString("congratulation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create free.
        /// </summary>
        public static string createFree {
            get {
                return ResourceManager.GetString("createFree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Daily packet 2.
        /// </summary>
        public static string daily10 {
            get {
                return ResourceManager.GetString("daily10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 10 Cent / day.
        /// </summary>
        public static string daily10Fee {
            get {
                return ResourceManager.GetString("daily10Fee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Daily packet 1.
        /// </summary>
        public static string daily15 {
            get {
                return ResourceManager.GetString("daily15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 15 Cent / day.
        /// </summary>
        public static string daily15Fee {
            get {
                return ResourceManager.GetString("daily15Fee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to days.
        /// </summary>
        public static string days {
            get {
                return ResourceManager.GetString("days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EQuiz is the entertainment services platform operating on 3G/4G. EQuiz users can join the game and enjoy a fun time with hundreds of different quizzes..
        /// </summary>
        public static string description1 {
            get {
                return ResourceManager.GetString("description1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EQuiz topics are very diverse and close to life around like love, friendship, money, predict the future, answer the question to get the reward ... to help players entertain and learn more new aspects of themself..
        /// </summary>
        public static string description2 {
            get {
                return ResourceManager.GetString("description2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User invite their friend successfully will get 3 point. 10 point will convert to 3 Cent basic account. .
        /// </summary>
        public static string description3 {
            get {
                return ResourceManager.GetString("description3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Sunday, the system will send 1 question to choose 1 people has fastest true answer (play on web). She/he will receive 1500 Cent in basic account. .
        /// </summary>
        public static string description4 {
            get {
                return ResourceManager.GetString("description4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Sunday, the system will send 1 question to choose 1 people has fastest true answer (play on web). She/he will receive 1500 Cent in basic account. .
        /// </summary>
        public static string descriptionMinigame {
            get {
                return ResourceManager.GetString("descriptionMinigame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For customer has high playing time in day.
        /// </summary>
        public static string descriptionTop10 {
            get {
                return ResourceManager.GetString("descriptionTop10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Sunday, the system will send 1 question to choose 1 people has fastest true answer (play on web). She/he will receive 1500 Cent in basic account. .
        /// </summary>
        public static string descriptionTop3 {
            get {
                return ResourceManager.GetString("descriptionTop3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In Sunday, the system will send 1 question to choose 2 people has fastest true answer..
        /// </summary>
        public static string descriptionTop3_1 {
            get {
                return ResourceManager.GetString("descriptionTop3_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You will receive 1500 Cent in basic account..
        /// </summary>
        public static string descriptionTop3_2 {
            get {
                return ResourceManager.GetString("descriptionTop3_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your friend&apos;s phone number.
        /// </summary>
        public static string enterFriendNumber {
            get {
                return ResourceManager.GetString("enterFriendNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your OTP.
        /// </summary>
        public static string enterOtp {
            get {
                return ResourceManager.GetString("enterOtp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your password.
        /// </summary>
        public static string enterPass {
            get {
                return ResourceManager.GetString("enterPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your phone number.
        /// </summary>
        public static string enterPhoneNumber {
            get {
                return ResourceManager.GetString("enterPhoneNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your phone.
        /// </summary>
        public static string enterYourPhone {
            get {
                return ResourceManager.GetString("enterYourPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your packet is not existed..
        /// </summary>
        public static string ErrTypePackage {
            get {
                return ResourceManager.GetString("ErrTypePackage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exchange.
        /// </summary>
        public static string exchange {
            get {
                return ResourceManager.GetString("exchange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exchange your points.
        /// </summary>
        public static string exchangeYourPoints {
            get {
                return ResourceManager.GetString("exchangeYourPoints", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill in this form to login..
        /// </summary>
        public static string fillToLogin {
            get {
                return ResourceManager.GetString("fillToLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill in this form to create an account..
        /// </summary>
        public static string fillToRegister {
            get {
                return ResourceManager.GetString("fillToRegister", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finish rechecking your answer!.
        /// </summary>
        public static string finishRechecking {
            get {
                return ResourceManager.GetString("finishRechecking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EQuiz is the entertainment services platform operating on 3G/4G. EQuiz users can join the game and enjoy a fun time with hundreds of different quizzes..
        /// </summary>
        public static string footer1 {
            get {
                return ResourceManager.GetString("footer1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User invite their friend successfully will get 3 point. 10 point will convert to 3 Cent basic account. .
        /// </summary>
        public static string footer2 {
            get {
                return ResourceManager.GetString("footer2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The system allow to register 2 package at same time.
        /// With subcribers
        ///- QUIZ15 package : 15Cent/day/12 playing times
        ///- QUIZ10 package: 10 Cent/day/7 playing times
        ///- Buy more times: 3 Cent/ 1 times play
        ///- Allow to register 2 package at same time. .
        /// </summary>
        public static string footer3 {
            get {
                return ResourceManager.GetString("footer3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forgot your password.
        /// </summary>
        public static string forgotPass {
            get {
                return ResourceManager.GetString("forgotPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Free for the first time registering.
        /// </summary>
        public static string freeFirstTime {
            get {
                return ResourceManager.GetString("freeFirstTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Free account.
        /// </summary>
        public static string freePacketInfo {
            get {
                return ResourceManager.GetString("freePacketInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Free subscriber.
        /// </summary>
        public static string freeSubscriber {
            get {
                return ResourceManager.GetString("freeSubscriber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your friend is already invited by another one..
        /// </summary>
        public static string friendExisted {
            get {
                return ResourceManager.GetString("friendExisted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full name.
        /// </summary>
        public static string fullName {
            get {
                return ResourceManager.GetString("fullName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Get your new password.
        /// </summary>
        public static string getPass {
            get {
                return ResourceManager.GetString("getPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hours.
        /// </summary>
        public static string hours {
            get {
                return ResourceManager.GetString("hours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can not authenticate your captcha.
        /// </summary>
        public static string invalidCaptcha {
            get {
                return ResourceManager.GetString("invalidCaptcha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invite your friends.
        /// </summary>
        public static string invite {
            get {
                return ResourceManager.GetString("invite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log in.
        /// </summary>
        public static string login {
            get {
                return ResourceManager.GetString("login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log out.
        /// </summary>
        public static string logout {
            get {
                return ResourceManager.GetString("logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oh no! Better luck next time!.
        /// </summary>
        public static string luckyNextTime {
            get {
                return ResourceManager.GetString("luckyNextTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minigame.
        /// </summary>
        public static string Minigame {
            get {
                return ResourceManager.GetString("Minigame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minigames.
        /// </summary>
        public static string Minigames {
            get {
                return ResourceManager.GetString("Minigames", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to minutes.
        /// </summary>
        public static string minutes {
            get {
                return ResourceManager.GetString("minutes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Money.
        /// </summary>
        public static string Money {
            get {
                return ResourceManager.GetString("Money", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More stuff.
        /// </summary>
        public static string moreStuff {
            get {
                return ResourceManager.GetString("moreStuff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have to login first !.
        /// </summary>
        public static string mustLogin {
            get {
                return ResourceManager.GetString("mustLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your new password.
        /// </summary>
        public static string newPass {
            get {
                return ResourceManager.GetString("newPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No mini game today.
        /// </summary>
        public static string noGame {
            get {
                return ResourceManager.GetString("noGame", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not enough point to exchange.
        /// </summary>
        public static string notEnoughPoint {
            get {
                return ResourceManager.GetString("notEnoughPoint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your old password.
        /// </summary>
        public static string oldPass {
            get {
                return ResourceManager.GetString("oldPass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to out of.
        /// </summary>
        public static string outOf {
            get {
                return ResourceManager.GetString("outOf", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Out of turn ! You have to buy more turn..
        /// </summary>
        public static string outOfTurn {
            get {
                return ResourceManager.GetString("outOfTurn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ou paka chanje anko.
        /// </summary>
        public static string overLimitExchange {
            get {
                return ResourceManager.GetString("overLimitExchange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QUIZ10 package : 5 Cent/day/7 playing times.
        /// </summary>
        public static string packetDaily10Info {
            get {
                return ResourceManager.GetString("packetDaily10Info", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QUIZ15 package: 15 Cent/day/12 playing times.
        /// </summary>
        public static string packetDaily15Info {
            get {
                return ResourceManager.GetString("packetDaily15Info", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone number.
        /// </summary>
        public static string phoneNumber {
            get {
                return ResourceManager.GetString("phoneNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to points.
        /// </summary>
        public static string points {
            get {
                return ResourceManager.GetString("points", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Points.
        /// </summary>
        public static string Points1 {
            get {
                return ResourceManager.GetString("Points1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Terms &amp; Privacy.
        /// </summary>
        public static string policy {
            get {
                return ResourceManager.GetString("policy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Products.
        /// </summary>
        public static string Products {
            get {
                return ResourceManager.GetString("Products", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string profile {
            get {
                return ResourceManager.GetString("profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quizes.
        /// </summary>
        public static string Quizes {
            get {
                return ResourceManager.GetString("Quizes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quizzes.
        /// </summary>
        public static string Quizzes {
            get {
                return ResourceManager.GetString("Quizzes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Game rank.
        /// </summary>
        public static string rank {
            get {
                return ResourceManager.GetString("rank", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ranking.
        /// </summary>
        public static string Ranking {
            get {
                return ResourceManager.GetString("Ranking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You will receive 1500 Cent over your phone..
        /// </summary>
        public static string receiveReward {
            get {
                return ResourceManager.GetString("receiveReward", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recheck.
        /// </summary>
        public static string recheck {
            get {
                return ResourceManager.GetString("recheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string register {
            get {
                return ResourceManager.GetString("register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Response.
        /// </summary>
        public static string response {
            get {
                return ResourceManager.GetString("response", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Result.
        /// </summary>
        public static string result {
            get {
                return ResourceManager.GetString("result", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Re-take the Quiz!.
        /// </summary>
        public static string retake {
            get {
                return ResourceManager.GetString("retake", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We will have one minigame weeky at 10:00 AM. Please don&apos;t forget participating our mini game..
        /// </summary>
        public static string schedule {
            get {
                return ResourceManager.GetString("schedule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to seconds.
        /// </summary>
        public static string seconds {
            get {
                return ResourceManager.GetString("seconds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign up.
        /// </summary>
        public static string signUp {
            get {
                return ResourceManager.GetString("signUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skip to main content.
        /// </summary>
        public static string skip {
            get {
                return ResourceManager.GetString("skip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sponsored.
        /// </summary>
        public static string Sponsored {
            get {
                return ResourceManager.GetString("Sponsored", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subscribe to Our Equiz service.
        /// </summary>
        public static string subscriberEquiz {
            get {
                return ResourceManager.GetString("subscriberEquiz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tests.
        /// </summary>
        public static string Test {
            get {
                return ResourceManager.GetString("Test", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tests.
        /// </summary>
        public static string Tests {
            get {
                return ResourceManager.GetString("Tests", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to to.
        /// </summary>
        public static string to {
            get {
                return ResourceManager.GetString("to", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You answered right but too late. Better lucky next time!.
        /// </summary>
        public static string tooLate {
            get {
                return ResourceManager.GetString("tooLate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TOP 10.
        /// </summary>
        public static string top10 {
            get {
                return ResourceManager.GetString("top10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TOP 3.
        /// </summary>
        public static string top3 {
            get {
                return ResourceManager.GetString("top3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total score.
        /// </summary>
        public static string totalScore {
            get {
                return ResourceManager.GetString("totalScore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total Spin.
        /// </summary>
        public static string totalSpin {
            get {
                return ResourceManager.GetString("totalSpin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type error.
        /// </summary>
        public static string TypeErr {
            get {
                return ResourceManager.GetString("TypeErr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        public static string update {
            get {
                return ResourceManager.GetString("update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update successful.
        /// </summary>
        public static string updateSuccessful {
            get {
                return ResourceManager.GetString("updateSuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we have a problem. Please try again!.
        /// </summary>
        public static string weHaveAProblem {
            get {
                return ResourceManager.GetString("weHaveAProblem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Winners.
        /// </summary>
        public static string Wins {
            get {
                return ResourceManager.GetString("Wins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wrong service ID.
        /// </summary>
        public static string WrongServiceID {
            get {
                return ResourceManager.GetString("WrongServiceID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your image.
        /// </summary>
        public static string yourImage {
            get {
                return ResourceManager.GetString("yourImage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recent plan.
        /// </summary>
        public static string yourPlan {
            get {
                return ResourceManager.GetString("yourPlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Score.
        /// </summary>
        public static string yourScore {
            get {
                return ResourceManager.GetString("yourScore", resourceCulture);
            }
        }
    }
}
