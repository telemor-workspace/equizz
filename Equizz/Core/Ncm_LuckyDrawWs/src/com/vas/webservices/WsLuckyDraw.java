/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.webservices;

import com.vas.wsfw.common.Common;
import com.vas.wsfw.common.Encrypt;
import com.vas.wsfw.common.MessageResponse;
import com.vas.wsfw.common.WebserviceAbstract;
import com.vas.wsfw.common.WebserviceManager;
import com.vas.wsfw.database.WsProcessUtils;
import com.vas.wsfw.obj.AccountInfo;
import com.vas.wsfw.obj.ChargeLog;
import com.vas.wsfw.obj.PointTotalObj;
import com.vas.wsfw.obj.ProductInfo;
import com.vas.wsfw.obj.RankingObj;
import com.vas.wsfw.obj.RegisterInfo;
import com.vas.wsfw.obj.Request;
import com.vas.wsfw.obj.Response;
import com.vas.wsfw.obj.SmsMtObj;
import com.vas.wsfw.obj.SpinGift;
import com.vas.wsfw.obj.TransactionLog;
import com.vas.wsfw.obj.UserInfo;

import com.vas.wsfw.services.WSProcessor;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Sungroup
 */
@WebService
public class WsLuckyDraw extends WebserviceAbstract {

    private WsProcessUtils db;
    private StringBuilder br = new StringBuilder();
    //private Exchange exchange;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private SimpleDateFormat fullDf = new SimpleDateFormat("yyyyMMddHHmmss");
    private SimpleDateFormat reqDf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    private WSProcessor ws;

    public WsLuckyDraw() throws Exception {
        super("WsLuckyDraw");
        db = new WsProcessUtils("FunQuiz", logger);
        ws = new WSProcessor(logger, "../etc/webservice.cfg", "../etc/database.xml");
        if (Common.iLoadConfig) {
            Common.listConfig = db.getConfig("PROCESS");
            MessageResponse.setMessage(Common.listConfig);
            logger.info("LIST CONFIG:\n" + Common.listConfig);
            Common.listProduct = db.iLoadPackage();
            //Common.mapPrize = db.loadPrize();
            Common.mapMpsConfig = db.loadMpsConfig();
            Common.loadConfig();
        }
    }

    //
    // FOR MPS
    //
    public Response renewMps(String msisdn, String packName) {
        Response response = new Response();
        response.setErrorCode(SUCCESS);
        try {
            msisdn = formatMsisdn(msisdn);
            // after this, only one package returned
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packName);
            if (listReg == null || listReg.isEmpty()) {
                logger.error("Renew but not registered: " + msisdn);
                response.setErrorCode(Common.ErrorCode.NOT_REGISTERED);
                response.setContent(MessageResponse.getDefaultMessage(Common.Message.NOT_REGISTERED, logger));
                return response;
            }
            // remove all package buy_code, just only packages daily
            RegisterInfo regInfo = listReg.get(0);

            ProductInfo productInfo = getProductByName(regInfo.getProductName());
            double fee = productInfo.getFee();
            logger.info("Renew pending success " + msisdn + ", fee " + fee + ", account " + msisdn);

            // insert register
            Timestamp expire = getExpireTime(productInfo);
            regInfo.setExpireTime(expire);
            regInfo = updateRegRenew(regInfo, productInfo);
            db.updateRenewReg(regInfo);

            //insert chargelog 
            if (fee > 0) {
                ChargeLog chargeLog = new ChargeLog();
                chargeLog.setFee(fee);
                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                chargeLog.setMsisdn(msisdn);
                chargeLog.setDescription("Register " + regInfo.getProductName());
                db.iInsertChargeLog(chargeLog);
            }

            // send message
            String message;
            message = MessageResponse.get(Common.Message.RENEW_SUCCESS,
                    Common.Message.RENEW_SUCCESS + "_" + regInfo.getProductName(), logger);
            message = message.replace("%fee%", fee + "");
            message = message.replaceAll("%expire%", sdf.format(expire));
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(msisdn);
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    public Response registerByMps(String msisdn, String packageName, int fee, String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
//        Timestamp reqTime = new Timestamp(System.currentTimeMillis());

        try {
            // check existed
            AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
//            UserProfile userProfile = db.iGetUserProfileByMsisdn(msisdn);

            if (accountInfo == null) {
                logger.error("Error check exist account: " + msisdn);
                response.setErrorCode(Common.ErrorCode.QUERY_ERROR);
                String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                response.setContent(message);
                return response;
            }
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
            if (listReg != null && !listReg.isEmpty()) {
                logger.info("Aready registered: " + msisdn + " -> cancel service");
//                String message = MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger);
//                SmsMtObj mt = new SmsMtObj();
//                mt.setMsisdn(msisdn);
//                mt.setChannel(Common.CHANNEL);
//                mt.setMessage(message);
//                db.insertMt(mt); 
//                response.setErrorCode(Common.ErrorCode.ALREADY_REGISTER);
//                response.setContent(message);
//                return response;
                db.disableAutoExtend(listReg.get(0).getRegisterId());
            }
            ProductInfo productInfo = getProductByName(packageName);
//            double fee = productInfo.getFee();
            List<RegisterInfo> listRegInday = null;
            boolean registered = false;
            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
            if (listRegInday != null && listRegInday.size() > 0) {
                registered = true;
            }

            logger.info("Register success " + msisdn + ", fee " + fee + ", account " + accountInfo.getMsisdn() + ", pack = " + productInfo.getProductName());

            boolean isCreated = true;
            // create account
            String password = generateValidateCode(4);
            if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                isCreated = false;
                String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                db.insertAccountUser(msisdn, encyptedPassword, "USSD", serviceId);
            }

            // check user_info
//            if (userProfile.getMsisdn() == null || userProfile.getMsisdn().isEmpty()) {
//                isCreated = false;
//                db.insertUserProfile(msisdn);
//            }

            RegisterInfo reg;
            if (!registered) {
                // insert register
                reg = new RegisterInfo();
                reg.setMsisdn(msisdn);
                reg.setProductName(productInfo.getProductName());
                reg.setExpireTime(getExpireTime(productInfo));
                reg = updateRegRenew(reg, productInfo);
                db.iInsertRegisterInfo(reg);

            } else {
                reg = listRegInday.get(0);
                reg.setExpireTime(getExpireTime(productInfo));
                db.iInsertRegisterInfo(reg);
            }

            //insert chargelog
            if (fee > 0) {
                ChargeLog chargeLog = new ChargeLog();
                chargeLog.setFee(fee);
                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                chargeLog.setMsisdn(msisdn);
                chargeLog.setDescription("Register " + packageName);
                db.iInsertChargeLog(chargeLog);
            }

            // send message
            String message = "";
            if (!isCreated) {
//                message = MessageResponse.get(Common.Message.REGISTER_SUCCESS + "_" + productInfo.getProductName(),
//                        Common.Message.REGISTER_SUCCESS, logger);
                message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS, logger);
                message = message.replace("%fee%", fee + "");
                message = message.replaceAll("%password%", password);
                message = message.replaceAll("%expire%", sdf.format(reg.getExpireTime()));
                SmsMtObj mt = new SmsMtObj();
                mt.setMsisdn(msisdn);
                mt.setChannel(Common.CHANNEL);
                mt.setMessage(message);
                db.insertMt(mt);
            }
//            else {
//                message = MessageResponse.get(Common.Message.REGISTER_SUCCESS_USSD + "_" + productInfo.getProductName(),
//                        Common.Message.REGISTER_SUCCESS_USSD, logger);
//            }

            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    public Response cancelServiceMps(String msisdn, String packgName) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            // check existed
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packgName);
            if (listReg == null) {
                logger.error("Error check exist account: " + msisdn);
                response.setErrorCode(Common.ErrorCode.QUERY_ERROR);
                response.setContent("Error database!");
                return response;
            } else if (listReg.isEmpty()) {
                logger.info("Not registered: " + msisdn + " -> success");
//                response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_REGISTERED);
//                response.setContent("Not registered");
                String message = MessageResponse.get(Common.Message.DESTROY_ACCOUNT_SUCCESS, logger);
                response.setErrorCode(Common.ErrorCode.SUCCESS);
                response.setContent(message);
                return response;
            }
            db.disableAutoExtend(listReg.get(0).getRegisterId());
            // send message
            String message = MessageResponse.get(Common.Message.DESTROY_ACCOUNT_SUCCESS, logger);
//                        message = message.replaceAll("%expire_free%", shortDateFm.format(Common.FREE_DAY));
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(accountInfo.getMsisdn());
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
            // return
            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    @WebMethod(operationName = "subRequest")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public String subRequest(
            @WebParam(name = "username") String wsUser,
            @WebParam(name = "password") String wsPass,
            @WebParam(name = "serviceid") String serviceid,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "chargetime") String chargetime,
            @WebParam(name = "params") String params,
            @WebParam(name = "amount") String fee,
            @WebParam(name = "transactionId") String transactionId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
        // translog
        Timestamp reqTime = new Timestamp(System.currentTimeMillis());
        TransactionLog transLog = new TransactionLog();
        transLog.setTransactionId(transactionId);
        transLog.setCommand(params);
        transLog.setMsisdn(msisdn);
        transLog.setRequestTime(reqTime);
        transLog.setRequest(serviceid + "|" + msisdn + "|" + chargetime + "|" + params + "|" + fee);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("subRequest Params:").
                    append("\nserviceid:").append(serviceid).
                    append("\nmsisdn:").append(msisdn).
                    append("\nchargetime:").append(chargetime).
                    append("\namount:").append(fee).
                    append("\nparams:").append(params).
                    append("\ntransactionId:").append(transactionId);
            logger.info(br);

            Request request = new Request();
            request.setUser(msisdn);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (params == null || params.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("params is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                    return response.getErrorCode() + "|" + response.getContent();
                } else {
                    msisdn = formatMsisdn(msisdn);
                    if (params.equals("0")) {
                        int amount = Integer.parseInt(fee);
                        // register
                        response = registerByMps(msisdn, serviceid, amount, serviceid);
                    } else if (params.equals("1")) {
                        // unsub
                        response = cancelServiceMps(msisdn, serviceid);
                    } else {
                        logger.info("Unknown params: " + params);
                        response.setErrorCode(PARAM_NOT_VALID);
                        response.setContent("Unknown params: " + params);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        } finally {
            try {
                transLog.setErrorCode(response.getErrorCode());
                transLog.setResponse(response.toString());
                transLog.setResponseTime(new Timestamp(System.currentTimeMillis()));
                db.insertTransLog1(transLog);
            } catch (Exception ex) {
                logger.error("Error insert log", ex);
            } finally {
            }
        }

        return response.getErrorCode();
    }

    @WebMethod(operationName = "receiveresult")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public String receiveresult(
            @WebParam(name = "username") String wsUser,
            @WebParam(name = "password") String wsPass,
            @WebParam(name = "serviceid") String serviceid,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "chargetime") String chargetime,
            @WebParam(name = "params") String params,
            @WebParam(name = "amount") String fee,
            @WebParam(name = "transactionId") String transactionId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
        // translog
        Timestamp reqTime = new Timestamp(System.currentTimeMillis());
        TransactionLog transLog = new TransactionLog();
        transLog.setTransactionId(transactionId);
        transLog.setCommand(params);
        transLog.setMsisdn(msisdn);
        transLog.setRequestTime(reqTime);
        transLog.setRequest(serviceid + "|" + msisdn + "|" + chargetime + "|" + params + "|" + fee);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("receiveresult Params:").
                    append("\nmsisdn:").append(msisdn).
                    append("\nparams:").append(params).
                    append("\nchargetime:").append(chargetime).
                    append("\nserviceid:").append(serviceid).
                    append("\namount:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(msisdn);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (params == null || params.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("params is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                    return response.getErrorCode() + "|" + response.getContent();
                } else {
                    msisdn = formatMsisdn(msisdn);
                    if (params.equals("0")) {
                        renewMps(msisdn, serviceid);

                        // send message
//                    Timestamp expireFree = db.iGetExpireFree(msisdn);
//                    String message2 = MessageResponse.getDefaultMessage(autoRenew ? Common.Message.REGISTER_SUCCESS_USSD_RENEW : Common.Message.REGISTER_SUCCESS_USSD, logger);
//                    message2 = message2.replaceAll("%expire%", df.format(expireTime));
//                    message2 = message2.replaceAll("%expire_free%", shortDateFm.format(Common.FREE_DAY));
//                    message2 = message2.replaceAll("%fee%", Common.FEE + "");
//                    message2 = message2.replaceAll("%password%", password);
//                    SmsMtObj mt = new SmsMtObj();
//                    mt.setMsisdn(accountInfo.getMsisdn());
//                    mt.setChannel(Common.CHANNEL);
//                    mt.setMessage(message2);
//                    db.insertMt(mt);
                        // return
                        logger.info("Renew account success: " + msisdn);
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(Common.ResultCode.SUCCESS);
                        return response.getErrorCode();

                    } else {
                        logger.info("Renew fail: " + msisdn);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        } finally {
            try {
                transLog.setErrorCode(response.getErrorCode());
                transLog.setResponse(response.toString());
                transLog.setResponseTime(new Timestamp(System.currentTimeMillis()));
                db.insertTransLog1(transLog);
            } catch (Exception ex) {
                logger.error("Error insert log", ex);
            } finally {
            }
        }

        return response.getErrorCode();
    }

    @WebMethod(operationName = "wsCreateFree")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsCreateFree(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Channel") String channel,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if ((accountInfo.getMsisdn() != null && !accountInfo.getMsisdn().isEmpty())) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        // create a free account -> insert to account_user table and fq_user_info
                        String password = generateValidateCode(4);
                        String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                        db.createAccountUser(msisdn, encyptedPassword, channel, serviceId);
                        db.createUserInfo(msisdn);
                        logger.info("Create account success " + msisdn);

                        // send sms password to user
                        String message = "";
                        message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS,
                                Common.Message.CREATE_ACCOUNT_SUCCESS, logger);

                        // MT table for send sms to user 
                        SmsMtObj mt = new SmsMtObj();
                        mt.setMsisdn(msisdn);
                        mt.setChannel(Common.CHANNEL);
                        mt.setMessage(message);
                        db.insertMt(mt);

                        // response message to web
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsResetPass")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsResetPass(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if ((accountInfo.getMsisdn() != null && !accountInfo.getMsisdn().isEmpty())) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        // create a free account -> insert to account_user table and fq_user_info
                        String password = generateValidateCode(4);
                        String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                        db.updateAccountUser(msisdn, encyptedPassword);
                        logger.info("Update account success " + msisdn);

                        // send sms password to user
                        String message = "";
                        message = MessageResponse.get(Common.Message.UPDATE_ACCOUNT_SUCCESS,
                                Common.Message.UPDATE_ACCOUNT_SUCCESS, logger);

                        // MT table for send sms to user 
                        SmsMtObj mt = new SmsMtObj();
                        mt.setMsisdn(msisdn);
                        mt.setChannel(Common.CHANNEL);
                        mt.setMessage(message);
                        db.insertMt(mt);

                        // response message to web
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsChangePass")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChangePass(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "OldPassword") String oldPassword,
            @WebParam(name = "NewPassword") String newPassword,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (oldPassword == null || oldPassword.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            } else if (newPassword == null || newPassword.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if ((accountInfo.getMsisdn() != null && !accountInfo.getMsisdn().isEmpty())) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        // update password
                        // check pass
                        if (accountInfo.getPassword() == oldPassword) {
                            db.updateAccountUser(msisdn, newPassword);
                            logger.info("Update account success " + msisdn);

                            // send sms password to user
                            String message = "";
                            message = MessageResponse.get(Common.Message.UPDATE_ACCOUNT_SUCCESS,
                                    Common.Message.UPDATE_ACCOUNT_SUCCESS, logger);

                            // response message to web
                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else {
                            // send sms password to user
                            String message = "";
                            message = MessageResponse.get(Common.Message.WRONG_INFO_TO_UPDATE,
                                    Common.Message.WRONG_INFO_TO_UPDATE, logger);
                            // response message to web
                            response.setErrorCode(Common.ErrorCode.FAILURE);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsBuyCode")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsBuyCode(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
                    if (listReg == null || listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
                        // not waiting confirm
                        String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());
                        resultPaid = ws.requestMpsOtp(msisdn, fee, REQ, productInfo.getProductName());
                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Create otp success " + msisdn + ", fee " + fee);

                            // send message
                            String message = MessageResponse.get(Common.Message.REGISTER_WAIT_CONFIRM + "_" + productInfo.getProductName(),
                                    Common.Message.REGISTER_WAIT_CONFIRM, logger);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                            response.setResultCode(REQ);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsBuyCodeConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsBuyCodeConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsBuyCodeConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (otp == null || otp.length() == 0) {
                logger.info("otp is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("otp is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
                    if (listReg == null || listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();

                        List<RegisterInfo> listRegInday = null;
                        boolean registered = false;
                        if (!db.checkRegistered(msisdn, productInfo.getProductName())) {
                            // if user is not existed
                            fee = 0;
                        } else {
                            // user existed
                            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
                            if (listRegInday != null && listRegInday.size() > 0) {
                                // the packet is valid, user can play
                                fee = 0;
                                registered = true;
                            }
                        }

                        resultPaid = ws.registerMpsConfirm(msisdn, fee, otp, requestId, productInfo.getProductName());

                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Buy code success success " + msisdn + ", fee " + fee);
                            Timestamp expireTime = getExpireTime(productInfo);
                            // check registered and write the next action to database
                            RegisterInfo reg;
                            if (!registered) {
                                // insert register
                                reg = new RegisterInfo();
                                reg.setMsisdn(msisdn);
                                reg.setProductName(productInfo.getProductName());
                                reg.setExpireTime(expireTime);
                                reg = updateRegRenew(reg, productInfo);
                                db.iInsertRegisterInfo(reg);
                            } else {
                                reg = listRegInday.get(0);
                                db.iInsertRegisterInfo(reg);
                            }

                            //insert chargelog
                            if (fee > 0) {
                                ChargeLog chargeLog = new ChargeLog();
                                chargeLog.setFee(productInfo.getFee());
                                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                chargeLog.setMsisdn(msisdn);
                                chargeLog.setDescription("Register " + packageName);
                                db.iInsertChargeLog(chargeLog);
                            }

                            // send message
                            String message = "";
                            message = MessageResponse.get(Common.Message.BUY_SPIN_SUB_SUCCESS + "_" + productInfo.getProductName(),
                                    Common.Message.BUY_SPIN_SUB_SUCCESS, logger);
                            message = message.replace("%fee%", productInfo.getFee() + "");
                            message = message.replaceAll("%expire%", sdf.format(expireTime));
                            // MT table for send sms to user 
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            String message = MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger);
                            response.setContent(message);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsRegisterSubOtp")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsRegisterSubOtp(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
                    if (listReg != null && !listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
                        // not waiting confirm
                        String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());

                        // for testing -----------------------------------------
//                        logger.info("Create otp success " + msisdn + ", fee " + fee);
//                        // send message
//                        String message = MessageResponse.get(Common.Message.REGISTER_WAIT_CONFIRM + "_" + productInfo.getProductName(),
//                                Common.Message.REGISTER_WAIT_CONFIRM, logger);
//
//                        response.setErrorCode(Common.ErrorCode.SUCCESS);
//                        response.setContent(message);
//                        logger.info(message);
//                        response.setResultCode(REQ);
                        // right -----------------------------------------------
                        resultPaid = ws.requestMpsOtp(msisdn, fee, REQ, productInfo.getProductName());
                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Create otp success " + msisdn + ", fee " + fee);

                            // send message
                            String message = MessageResponse.get(Common.Message.REGISTER_WAIT_CONFIRM + "_" + productInfo.getProductName(),
                                    Common.Message.REGISTER_WAIT_CONFIRM, logger);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                            response.setResultCode(REQ);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsRegisterSubConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsRegisterSubConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (otp == null || otp.length() == 0) {
                logger.info("otp is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("otp is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
                    if (listReg != null && !listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
//                        if (productInfo.getFee() > 0) {

                        List<RegisterInfo> listRegInday = null;
                        boolean registered = false;
                        if (!db.checkRegistered(msisdn, productInfo.getProductName())) {
                            fee = 0;
                        } else {
                            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
                            if (listRegInday != null && listRegInday.size() > 0) {
                                fee = 0;
                                registered = true;
                            }
                        }

                        resultPaid = ws.registerMpsConfirm(msisdn, fee, otp, requestId, productInfo.getProductName());

                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Register success success " + msisdn + ", fee " + fee);
                            Timestamp expireTime = getExpireTime(productInfo);
                            // check registered
                            RegisterInfo reg;
                            if (!registered) {
                                // insert register
                                reg = new RegisterInfo();
                                reg.setMsisdn(msisdn);
                                reg.setProductName(productInfo.getProductName());
                                reg.setExpireTime(expireTime);
                                reg = updateRegRenew(reg, productInfo);
                                db.iInsertRegisterInfo(reg);
                            } else {
                                reg = listRegInday.get(0);
                                db.iInsertRegisterInfo(reg);
                            }

                            //insert chargelog
                            if (fee > 0) {
                                ChargeLog chargeLog = new ChargeLog();
                                chargeLog.setFee(productInfo.getFee());
                                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                chargeLog.setMsisdn(msisdn);
                                chargeLog.setDescription("Register " + packageName);
                                db.iInsertChargeLog(chargeLog);
                            }

                            // create a new account_user for this user
                            boolean isCreated = true;
                            // check existed
                            AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                            // create account
                            String password = generateValidateCode(4);
                            if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                                isCreated = false;
                                String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                                logger.info("Create account success " + msisdn);
                                db.insertAccountUser(msisdn, encyptedPassword, "WEB", serviceId);
                            }
                            // send message
                            String message = "";
                            if (!isCreated) {
                                // message = MessageResponse.get(Common.Message.REGISTER_SUCCESS + "_" + productInfo.getProductName(),
                                // Common.Message.REGISTER_SUCCESS, logger);
                                message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS, logger);
                                message = message.replace("%fee%", fee + "");
                                message = message.replaceAll("%password%", password);
                                message = message.replaceAll("%expire%", sdf.format(reg.getExpireTime()));
                                SmsMtObj mt = new SmsMtObj();
                                mt.setMsisdn(msisdn);
                                mt.setChannel(Common.CHANNEL);
                                mt.setMessage(message);
                                db.insertMt(mt);
                            }
                            // send message
                            message = MessageResponse.get(Common.Message.REGISTER_SUCCESS + "_" + productInfo.getProductName(),
                                    Common.Message.REGISTER_SUCCESS, logger);
                            message = message.replace("%fee%", productInfo.getFee() + "");
                            message = message.replaceAll("%expire%", sdf.format(expireTime));
                            // MT table for send sms to user 
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            String message = MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger);
                            response.setContent(message);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsChargeFeeOtp")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChargeFeeOtp(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "fee") String fee) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsChargeFeeOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\n Msisdn:").append(msisdn).
                    append("\n Fee:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    int moneyCharge = Common.ADD_SPIN_FEE;
                    if (fee != null && fee.length() > 0) {
                        moneyCharge = Integer.parseInt(fee);
                    }

                    if (moneyCharge < 0) {
                        response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                        response.setContent("Charge fail");
                        return response;
                    }
                    // not waiting confirm
                    String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());
                    String resultPaid = ws.chargeFeeOtp(msisdn, moneyCharge, REQ);
                    if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                        logger.info("Create otp success " + msisdn + ", fee " + moneyCharge);

                        // send message
                        String message = MessageResponse.get(Common.Message.CHARGE_WAIT_CONFIRM, logger);
                        message = message.replaceAll("%money%", fee);

                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                        response.setResultCode(REQ);
                    } else {
                        response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                        String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                        response.setContent(message);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsChargeFeeConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChargeFeeConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Fee") String fee,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsChargeFeeConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nFee:").append(fee).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    int moneyCharge = Common.ADD_SPIN_FEE;
                    if (fee != null && fee.length() > 0) {
                        moneyCharge = Integer.parseInt(fee);
                    }
                    if (moneyCharge < 0) {
                        response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                        response.setContent("Charge fail");
                        return response;
                    }
                    // charge
                    String resultPaid = ws.chargeFeeConfirm(msisdn, moneyCharge, otp, requestId);

                    if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                        //insert chargelog
                        if (moneyCharge > 0) {
                            ChargeLog chargeLog = new ChargeLog();
                            chargeLog.setFee(moneyCharge);
                            chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                            chargeLog.setMsisdn(msisdn);
                            chargeLog.setDescription("Charge");
                            db.iInsertChargeLog(chargeLog);
                        }

                        processAfterCharge(msisdn, moneyCharge);

                        String message = MessageResponse.get(Common.Message.CHARGE_FEE_SUCCESS, logger);
                        message = message.replace("%fee%", moneyCharge + "");
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(msisdn);
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                        response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                        response.setContent(MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger));
                    } else {
                        response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                        response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    public void processAfterCharge(String msisdn, int fee) {
        List<RegisterInfo> listReg = db.getRegisterInfoAll(msisdn);
        if (listReg == null || listReg.isEmpty()) {
            // unsub
            int spinNum = (int) (fee / Common.SPIN_FEE);
            // update spin_gift
            List<SpinGift> listSg = db.getSpinGift(msisdn);
            if (listSg != null && listSg.size() > 0) {
                // update spin gift
                SpinGift sg = listSg.get(0);
                sg.setGiftMsisdn(msisdn);
                sg.setNumberSpin(sg.getNumberSpin() + (int) spinNum);
                db.iUpdateSpinGift(sg);
            } else {
                // insert new spin gift
                SpinGift sg = new SpinGift();
                sg.setMsisdn(msisdn);
                sg.setGiftMsisdn(msisdn);
                sg.setNumberSpin(spinNum);
                db.iInsertSpinGift(sg);
            }

            String message = MessageResponse.get(Common.Message.BUY_SPIN_UNSUB_SUCCESS, logger);
            message = message.replace("%fee%", fee + "");
            message = message.replace("%spin%", spinNum + "");
//            SmsMtObj mt = new SmsMtObj();
//            mt.setMsisdn(msisdn);
//            mt.setChannel(Common.CHANNEL);
//            mt.setMessage(message);
//            db.insertMt(mt);

            // update ranking
            updateRanking(msisdn, Common.SubType.UNSUB, spinNum, Common.RankType.BOUGHT, Common.PeriodPrize.WEEKLY);
            updateRanking(msisdn, Common.SubType.UNSUB, spinNum, Common.RankType.BOUGHT, Common.PeriodPrize.MONTHLY);
        } else {
            // for sub 
            int spinInt = fee * Common.ADD_SPIN_TIMES / Common.ADD_SPIN_FEE;
            // update spin 
            db.updateAddMoreTimes(listReg.get(0).getRegisterId(), spinInt);

            // send message
            String message = MessageResponse.get(Common.Message.BUY_SPIN_SUB_SUCCESS, logger);
            message = message.replaceAll("%fee%", fee + "");
            message = message.replaceAll("%spin%", spinInt + "");
//            SmsMtObj mt = new SmsMtObj();
//            mt.setMsisdn(msisdn);
//            mt.setChannel(Common.CHANNEL);
//            mt.setMessage(message);
//            db.insertMt(mt);

            // update ranking
            updateRanking(msisdn, Common.SubType.SUB, spinInt, Common.RankType.BOUGHT, Common.PeriodPrize.WEEKLY);
            updateRanking(msisdn, Common.SubType.SUB, spinInt, Common.RankType.BOUGHT, Common.PeriodPrize.MONTHLY);
        }
    }

    public RegisterInfo updateRegRenew(RegisterInfo reg, ProductInfo productInfo) {
        reg.setNumberSpin(productInfo.getNumberSpin());
        reg.setExtendStatus(0);
        reg.setPlayedTimes(0);
        reg.setRenew(productInfo.getRenew());
        return reg;
    }

    private String generateValidateCode(int length) {
        String output = "";
        String input = "1234567890";
        Random rnd = new Random();
        for (int i = 0; i < length; i++) {
            output = output + input.charAt(rnd.nextInt(input.length()));
        }
        return output;
    }

    private String formatMsisdn(String msisdn) {
        if (msisdn.startsWith(WebserviceManager.countryCode)) {
            return msisdn;
        } else if (msisdn.startsWith("0")) {
            return WebserviceManager.countryCode + msisdn.substring(1);
        }
        return WebserviceManager.countryCode + msisdn;
    }

    private void updateRanking(String msisdn, int subType, int count, int rankType, int period) {
        List<RankingObj> listRank = db.getRankObj(msisdn, rankType, period);
        if (listRank != null && listRank.size() > 0) {
            // update
            db.iUpdateRanking(listRank.get(0).getId(), subType, count);
        } else {
            // insert
            Calendar currentTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            endTime.set(Calendar.HOUR_OF_DAY, 20);
            endTime.set(Calendar.MINUTE, 0);
            endTime.set(Calendar.SECOND, 0);
            endTime.set(Calendar.MILLISECOND, 0);

            if (currentTime.after(endTime)) {
                endTime.add(Calendar.DAY_OF_MONTH, 1);
            }

            Calendar startTime = Calendar.getInstance();
            startTime.setTimeInMillis(endTime.getTimeInMillis() - 86400000L + 1000);

            RankingObj ranking = new RankingObj();
            ranking.setMsisdn(msisdn);
            ranking.setSpintCount(count);
            ranking.setStartTime(new Timestamp(startTime.getTimeInMillis()));
            ranking.setEndTime(new Timestamp(endTime.getTimeInMillis()));
            ranking.setSubType(subType);
            ranking.setRankType(rankType);

            if (period == Common.PeriodPrize.DAILY) {
                db.iInsertRanking(ranking);
            } else if (period == Common.PeriodPrize.WEEKLY) {
                db.iInsertRankingWeekly(ranking);
            }
            if (period == Common.PeriodPrize.MONTHLY) {
                db.iInsertRankingMonth(ranking);
            }
        }
    }

    private PointTotalObj updatePoint(String msisdn, int point, int type) {
        List<PointTotalObj> listPoint = db.getPointTotal(msisdn);
        if (listPoint != null && listPoint.size() > 0) {
            PointTotalObj newPoint = listPoint.get(0);
            // update
            logger.info("Update point: id " + newPoint.getId() + ", msisdn " + msisdn + ", point " + point);
            if (type == 1) {
                // add
                newPoint.setAddedPoint(point);
                db.iUpdatePointTotal(newPoint.getId(), point);
                newPoint.setPoint(newPoint.getPoint() + point);
            } else if (type == 2) {
                // multiple
                newPoint.setAddedPoint(newPoint.getPoint() * (point - 1));
                db.iUpdatePointTotal(newPoint.getId(), newPoint.getPoint() * (point - 1));
                newPoint.setPoint(newPoint.getPoint() * point);
            }
            return newPoint;
        } else {
            // insert
            PointTotalObj pointObj = new PointTotalObj();
            pointObj.setMsisdn(msisdn);
            if (type == 1) {
                // added
                pointObj.setAddedPoint(point);
                pointObj.setPoint(point);
            } else {
                // multiple
                pointObj.setAddedPoint(0);
                pointObj.setPoint(0);
            }
            pointObj.setSubType(0);
            db.iInsertPointTotal(pointObj);
            return pointObj;
        }
    }

    private ProductInfo getProductByName(String packName) {
        logger.info(Common.listProduct);
        for (ProductInfo product : Common.listProduct) {
            logger.info(product.getProductName());
            logger.info("packName: " + packName);
            if (product.getProductName().equalsIgnoreCase(packName)) {
                return product;
            }
        }
        return null;
    }

    private Timestamp getExpireTime(ProductInfo productInfo) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, productInfo.getExpireDays() - 1);
        return new Timestamp(cal.getTimeInMillis());
    }

    private Timestamp getTruncSysdate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    private String getHashCode(String securityCode, String salt, Logger logger) {
        MessageDigest md = null;
        try {
            String seq = salt + securityCode;
            md = MessageDigest.getInstance("SHA-256"); //step 2
            md.update(seq.getBytes("UTF-8")); //step 3
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        byte raw[] = md.digest(); //step 4
        String hash = (new BASE64Encoder()).encode(raw); //step 5
        return hash;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//        MessageDigest md = null;
//        String seq = "123456a@" + "32323232";
//        md = MessageDigest.getInstance("SHA-256"); //step 2
//        md.update(seq.getBytes("UTF-8")); //step 3

//        byte raw[] = md.digest(); //step 4
//        String hash = (new BASE64Encoder()).encode(raw); //step 5
//        System.out.println("hash" + hash);
//        String pass = "1QAaz@123@";
//        Pattern pattern = Pattern.compile("abcdefghijklmnopqrstuvwxyz");
        //boolean match = list.matches(".");
//        System.out.println("hash " + pass.matches(".*[A-Z].*"));
//        String output = "";
//        String input = "1234567890";
//        Random rnd = new Random();
//        for (int i = 0; i < 8; i++) {
//            output = output + input.charAt(rnd.nextInt(input.length()));
//        }
//        System.out.println("output = " + output);
//        double x = 1.1;
//        System.out.println("output = " + (((x * 3 / 2) * 100) / 100));
//        System.out.println("output = " + (x * 3 / 2));
        int point = Integer.parseInt("116000.0");
        int money = point / 10;
        System.out.println("Poinnt " + point + ", money: " + money);
    }

    @Override
    public UserInfo authenticate(String userName, String password, String ipAddress) {
        // validate thong tin user
        // validate thong tin user
        UserInfo userInfo = db.iGetUser(userName);
        if (userInfo == null || userInfo.getWsUserId() <= 0) {
            logger.info("User not existed: " + userName);
            userInfo = new UserInfo();
            userInfo.setWsUserId(-1);
            return userInfo;
        }

        if (userInfo.getIp() != null && userInfo.getIp().length() > 0) {
            // Check IP
            String ip[] = userInfo.getIp().split(",");
            boolean pass = false;
            for (String ipConfig : ip) {
                if (pair(ipAddress, ipConfig.trim())) {
                    pass = true;
                    break;
                }
            }
            if (!pass) {
                logger.info("IP address not allowed: " + ipAddress);
                userInfo.setWsUserId(-2);
                return userInfo;
            }
        }
        // Check password
        if (userInfo.getPass() != null && userInfo.getPass().trim().length() > 0) {
            if (password.equals(userInfo.getPass())) {
                return userInfo;
            }

            String passEncript = "";
            passEncript = Encrypt.getHashCode(userName, password, logger);
            if (passEncript.equals(userInfo.getPass())) {
                return userInfo;
            }
            logger.info("Password incorrect: user=" + userName);
            userInfo.setWsUserId(-3);
            return userInfo;
        }
        return null;//tam fix
    }
}
