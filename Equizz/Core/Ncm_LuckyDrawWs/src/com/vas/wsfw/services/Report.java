package com.vas.wsfw.services;

import com.vas.webservices.WsFunQuiz;
import com.vas.wsfw.common.Common;
import com.vas.wsfw.common.MessageResponse;
import com.vas.wsfw.obj.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;

public class Report extends WsFunQuiz implements Runnable {

    public static double MINIGAME_REWARD = -1000;
    public static double DAILY_REWARD = -50;
    public static double EXCHANGE_2 = -2;
    public static double EXCHANGE_4 = -4;
    public static double EXCHANGE_10 = -10;


    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    public Report() throws Exception {
    }

//    public void reportForAnHour() {
//        final Runnable reporter = new Runnable() {
//            public void run() {
//                Date date = new Date();
//                int hour = date.getHours();
////                System.out.println(hour);
//                logger.error("Loop reporting times " + hour);
//                try {
//                    // set condition to add money day by day
//                    // get playing time history by time in 1 -> 2 o'clock
//                    String reportTime = db.getConfigurationByKey(Common.Constant.TRIGGER_TIME_REPORT);
//                    String reportPhone = db.getConfigurationByKey(Common.Constant.TRIGGER_PHONE_REPORT);
//
//                    // get all time to report
//                    ArrayList<String> times = new ArrayList<>(Arrays.asList(reportTime.split(",")));
//                    ArrayList<String> phones = new ArrayList<>(Arrays.asList(reportPhone.split(",")));
//
//                    logger.error(times);
//                    logger.error(phones);
//
//                    if (times.contains(String.valueOf(hour))) {
//                        logger.error("Start reporting");
//                        reporting(hour, times, phones);
//                    }
//                } catch (Exception e) {
//                    logger.error("Error processing", e);
//                }
//            }
//        };
//        final ScheduledFuture<?> reportHandle =
//                scheduler.scheduleAtFixedRate(reporter, 1, 5, MINUTES);
////        scheduler.schedule(new Runnable() {
////            public void run() {
////                beeperHandle.cancel(true);
////            }
////        }, 2 * 60, SECONDS);
//    }

    private boolean reporting(int hourNow, ArrayList<String> listHour, ArrayList<String> listPhone) {
        // get all "doanh thu (dk mới, gia hạn, mua thêm lượt) : từng gói"
        // get all new registering
        // get position of this hour in a list config
        int pos = listHour.indexOf(String.valueOf(hourNow));
        Timestamp fromTime;
        Timestamp toTime;
        if (pos == 0) {
            // get from 0h00 -> now
            fromTime = getExactTimeInDay(0, 0, Integer.parseInt(listHour.get(listHour.size())), -1);
            toTime = getExactTimeInDay(0, 0, hourNow);
        } else {
            fromTime = getExactTimeInDay(0, 0, Integer.parseInt(listHour.get(pos - 1)));
            toTime = getExactTimeInDay(0, 0, hourNow);
        }

//        System.out.println(fromTime);
//        System.out.println(toTime);

        List<RegisterInfo> registerNew = db.getNewRegisterByDay(fromTime, toTime, Common.Constant.FUNQUIZ_SERVICEID);
        List<RegisterInfo> registerRenew = db.getReNewRegisterByDay(fromTime, toTime, Common.Constant.FUNQUIZ_SERVICEID);
        List<RegisterInfo> registerBuyMore = db.getBuyMoreByDay(fromTime, toTime, Common.Constant.FUNQUIZ_SERVICEID);
        List<RegisterInfo> registerCancel = db.getCancelRegisterByDay(fromTime, toTime, Common.Constant.FUNQUIZ_SERVICEID);
        List<ChargeLog> chargeLogs = db.getChargeLogByDay(fromTime, toTime);
        int total = db.getTotalSub(Common.Constant.FUNQUIZ_SERVICEID);

        //calculator
        int np2 = 0, np3 = 0, np5 = 0, rnp2 = 0, rnp3 = 0, rnp5 = 0, bm = 0, cp2 = 0, cp3 = 0, cp5 = 0, daily = 0, minigame = 0, coin = 0;
        if (registerNew != null && registerNew.size() > 0)
            for (int i = 0; i < registerNew.size(); i++) {
                if (registerNew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_2)) np2++;
                else if (registerNew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_3)) np3++;
                else if (registerNew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_5)) np5++;
            }
        if (registerRenew != null && registerRenew.size() > 0)
            for (int i = 0; i < registerRenew.size(); i++) {
                if (registerRenew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_2)) rnp2++;
                else if (registerRenew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_3)) rnp3++;
                else if (registerRenew.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_5)) rnp5++;
            }
        if (registerCancel != null && registerCancel.size() > 0)
            for (int i = 0; i < registerCancel.size(); i++) {
                if (registerCancel.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_2)) cp2++;
                else if (registerCancel.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_3)) cp3++;
                else if (registerCancel.get(i).getProductName().equals(Common.Constant.REGISTER_DAILY_5)) cp5++;
            }
        if (registerBuyMore != null && registerBuyMore.size() > 0) {
            bm = registerBuyMore.size();
        }

        if (chargeLogs != null && chargeLogs.size() > 0) {
            for (int c = 0; c < chargeLogs.size(); c++) {
                if (chargeLogs.get(c).getFee() == EXCHANGE_2) coin += 2;
                else if (chargeLogs.get(c).getFee() == EXCHANGE_4) coin += 4;
                else if (chargeLogs.get(c).getFee() == EXCHANGE_10) coin += 10;
                else if (chargeLogs.get(c).getFee() == DAILY_REWARD) daily += 50;
                else if (chargeLogs.get(c).getFee() == MINIGAME_REWARD) minigame += 1000;
            }
        }

        // build message
        Date date = new Date();
        int year = date.getYear() + 1900;
        int month = date.getMonth() + 1;
        String dateString = date.getDate() + "/" + month + "/" + year + " " + hourNow + ":" + date.getMinutes();

        String message = "";
        message = MessageResponse.get(Common.Message.TRIGGER_REPORT_MESSAGE,
                Common.Message.TRIGGER_REPORT_MESSAGE, logger);
        message = message.replaceAll("%date%", dateString);
        message = message.replaceAll("%new2%", String.valueOf(np2));
        message = message.replaceAll("%new3%", String.valueOf(np3));
        message = message.replaceAll("%new5%", String.valueOf(np5));
        message = message.replaceAll("%revenue2%", String.valueOf(rnp2));
        message = message.replaceAll("%revenue3%", String.valueOf(rnp3));
        message = message.replaceAll("%revenue5%", String.valueOf(rnp5));
        message = message.replaceAll("%cancel2%", String.valueOf(cp2));
        message = message.replaceAll("%cancel3%", String.valueOf(cp3));
        message = message.replaceAll("%cancel5%", String.valueOf(cp5));
        message = message.replaceAll("%buy%", String.valueOf(bm));

        message = message.replaceAll("%daily%", String.valueOf(daily));
        message = message.replaceAll("%minigame%", String.valueOf(minigame));
        message = message.replaceAll("%coin%", String.valueOf(coin));
        message = message.replaceAll("%total%", String.valueOf(total));
        logger.error("Message: " + message);

        for (int a = 0; a < listPhone.size(); a++) {
            String msisdn = listPhone.get(a);
            SmsMtObj mt = new SmsMtObj();
            mt.setMsisdn(msisdn);
            mt.setChannel(Common.CHANNEL);
            mt.setMessage(message);
            db.insertMt(mt);
        }
        return true;
    }

    @Override
    public void run() {
        final Runnable reporter = new Runnable() {
            public void run() {
                Date date = new Date();
                int hour = date.getHours();
                logger.error("Loop reporting times " + hour);
                try {
                    // set condition to add money day by day
                    // get playing time history by time in 1 -> 2 o'clock
                    String reportTime = db.getConfigurationByKey(Common.Constant.TRIGGER_TIME_REPORT);
                    String reportPhone = db.getConfigurationByKey(Common.Constant.TRIGGER_PHONE_REPORT);

                    // get all time to report
                    ArrayList<String> times = new ArrayList<>(Arrays.asList(reportTime.split(",")));
                    ArrayList<String> phones = new ArrayList<>(Arrays.asList(reportPhone.split(",")));

                    logger.error(times);
                    logger.error(phones);

                    if (times.contains(String.valueOf(hour))) {
                        logger.error("Start reporting");
                        reporting(hour, times, phones);
                    }
                } catch (Exception e) {
                    logger.error("Error processing", e);
                }
            }
        };
        final ScheduledFuture<?> reportHandle =
                scheduler.scheduleAtFixedRate(reporter, 0, 1, HOURS);
    }
}
