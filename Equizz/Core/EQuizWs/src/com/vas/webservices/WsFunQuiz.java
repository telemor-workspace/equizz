/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vas.webservices;

import com.vas.wsfw.common.Common;
import com.vas.wsfw.common.Encrypt;
import com.vas.wsfw.common.MessageResponse;
import com.vas.wsfw.common.WebserviceAbstract;

import static com.vas.wsfw.common.WebserviceAbstract.EXCEPTION;
import static com.vas.wsfw.common.WebserviceAbstract.PARAM_NOT_ENOUGH;
import static com.vas.wsfw.common.WebserviceAbstract.PARAM_NOT_VALID;
import static com.vas.wsfw.common.WebserviceAbstract.SUCCESS;
import static com.vas.wsfw.common.WebserviceAbstract.WRONG_PASSWORD;

import com.vas.wsfw.common.WebserviceManager;
import com.vas.wsfw.database.WsProcessUtils;
import com.vas.wsfw.obj.*;

import com.vas.wsfw.services.WSProcessor;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;

/**
 * @author Sungroup
 */
@WebService
public class WsFunQuiz extends WebserviceAbstract {

    public WsProcessUtils db;
    private StringBuilder br = new StringBuilder();
    //private Exchange exchange;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private SimpleDateFormat fullDf = new SimpleDateFormat("yyyyMMddHHmmss");
    private SimpleDateFormat reqDf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public WSProcessor ws;

    public WsFunQuiz() throws Exception {
        super("WsFunQuiz");
        db = new WsProcessUtils("dbProcess", logger);
        ws = new WSProcessor(logger, "../etc/webservice.cfg", "../etc/database.xml");
        if (Common.iLoadConfig) {
            Common.listConfig = db.getConfig("PROCESS");
            MessageResponse.setMessage(Common.listConfig);
            logger.info("LIST CONFIG:\n" + Common.listConfig);
            Common.listProduct = db.iLoadPackage();
            //Common.mapPrize = db.loadPrize();
            Common.mapMpsConfig = db.loadMpsConfig();
            Common.loadConfig();
        }
    }

    //
    // FOR MPS
    //
    public Response renewMps(String msisdn, ProductInfo product) {
        Response response = new Response();
        response.setErrorCode(SUCCESS);
        try {
            msisdn = formatMsisdn(msisdn);
            // after this, only one package returned
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, product.getProductName());
            if (listReg == null || listReg.isEmpty()) {
                logger.error("Renew but not registered: " + msisdn);
                response.setErrorCode(Common.ErrorCode.NOT_REGISTERED);
                response.setContent(MessageResponse.getDefaultMessage(Common.Message.NOT_REGISTERED, logger));
                return response;
            }
            // remove all package buy_code, just only packages daily
            RegisterInfo regInfo = listReg.get(0);

            ProductInfo productInfo = getProductByName(regInfo.getProductName());
            if (productInfo == null || productInfo.getProductName() == null) {
                logger.error("Error product info is not existed: " + regInfo.getProductName());
                response.setErrorCode(Common.ErrorCode.SYSTEM_ERROR);
                response.setContent(MessageResponse.getDefaultMessage(Common.Message.SYSTEM_FAIL, logger));
                return response;
            }
            double fee = productInfo.getFee();
            logger.info("Renew pending success " + msisdn + ", fee " + fee + ", account " + msisdn);

            // insert register
            Timestamp expire = getExpireTime(productInfo);
            regInfo.setExpireTime(expire);
            regInfo = updateRegRenew(regInfo, productInfo);
            db.updateRenewReg(regInfo);

            //insert chargelog
            if (fee > 0) {
                ChargeLog chargeLog = new ChargeLog();
                chargeLog.setFee(fee);
                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                chargeLog.setMsisdn(msisdn);
                chargeLog.setDescription("Register " + regInfo.getProductName());
                db.iInsertChargeLog(chargeLog);
            }

            // send message
            String message;
            message = MessageResponse.get(Common.Message.RENEW_SUCCESS,
                    Common.Message.RENEW_SUCCESS + "_" + regInfo.getProductName(), logger);
            message = message.replace("%fee%", fee + "");
            message = message.replaceAll("%expire%", sdf.format(expire));
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(msisdn);
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    public Response registerByMps(String msisdn, ProductInfo productInfo, int fee, String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
//        Timestamp reqTime = new Timestamp(System.currentTimeMillis());

        try {
            // check existed
            AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, String.valueOf(serviceId));
//            UserProfile userProfile = db.iGetUserProfileByMsisdn(msisdn);

            if (accountInfo == null) {
                logger.error("Error check exist account: " + msisdn);
                response.setErrorCode(Common.ErrorCode.QUERY_ERROR);
                String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                response.setContent(message);
                return response;
            }
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, productInfo.getProductName());
            if (listReg != null && !listReg.isEmpty()) {
                logger.info("Already registered: " + msisdn + " -> cancel service");
//                String message = MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger);
//                SmsMtObj mt = new SmsMtObj();
//                mt.setMsisdn(msisdn);
//                mt.setChannel(Common.CHANNEL);
//                mt.setMessage(message);
//                db.insertMt(mt);
//                response.setErrorCode(Common.ErrorCode.ALREADY_REGISTER);
//                response.setContent(message);
//                return response;
                db.disableAutoExtend(listReg.get(0).getRegisterId());
            }
//            ProductInfo productInfo = getProductByName(packageName);
//            double fee = productInfo.getFee();
            List<RegisterInfo> listRegInday = null;
            boolean registered = false;
            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
            if (listRegInday != null && listRegInday.size() > 0) {
                registered = true;
            }

            logger.info("Register success " + msisdn + ", fee " + fee + ", account " + accountInfo.getMsisdn() + ", pack = " + productInfo.getProductName());

            boolean isCreated = true;
            // create account
            String password = generateValidateCode(4);
            if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                isCreated = false;
                String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                db.insertAccountUser(msisdn, password, "USSD", serviceId);
            }

            // check user_info
//            if (userProfile.getMsisdn() == null || userProfile.getMsisdn().isEmpty()) {
////                isCreated = false;
//                db.insertUserProfile(msisdn);
//            }

            RegisterInfo reg;
            if (!registered) {
                // insert register
                logger.info("create new reg_info " + msisdn);
                reg = new RegisterInfo();
                reg.setMsisdn(msisdn);
                reg.setProductName(productInfo.getProductName());
                reg.setExpireTime(getExpireTime(productInfo));
                reg.setServiceId(Integer.parseInt(serviceId));
                reg = updateRegRenew(reg, productInfo);
                db.iInsertRegisterInfo(reg);

            } else {
                logger.info("register again reg_info " + msisdn);
                reg = listRegInday.get(0);
                reg.setExpireTime(getExpireTime(productInfo));
                db.iInsertRegisterInfo(reg);
            }
            // add points to their friends
            FqInviting inviter = null;
            inviter = db.getInvitingActive(msisdn);

            if (inviter != null && inviter.getOwnerMsisdn() != null) {
                // update status of fq_inviting to 0
                Boolean res = db.updateStatusInvitingFriend(inviter.getOwnerMsisdn(), msisdn, serviceId);
                if (res) {
                    // add point to owner_msisdn
                    db.iAddPointToAccountByMsisdn(inviter.getOwnerMsisdn(), serviceId);
                    String message2 = "";
                    message2 = MessageResponse.get(Common.Message.INVITE_FRIEND_REGISTER_SUCCESS,
                            Common.Message.INVITE_FRIEND_REGISTER_SUCCESS, logger);
                    message2 = message2.replaceAll("%friendmsisdn%", inviter.getFriendMsisdn());
                    // MT table for send sms to user
                    SmsMtObj mt = new SmsMtObj();
                    mt.setMsisdn(inviter.getOwnerMsisdn());
                    mt.setChannel(Common.CHANNEL);
                    mt.setMessage(message2);
                    db.insertMt(mt);
                }
            }

            //insert chargelog
            if (fee > 0) {
                ChargeLog chargeLog = new ChargeLog();
                chargeLog.setFee(fee);
                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                chargeLog.setMsisdn(msisdn);
                chargeLog.setDescription("Register " + productInfo.getProductName());
                db.iInsertChargeLog(chargeLog);
            }

            // send message
            String message = "";
            if (!isCreated) {
//                message = MessageResponse.get(Common.Message.REGISTER_SUCCESS + "_" + productInfo.getProductName(),
//                        Common.Message.REGISTER_SUCCESS, logger);
//                message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS, logger);
                message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS,
                        Common.Message.CREATE_ACCOUNT_SUCCESS, logger);
//                message = message.replace("%fee%", fee + "");
                message = message.replaceAll("%password%", password);
//                message = message.replaceAll("%expire%", sdf.format(reg.getExpireTime()));
                SmsMtObj mt = new SmsMtObj();
                mt.setMsisdn(msisdn);
                mt.setChannel(Common.CHANNEL);
                mt.setMessage(message);
                db.insertMt(mt);
            }
//            else {
//                message = MessageResponse.get(Common.Message.REGISTER_SUCCESS_USSD + "_" + productInfo.getProductName(),
//                        Common.Message.REGISTER_SUCCESS_USSD, logger);
//            }
//            message = MessageResponse.get(Common.Message.REGISTER_SUCCESS, logger);
            message = MessageResponse.get(Common.Message.REGISTER_SUCCESS,
                    Common.Message.REGISTER_SUCCESS, logger);
//            message = message.replaceAll("%package%", productInfo.getProductName());
            message = message.replaceAll("%fee%", String.valueOf(productInfo.getFee()));
            message = message.replaceAll("%spin%", String.valueOf(productInfo.getNumberSpin()));

//            if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_2))
//                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_2);
//            else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_3))
//                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_3);
//            else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_5))
//                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_5);

            if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_2)) {
                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_2);
                message = message.replaceAll("%package%", Common.Constant.QUIZ2 + "");
            } else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_3)) {
                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_3);
                message = message.replaceAll("%package%", Common.Constant.QUIZ3 + "");
            } else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_5)) {
                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_5);
                message = message.replaceAll("%package%", Common.Constant.QUIZ5 + "");
            }

            SmsMtObj mt = new SmsMtObj();
            mt.setMsisdn(msisdn);
            mt.setChannel(Common.CHANNEL);
            mt.setMessage(message);
            db.insertMt(mt);

            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    public Response cancelServiceMps(String msisdn, ProductInfo productInfo, String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            // check existed
            msisdn = formatMsisdn(msisdn);
            List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, productInfo.getProductName());
            if (listReg == null) {
                logger.error("Error check exist account: " + msisdn);
                response.setErrorCode(Common.ErrorCode.QUERY_ERROR);
                response.setContent("Error database!");
                return response;
            } else if (listReg.isEmpty()) {
                logger.info("Not registered: " + msisdn + " -> success");
//                response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_REGISTERED);
//                response.setContent("Not registered");
                String message = MessageResponse.get(Common.Message.DESTROY_ACCOUNT_SUCCESS, logger);
                response.setErrorCode(Common.ErrorCode.SUCCESS);
                response.setContent(message);
                return response;
            }
            db.disableAutoExtend(listReg.get(0).getRegisterId());
            // send message
            String message = MessageResponse.get(Common.Message.DESTROY_ACCOUNT_SUCCESS, logger);
//                        message = message.replaceAll("%expire_free%", shortDateFm.format(Common.FREE_DAY));
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(accountInfo.getMsisdn());
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
            // return
            response.setErrorCode(Common.ErrorCode.SUCCESS);
            response.setContent(message);

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "subRequest")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public String subRequest(
            @WebParam(name = "username") String wsUser,
            @WebParam(name = "password") String wsPass,
            @WebParam(name = "serviceid") String serviceid,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "chargetime") String chargetime,
            @WebParam(name = "params") String params,
            @WebParam(name = "subServiceName") String subServiceName,
            @WebParam(name = "amount") String fee,
            @WebParam(name = "transactionId") String transactionId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
        // translog
        Timestamp reqTime = new Timestamp(System.currentTimeMillis());
        TransactionLog transLog = new TransactionLog();
        transLog.setTransactionId(transactionId);
        transLog.setCommand(params);
        transLog.setMsisdn(msisdn);
        transLog.setRequestTime(reqTime);
        transLog.setRequest(serviceid + "|" + msisdn + "|" + chargetime + "|" + params + "|" + fee);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("subRequest Params:").
                    append("\nserviceid:").append(serviceid).
                    append("\nmsisdn:").append(msisdn).
                    append("\nchargetime:").append(chargetime).
                    append("\namount:").append(fee).
                    append("\nparams:").append(params).
                    append("\nsubServiceName:").append(subServiceName).
                    append("\ntransactionId:").append(transactionId);
            logger.info(br);

            Request request = new Request();
            request.setUser(msisdn);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (params == null || params.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("params is null");
            } else if (subServiceName == null || subServiceName.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("subServiceName is null");
            } else if (transactionId == null || transactionId.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("transactionId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                    return response.getErrorCode() + "|" + response.getContent();
                } else {
                    msisdn = formatMsisdn(msisdn);
                    ProductInfo productInfo = getProductByName(subServiceName);
                    if (productInfo == null || productInfo.getProductName() == null) {
                        logger.info("subServiceName is wrong: " + subServiceName);
                        response.setErrorCode(PARAM_NOT_VALID);
                        response.setContent("subServiceName is wrong");
                    } else {
                        if (params.equals("0")) {
                            int amount = Integer.parseInt(fee);
                            // register
//                        ProductInfo product = db.iLoadPackageByName(subServiceName);
                            response = registerByMps(msisdn, productInfo, amount, serviceid);
                        } else if (params.equals("1")) {
                            // unsub
                            response = cancelServiceMps(msisdn, productInfo, serviceid);
                        } else {
                            logger.info("Unknown params: " + params);
                            response.setErrorCode(PARAM_NOT_VALID);
                            response.setContent("Unknown params: " + params);
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        } finally {
            try {
                transLog.setErrorCode(response.getErrorCode());
                transLog.setResponse(response.toString());
                transLog.setResponseTime(new Timestamp(System.currentTimeMillis()));
                db.insertTransLog1(transLog);
            } catch (Exception ex) {
                logger.error("Error insert log", ex);
            } finally {
            }
        }

        return response.getErrorCode();
    }

    @WebMethod(operationName = "receiveresult")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public String receiveresult(
            @WebParam(name = "username") String wsUser,
            @WebParam(name = "password") String wsPass,
            @WebParam(name = "serviceid") String serviceid,
            @WebParam(name = "msisdn") String msisdn,
            @WebParam(name = "chargetime") String chargetime,
            @WebParam(name = "params") String params,
            @WebParam(name = "subServiceName") String subServiceName,
            @WebParam(name = "amount") String fee,
            @WebParam(name = "transactionId") String transactionId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);
        // translog
        Timestamp reqTime = new Timestamp(System.currentTimeMillis());
        TransactionLog transLog = new TransactionLog();
        transLog.setTransactionId(transactionId);
        transLog.setCommand(params);
        transLog.setMsisdn(msisdn);
        transLog.setRequestTime(reqTime);
        transLog.setRequest(serviceid + "|" + msisdn + "|" + chargetime + "|" + params + "|" + fee);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("receiveresult Params:").
                    append("\nmsisdn:").append(msisdn).
                    append("\nparams:").append(params).
                    append("\nchargetime:").append(chargetime).
                    append("\nserviceid:").append(serviceid).
                    append("\namount:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(msisdn);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (params == null || params.length() == 0) {
                logger.info("params is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("params is null");
            } else if (subServiceName == null || subServiceName.length() == 0) {
                logger.info("subServiceName is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("subServiceName is null");
            } else if (fee == null || fee.length() == 0) {
                logger.info("amount is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("amount is null");
            } else if (transactionId == null || transactionId.length() == 0) {
                logger.info("transactionId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("transactionId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                    return response.getErrorCode() + "|" + response.getContent();
                } else {
                    msisdn = formatMsisdn(msisdn);
                    if (params.equals("0")) {
                        ProductInfo productInfo = getProductByName(subServiceName);
                        renewMps(msisdn, productInfo);

                        // send message
//                    Timestamp expireFree = db.iGetExpireFree(msisdn);
//                    String message2 = MessageResponse.getDefaultMessage(autoRenew ? Common.Message.REGISTER_SUCCESS_USSD_RENEW : Common.Message.REGISTER_SUCCESS_USSD, logger);
//                    message2 = message2.replaceAll("%expire%", df.format(expireTime));
//                    message2 = message2.replaceAll("%expire_free%", shortDateFm.format(Common.FREE_DAY));
//                    message2 = message2.replaceAll("%fee%", Common.FEE + "");
//                    message2 = message2.replaceAll("%password%", password);
//                    SmsMtObj mt = new SmsMtObj();
//                    mt.setMsisdn(accountInfo.getMsisdn());
//                    mt.setChannel(Common.CHANNEL);
//                    mt.setMessage(message2);
//                    db.insertMt(mt);
                        // return
                        logger.info("Renew account success: " + msisdn);
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(Common.ResultCode.SUCCESS);
                        return response.getErrorCode();

                    } else {
                        logger.info("Renew fail: " + msisdn);
//                        logger.info("Unknown params: " + params);
//                        response.setErrorCode(PARAM_NOT_VALID);
//                        response.setContent("Unknown params: " + params);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        } finally {
            try {
                transLog.setErrorCode(response.getErrorCode());
                transLog.setResponse(response.toString());
                transLog.setResponseTime(new Timestamp(System.currentTimeMillis()));
                db.insertTransLog1(transLog);
            } catch (Exception ex) {
                logger.error("Error insert log", ex);
            } finally {
            }
        }

        return response.getErrorCode();
    }

    @WebMethod(operationName = "wsLogin")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsLogin(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Password") String password,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsLogin Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (password == null || password.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    AccountInfo accountInfo = db.iGetAccountByMsisdnAndPassword(msisdn, password, serviceId);
                    if ((accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty())) {
                        logger.info("account null " + msisdn);
                        response.setErrorCode(Common.ErrorCode.LOGIN_FAILURE);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.LOGIN_FAILURE, logger));
                    } else {
                        // get account_user by user and password
//                        String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);

                        // reset total spin and last update time if old last update time is invalid
                        Timestamp lastUpdate = getExpireTimeInDay(1);
                        Boolean reset = db.resetSpins(serviceId, lastUpdate);
                        if (!reset) {
                            logger.error("Can not reset spin for all user");
                        }

                        logger.info("login account success " + msisdn);
                        String message = "";
//                        AccountInfo getAccount = db.getAccountAndSpin(accountInfo.getAccountId(), serviceId);
//                        if (getAccount == null || getAccount.getMsisdn() == null) {
//                            Timestamp lastUpdate = getExpireTimeInDay(1);
//                            Boolean updateAccount = db.updateAccountAndSpin(accountInfo.getAccountId(), lastUpdate);
//                            if (!updateAccount) {
//                                logger.info("getaccount null and can not update");
//                                message = MessageResponse.get(Common.Message.LOGIN_FAIL,
//                                        Common.Message.LOGIN_SUCCESS, logger);
//                                response.setErrorCode(Common.ErrorCode.FAILURE);
//                            } else {
//                                logger.info("getaccount null and update successful");
//                                message = MessageResponse.get(Common.Message.LOGIN_SUCCESS,
//                                        Common.Message.LOGIN_SUCCESS, logger);
//                                response.setErrorCode(Common.ErrorCode.SUCCESS);
//                            }
//                        } else {
//                            logger.info("getaccount successful");
//                            message = MessageResponse.get(Common.Message.LOGIN_SUCCESS,
//                                    Common.Message.LOGIN_SUCCESS, logger);
//                            response.setErrorCode(Common.ErrorCode.SUCCESS);
//                        }
                        // response message to web
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        message = MessageResponse.get(Common.Message.LOGIN_SUCCESS,
                                Common.Message.LOGIN_SUCCESS, logger);
                        response.setContent(message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsCreateFree")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsCreateFree(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Channel") String channel,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsCreateFree Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if (accountInfo == null || accountInfo.getMsisdn() != null && !accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        // create a free account -> insert to account_user table and fq_user_info
                        String password = generateValidateCode(4);
                        String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                        db.insertAccountUser(msisdn, password, channel, serviceId);
//                        db.createUserInfo(msisdn);
                        logger.info("Create account success " + msisdn);

                        // send sms password to user
                        String message = "";
                        message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS,
                                Common.Message.CREATE_ACCOUNT_SUCCESS, logger);

                        // MT table for send sms to user
                        // send message
                        message = message.replaceAll("%password%", password);
                        SmsMtObj mt = new SmsMtObj();
                        mt.setMsisdn(msisdn);
                        mt.setChannel(Common.CHANNEL);
                        mt.setMessage(message);
                        db.insertMt(mt);

                        // response message to web
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsInviteFriends")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsInviteFriends(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "FriendMsisdn") String friendMsisdn,
            @WebParam(name = "Channel") String channel,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsInviteFriends Params:\n").
                    append("WsUser:").
                    append(wsUser).
                    append("\nMsisdn:").
                    append(msisdn).
                    append("\nFriendMsisdn: ").
                    append(friendMsisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (friendMsisdn == null || friendMsisdn.length() == 0) {
                logger.info("friendMsisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("friendMsisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    friendMsisdn = formatMsisdn(friendMsisdn);

                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if ((accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) || msisdn.equals(friendMsisdn)) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {

                        // add points to their friends
                        FqInviting inviter = null;
                        inviter = db.getInviting(friendMsisdn);

                        if (inviter != null && inviter.getFriendMsisdn() != null) {
                            logger.info("Log inviting friend failure " + msisdn + " friend: " + friendMsisdn);
                            String message = "";
                            message = MessageResponse.get(Common.Message.INVITE_FRIEND_FAILURE,
                                    Common.Message.INVITE_FRIEND_FAILURE, logger);
                            // response message to web
                            response.setErrorCode(Common.ErrorCode.FRIEND_EXISTED);
                            response.setContent(message);
                        } else {
                            // update to inviting table
                            db.createInviting(msisdn, friendMsisdn, channel, serviceId);

                            logger.info("Log inviting friend successful " + msisdn + " friend: " + friendMsisdn);

                            // send sms password to user
                            String message = "";
                            message = MessageResponse.get(Common.Message.INVITE_FRIEND_SUCCESS,
                                    Common.Message.INVITE_FRIEND_SUCCESS, logger);
                            message = message.replaceAll("%msisdn%", friendMsisdn);
                            // MT table for send sms to user
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            String message2 = "";
                            message2 = MessageResponse.get(Common.Message.INVITE_TO_SERVICE_SUCCESS,
                                    Common.Message.INVITE_TO_SERVICE_SUCCESS, logger);
                            // MT table for send sms to user
                            SmsMtObj mt2 = new SmsMtObj();
                            mt2.setMsisdn(friendMsisdn);
                            mt2.setChannel(Common.CHANNEL);
                            mt2.setMessage(message2);
                            db.insertMt(mt2);

                            // response message to web
                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsResetPass")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsResetPass(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsResetPass Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if (accountInfo == null || accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        // create a free account -> insert to account_user table and fq_user_info
                        String password = generateValidateCode(4);
                        String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                        db.updateAccountUser(msisdn, password);
                        logger.info("Update account success " + msisdn);

                        // send sms password to user
                        String message = "";
                        message = MessageResponse.get(Common.Message.RESET_PASSWORD_SUCCESS,
                                Common.Message.RESET_PASSWORD_SUCCESS, logger);
                        message = message.replaceAll("%password%", password);
                        // MT table for send sms to user
                        SmsMtObj mt = new SmsMtObj();
                        mt.setMsisdn(msisdn);
                        mt.setChannel(Common.CHANNEL);
                        mt.setMessage(message);
                        db.insertMt(mt);

                        // response message to web
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsChangePass")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChangePass(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "OldPassword") String oldPassword,
            @WebParam(name = "NewPassword") String newPassword,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsChangePass Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (oldPassword == null || oldPassword.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            } else if (newPassword == null || newPassword.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if ((accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty())) {
                        logger.info("account null " + msisdn);
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        // update password
                        // check pass
//                        String encyptedOldPassword = Encrypt.getHashCode(msisdn, oldPassword, logger);
                        if (accountInfo.getPassword().equals(oldPassword)) {
//                            String encyptedNewPassword = Encrypt.getHashCode(msisdn, newPassword, logger);
                            db.updateAccountUser(msisdn, newPassword);
                            logger.info("Update account success " + msisdn);

                            // send sms password to user
                            String message = "";
                            message = MessageResponse.get(Common.Message.UPDATE_PASSWORD_SUCCESS,
                                    Common.Message.UPDATE_PASSWORD_SUCCESS, logger);

                            // send MT to the user
//                            SmsMtObj mt = new SmsMtObj();
//                            mt.setMsisdn(msisdn);
//                            mt.setChannel(Common.CHANNEL);
//                            mt.setMessage(message);
//                            db.insertMt(mt);

                            // response message to web
                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else {
                            logger.info("wrong password " + msisdn);
                            // send sms password to user
                            String message = "";
                            message = MessageResponse.get(Common.Message.WRONG_INFO_TO_UPDATE,
                                    Common.Message.WRONG_INFO_TO_UPDATE, logger);
                            // response message to web
                            response.setErrorCode(Common.ErrorCode.UPDATE_PASSWORD_FAILURE);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsBuyCode")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsBuyCode(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsBuyCode Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId).
                    append("\nPackageName:").append(packageName);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // check account
                    msisdn = formatMsisdn(msisdn);
                    // check account
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    // create account
                    if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
                        // not waiting confirm
                        String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());
                        resultPaid = ws.chargeFeeOtp(msisdn, fee, REQ);

//                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
//                            logger.info("Create otp success " + msisdn + ", fee " + fee);
//
//                            // send message
//                            String message = MessageResponse.get(Common.Message.REGISTER_WAIT_CONFIRM + "_" + productInfo.getProductName(),
//                                    Common.Message.REGISTER_WAIT_CONFIRM, logger);
//
//                            response.setErrorCode(Common.ErrorCode.SUCCESS);
//                            response.setContent(message);
//                            response.setResultCode(REQ);
//                        } else {
//                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
//                            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
//                            response.setContent(message);
//                        }

                        boolean registered = false;
                        List<RegisterInfo> listRegInday = null;
                        listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
                        if (listRegInday != null && listRegInday.size() > 0) {
                            registered = true;
                        }

                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Buy code success success " + msisdn + ", fee " + fee);
                            Timestamp expireTime = getExpireTime(productInfo);
                            // check registered and write the next action to database
                            RegisterInfo reg;
                            if (!registered) {
                                // insert register
                                reg = new RegisterInfo();
                                reg.setMsisdn(msisdn);
                                reg.setProductName(productInfo.getProductName());
                                reg.setExpireTime(expireTime);
                                reg.setServiceId(Integer.parseInt(serviceId));
                                reg = updateRegRenew(reg, productInfo);
                                db.iInsertRegisterInfo(reg);
                            } else {
                                reg = listRegInday.get(0);
                                db.iInsertRegisterInfo(reg);
                            }

                            //insert chargelog
                            if (fee > 0) {
                                ChargeLog chargeLog = new ChargeLog();
                                chargeLog.setFee(productInfo.getFee());
                                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                chargeLog.setMsisdn(msisdn);
                                chargeLog.setDescription("Register " + packageName);
                                db.iInsertChargeLog(chargeLog);
                            }

                            // send message
                            String message = "";
                            message = MessageResponse.get(Common.Message.BUY_SPIN_SUB_SUCCESS + "_" + productInfo.getProductName(),
                                    Common.Message.BUY_SPIN_SUB_SUCCESS, logger);
//                            message = message.replace("%fee%", productInfo.getFee() + "");
//                            message = message.replaceAll("%expire%", sdf.format(expireTime));
                            // MT table for send sms to user
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            String message = MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger);
                            response.setContent(message);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }

                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsBuyCodeConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsBuyCodeConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsBuyCodeConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName).
                    append("\nserviceId:").append(serviceId).
                    append("\nrequestId:").append(requestId).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (otp == null || otp.length() == 0) {
                logger.info("otp is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("otp is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    List<RegisterInfo> listReg = db.getRegisterInfo(msisdn, packageName);
                    if (listReg == null || listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();

                        boolean registered = false;
                        List<RegisterInfo> listRegInday = null;
                        listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
                        if (listRegInday != null && listRegInday.size() > 0) {
                            registered = true;
                        }
//                        boolean registered = false;
//                        if (!db.checkRegistered(msisdn, productInfo.getProductName())) {
//                            // if user is not existed
//                            fee = 0;
//                        } else {
//                            // user existed
//                            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
//                            if (listRegInday != null && listRegInday.size() > 0) {
//                                // the packet is valid, user can play
//                                fee = 0;
//                                registered = true;
//                            }
//                        }

                        resultPaid = ws.chargeFeeConfirm(msisdn, fee, otp, requestId);

                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Buy code success success " + msisdn + ", fee " + fee);
                            Timestamp expireTime = getExpireTime(productInfo);
                            // check registered and write the next action to database
                            RegisterInfo reg;
                            if (!registered) {
                                // insert register
                                reg = new RegisterInfo();
                                reg.setMsisdn(msisdn);
                                reg.setProductName(productInfo.getProductName());
                                reg.setExpireTime(expireTime);
                                reg.setServiceId(Integer.parseInt(serviceId));
                                reg = updateRegRenew(reg, productInfo);
                                db.iInsertRegisterInfo(reg);
                            } else {
                                reg = listRegInday.get(0);
                                db.iInsertRegisterInfo(reg);
                            }

                            //insert chargelog
                            if (fee > 0) {
                                ChargeLog chargeLog = new ChargeLog();
                                chargeLog.setFee(productInfo.getFee());
                                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                chargeLog.setMsisdn(msisdn);
                                chargeLog.setDescription("Register " + packageName);
                                db.iInsertChargeLog(chargeLog);
                            }

                            // send message
                            String message = "";
                            message = MessageResponse.get(Common.Message.BUY_SPIN_SUB_SUCCESS + "_" + productInfo.getProductName(),
                                    Common.Message.BUY_SPIN_SUB_SUCCESS, logger);
//                            message = message.replace("%fee%", productInfo.getFee() + "");
//                            message = message.replaceAll("%expire%", sdf.format(expireTime));
                            // MT table for send sms to user
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                        } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            String message = MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger);
                            response.setContent(message);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsRegisterSubOtp")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsRegisterSubOtp(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId).
                    append("\nPackageName:").append(packageName);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    List<RegisterInfo> listReg = db.getRegisterInday(msisdn, packageName);
                    if (listReg != null && !listReg.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ALREADY_REGISTER);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
                        // not waiting confirm
                        String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());

                        // right -----------------------------------------------
                        resultPaid = ws.requestMpsOtp(msisdn, fee, REQ, productInfo.getProductName());
                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Create otp success " + msisdn + ", fee " + fee);

                            // send message
                            String message = MessageResponse.get(Common.Message.REGISTER_WAIT_CONFIRM + "_" + productInfo.getProductName(),
                                    Common.Message.REGISTER_WAIT_CONFIRM, logger);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                            response.setResultCode(REQ);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsRegisterSubConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsRegisterSubConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsRegisterSubConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nPackageName:").append(packageName).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (otp == null || otp.length() == 0) {
                logger.info("otp is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("otp is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    List<RegisterInfo> listRegInday = db.getRegisterInday(msisdn, packageName);
                    if (listRegInday != null && !listRegInday.isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ALREADY_REGISTER);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ALREADY_REGISTERED, logger));
                    } else {
                        // da dang ki (status = 1) nhung he thong chua kip gia han
                        ProductInfo productInfo = getProductByName(packageName);
                        String resultPaid = "0";
                        int fee = (int) productInfo.getFee();
//                        if (productInfo.getFee() > 0) {

//                        List<RegisterInfo> listRegInday = null;

                        boolean registered = false;
                        if (!db.checkRegistered(msisdn, productInfo.getProductName())) {
                            // new user
                            fee = 0;
                        } else {
//                            listRegInday = db.getRegisterInday(msisdn, productInfo.getProductName());
                            if (listRegInday != null && listRegInday.size() > 0) {
                                fee = 0;
                                registered = true;
                            }
                        }

                        resultPaid = ws.registerMpsConfirm(msisdn, fee, otp, requestId, productInfo.getProductName());

                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Register success success " + msisdn + ", fee " + fee);
                            Timestamp expireTime = getExpireTime(productInfo);
                            // check registered
                            RegisterInfo reg;
                            if (!registered) {
                                // insert register
                                reg = new RegisterInfo();
                                reg.setMsisdn(msisdn);
                                reg.setProductName(productInfo.getProductName());
                                reg.setExpireTime(expireTime);
                                reg.setServiceId(Integer.parseInt(serviceId));
                                reg = updateRegRenew(reg, productInfo);
                                db.iInsertRegisterInfo(reg);
                            } else {
                                reg = listRegInday.get(0);
                                db.iInsertRegisterInfo(reg);
                            }

                            //insert chargelog
                            if (fee > 0) {
                                ChargeLog chargeLog = new ChargeLog();
                                chargeLog.setFee(productInfo.getFee());
                                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                chargeLog.setMsisdn(msisdn);
                                chargeLog.setDescription("Register " + packageName);
                                db.iInsertChargeLog(chargeLog);
                            }

                            // create a new account_user for this user
                            boolean isCreated = true;
                            // check existed
                            AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                            // create account
                            String password = generateValidateCode(4);
                            if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                                isCreated = false;
                                String encyptedPassword = Encrypt.getHashCode(msisdn, password, logger);
                                logger.info("Create account success " + msisdn);
                                db.insertAccountUser(msisdn, password, "WEB", serviceId);
                            }
                            // send message
                            String message = "";
                            if (!isCreated) {
                                // message = MessageResponse.get(Common.Message.REGISTER_SUCCESS + "_" + productInfo.getProductName(),
                                // Common.Message.REGISTER_SUCCESS, logger);
                                message = MessageResponse.get(Common.Message.CREATE_ACCOUNT_SUCCESS, logger);
//                                message = message.replace("%fee%", fee + "");
                                message = message.replaceAll("%password%", password);
//                                message = message.replaceAll("%expire%", sdf.format(reg.getExpireTime()));
                                SmsMtObj mt = new SmsMtObj();
                                mt.setMsisdn(msisdn);
                                mt.setChannel(Common.CHANNEL);
                                mt.setMessage(message);
                                db.insertMt(mt);
                            }

                            // add points to their friends

                            // add points to their friends
                            FqInviting inviter = null;
                            inviter = db.getInvitingActive(msisdn);

                            if (inviter != null && inviter.getOwnerMsisdn() != null) {
                                // update status of fq_inviting to 0
                                Boolean res = db.updateStatusInvitingFriend(inviter.getOwnerMsisdn(), msisdn, serviceId);
                                if (res) {
                                    // add point to owner_msisdn
                                    db.iAddPointToAccountByMsisdn(inviter.getOwnerMsisdn(), serviceId);
                                    String message2 = "";
                                    message2 = MessageResponse.get(Common.Message.INVITE_FRIEND_REGISTER_SUCCESS,
                                            Common.Message.INVITE_FRIEND_REGISTER_SUCCESS, logger);
                                    message2 = message2.replaceAll("%friendmsisdn%", inviter.getFriendMsisdn());
                                    // MT table for send sms to user
                                    SmsMtObj mt = new SmsMtObj();
                                    mt.setMsisdn(inviter.getOwnerMsisdn());
                                    mt.setChannel(Common.CHANNEL);
                                    mt.setMessage(message2);
                                    db.insertMt(mt);
                                }
                            }

                            // send message
                            message = MessageResponse.get(Common.Message.REGISTER_SUCCESS,
                                    Common.Message.REGISTER_SUCCESS, logger);
                            message = message.replace("%fee%", productInfo.getFee() + "");
                            message = message.replace("%spin%", productInfo.getNumberSpin() + "");

                            if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_2)) {
                                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_2);
                                message = message.replaceAll("%package%", Common.Constant.QUIZ2 + "");
                            } else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_3)) {
                                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_3);
                                message = message.replaceAll("%package%", Common.Constant.QUIZ3 + "");
                            } else if (productInfo.getProductName().equals(Common.Constant.REGISTER_DAILY_5)) {
                                message = message.replaceAll("%command%", Common.Constant.OFF_DAILY_5);
                                message = message.replaceAll("%package%", Common.Constant.QUIZ5 + "");
                            }
//                            message = message.replaceAll("%expire%", sdf.format(expireTime));
                            // MT table for send sms to user
                            SmsMtObj mt = new SmsMtObj();
                            mt.setMsisdn(msisdn);
                            mt.setChannel(Common.CHANNEL);
                            mt.setMessage(message);
                            db.insertMt(mt);

                            // check user have registered in the past
                            if (!db.checkRegisteredForTheFirstTime(msisdn, serviceId)) {
                                response.setErrorCode(Common.ErrorCode.SUCCESS_FOR_THE_FIRST_TIME_REGISTER);
                            } else {
                                response.setErrorCode(Common.ErrorCode.SUCCESS);
                            }

                            response.setContent(message);
                        } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            String message = MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger);
                            response.setContent(message);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsChargeFeeOtp")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChargeFeeOtp(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Fee") String fee,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsChargeFeeOtp Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\n Msisdn:").append(msisdn).
                    append("\n serviceId:").append(serviceId).
                    append("\n Fee:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (fee == null || fee.length() == 0) {
                logger.info("fee is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("fee is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // check account
                    msisdn = formatMsisdn(msisdn);
                    // check account
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    // create account
                    if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        int moneyCharge = Common.ADD_SPIN_FEE;
                        if (fee != null && fee.length() > 0) {
                            moneyCharge = Integer.parseInt(fee);
                        }

                        if (moneyCharge < 0) {
                            response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                            response.setContent("Charge fail");
                            return response;
                        }
                        // not waiting confirm
                        String REQ = reqDf.format(new Date()) + "1" + msisdn.substring(Common.COUNTRY_CODE.length());
                        String resultPaid = ws.chargeFeeOtp(msisdn, moneyCharge, REQ);
                        if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                            logger.info("Create otp success " + msisdn + ", fee " + moneyCharge);

                            // send message
                            String message = MessageResponse.get(Common.Message.CHARGE_WAIT_CONFIRM, logger);
                            message = message.replaceAll("%money%", fee);

                            response.setErrorCode(Common.ErrorCode.SUCCESS);
                            response.setContent(message);
                            response.setResultCode(REQ);
                        } else {
                            response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
                            response.setContent(message);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsChargeFeeConfirm")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsChargeFeeConfirm(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Fee") String fee,
            @WebParam(name = "RequestId") String requestId,
            @WebParam(name = "Otp") String otp,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsChargeFeeConfirm Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nMsisdn:").append(msisdn).
                    append("\nFee:").append(fee).
                    append("\nServiceId:").append(serviceId).
                    append("\nOtp:").append(otp);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    int moneyCharge = Common.ADD_SPIN_FEE;
                    if (fee != null && fee.length() > 0) {
                        moneyCharge = Integer.parseInt(fee);
                    }
                    if (moneyCharge < 0) {
                        response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                        response.setContent("Charge fail");
                        return response;
                    }
                    // charge
                    String resultPaid = ws.chargeFeeConfirm(msisdn, moneyCharge, otp, requestId);

                    if (WSProcessor.LIST_SUCCESS_RES.contains(resultPaid)) {
                        //insert chargelog
                        if (moneyCharge > 0) {
                            ChargeLog chargeLog = new ChargeLog();
                            chargeLog.setFee(moneyCharge);
                            chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                            chargeLog.setMsisdn(msisdn);
                            chargeLog.setDescription("Charge");
                            db.iInsertChargeLog(chargeLog);
                        }

                        processAfterCharge(msisdn, moneyCharge);

                        String message = MessageResponse.get(Common.Message.CHARGE_FEE_SUCCESS, logger);
                        message = message.replace("%fee%", moneyCharge + "");
//                        SmsMtObj mt = new SmsMtObj();
//                        mt.setMsisdn(msisdn);
//                        mt.setChannel(Common.CHANNEL);
//                        mt.setMessage(message);
//                        db.insertMt(mt);
                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        response.setContent(message);
                    } else if (WSProcessor.NOT_ENOUGH_MONEY_RES.contains(resultPaid)) {
                        response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_MONEY);
                        response.setContent(MessageResponse.get(Common.Message.NOT_ENOUGH_BALANCE, logger));
                    } else {
                        response.setErrorCode(Common.ErrorCode.CHARGE_ERROR);
                        response.setContent(MessageResponse.get(Common.Message.SYSTEM_FAIL, logger));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }

        return response;
    }

    @WebMethod(operationName = "wsUserExchangeMoney")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsUserExchangeMoney(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "Point") String point,
            @WebParam(name = "Fee") String fee,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsUserExchangeMoney Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\n Msisdn:").append(msisdn).
                    append("\n Fee:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (packageName == null || packageName.length() == 0) {
                logger.info("packageName is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("packageName is null");
            } else if (fee == null || fee.length() == 0) {
                logger.info("fee is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("fee is null");
            } else if (point == null || point.length() == 0) {
                logger.info("point is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("point is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    // check account
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    // create account
                    if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {
                        String enable = db.getConfiguration(Common.Constant.PLAYING_TIMES_ENABLE, Common.Constant.FUNQUIZ_SERVICEID);
                        if (enable.equals("1")) {
                            int money = Integer.parseInt(fee);
                            // get total exchange per day , max is 100HTG
                            Timestamp fromDate = getExactTimeInDay(0, 0, 0);
                            Timestamp toDate = getExactTimeInDay(23, 59, 59);

                            List<ChargeLog> chargeLogs = db.getChargeLogAddMoneyHistory(fromDate, toDate);
                            boolean check = true;
                            if (chargeLogs != null && !chargeLogs.isEmpty()) {
                                double total = 0;
                                for (int x = 0; x < chargeLogs.size(); x++) {
                                    total += (-chargeLogs.get(x).getFee());
                                }
                                if (total >= 100) {
                                    response.setErrorCode(Common.ErrorCode.OVER_LIMIT_EXCHANGE);
                                    String message = MessageResponse.get(Common.Message.EXCHANGE_FAILURE, logger);
                                    response.setContent(message);
                                    check = false;
                                }
                            }
                            // check account
                            if (check) {
                                if (Integer.parseInt(accountInfo.getPoint()) >= Integer.parseInt(point)) {
                                    // reduce the point of this user
                                    Boolean status = db.updatePointAccountUser(msisdn, point, serviceId);
                                    if (!status) {
                                        response.setErrorCode(Common.ErrorCode.SYSTEM_ERROR);
                                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.SYSTEM_FAIL, logger));
                                    } else {
                                        ws.addMoney(msisdn, money);

                                        ChargeLog chargeLog = new ChargeLog();
                                        chargeLog.setFee(-money);
                                        chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                        chargeLog.setMsisdn(msisdn);
                                        chargeLog.setDescription("Exchange points to money");
                                        db.iInsertChargeLog(chargeLog);

                                        response.setErrorCode(Common.ErrorCode.SUCCESS);
//                                String message = MessageResponse.get(Common.Message.EXCHANGE_SUCCESS, logger);
                                        String message = MessageResponse.get(Common.Message.EXCHANGE_SUCCESS,
                                                Common.Message.EXCHANGE_SUCCESS, logger);
                                        message = message.replaceAll("%point%", point + "");
                                        message = message.replaceAll("%money%", money + "");
                                        response.setContent(message);

                                        SmsMtObj mt = new SmsMtObj();
                                        mt.setMsisdn(msisdn);
                                        mt.setChannel(Common.CHANNEL);
                                        mt.setMessage(message);
                                        db.insertMt(mt);
                                    }
                                } else {
                                    response.setErrorCode(Common.ErrorCode.NOT_ENOUGH_POINT);
                                    String message = MessageResponse.get(Common.Message.EXCHANGE_FAILURE, logger);
                                    response.setContent(message);
//                            SmsMtObj mt = new SmsMtObj();
//                            mt.setMsisdn(msisdn);
//                            mt.setChannel(Common.CHANNEL);
//                            mt.setMessage(message);
//                            db.insertMt(mt);
                                }
                            }
                        } else {
                            response.setErrorCode(Common.ErrorCode.SYSTEM_ERROR);
                            response.setContent(MessageResponse.getDefaultMessage(Common.Message.SYSTEM_FAIL, logger));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsAddMoney")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAddMoney(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "PackageName") String packageName,
            @WebParam(name = "Fee") String fee,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsAddMoney Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\n Msisdn:").append(msisdn).
                    append("\n PackageName:").append(packageName).
                    append("\n Msisdn:").append(msisdn).
                    append("\n ServiceId:").append(serviceId).
                    append("\n Fee:").append(fee);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (packageName == null || packageName.length() == 0) {
                logger.info("packageName is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("packageName is null");
            } else if (fee == null || fee.length() == 0) {
                logger.info("fee is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("fee is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // kiem tra thong tin dang nhap
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    // check account
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    // create account
                    if (accountInfo.getMsisdn() == null || accountInfo.getMsisdn().isEmpty()) {
                        response.setErrorCode(Common.ErrorCode.ACCOUNT_NOT_EXISTED);
                        response.setContent(MessageResponse.getDefaultMessage(Common.Message.ACCOUNT_NOT_EXISTED, logger));
                    } else {

                        int money = Integer.parseInt(fee);
                        ws.addMoney(msisdn, money);

                        ChargeLog chargeLog = new ChargeLog();
                        chargeLog.setFee(-money);
                        chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                        chargeLog.setMsisdn(msisdn);
                        chargeLog.setDescription("Exchange points to money");
                        db.iInsertChargeLog(chargeLog);

                        response.setErrorCode(Common.ErrorCode.SUCCESS);
                        String message = MessageResponse.get(Common.Message.EXCHANGE_SUCCESS, logger);
//                        message = message.replaceAll("%point%", point + "");
                        message = message.replaceAll("%money%", money + "");
                        response.setContent(message);

                        SmsMtObj mt = new SmsMtObj();
                        mt.setMsisdn(msisdn);
                        mt.setChannel(Common.CHANNEL);
                        mt.setMessage(message);
                        db.insertMt(mt);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }

        return response;
    }

    @WebMethod(operationName = "wsGetDataByParentId")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetDataByParentId(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ParentId") String parentId,
            @WebParam(name = "SeqPage") String seqPage,
            @WebParam(name = "RowsOnPage") String rowsOnPage,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetDataByParentId Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nparentId:").append(parentId).
                    append("\nseqPage:").append(seqPage).
                    append("\nrowsOnPage:").append(rowsOnPage).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (parentId == null || parentId.length() == 0) {
                logger.info("parentId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("parentId is null");
            } else if (seqPage == null || seqPage.length() == 0) {
                logger.info("seqPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("seqPage is null");
            } else if (rowsOnPage == null || rowsOnPage.length() == 0) {
                logger.info("rowsOnPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("rowsOnPage is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<Game> listGames = db.getDataByParentId(parentId, seqPage, rowsOnPage, serviceId);
                    if (listGames == null || listGames.isEmpty()) {
                        response.setErrorCode(NO_DATA);
                    } else {
                        // get total pages
                        int totalPage = db.getTotalPage(parentId, seqPage, rowsOnPage, serviceId);
                        response.setTotalPage(String.valueOf(totalPage));
                        // get account_user
                        if (msisdn != null && !msisdn.equals("")) {
                            AccountInfo account = db.iGetAccountByMsisdn(msisdn, serviceId);
                            if (account != null) {
                                if (account.getLanguage().equals(Common.Constant.FOREIGN_LANGUAGE)) {
                                    for (int i = 0; i < listGames.size(); i++) {
                                        listGames.get(i).setName(listGames.get(i).getNameLocal());
                                        listGames.get(i).setIntroduction(listGames.get(i).getIntroductionLocal());
                                        listGames.get(i).setDescription(listGames.get(i).getDescriptionLocal());
                                    }
                                } else {
                                    for (int i = 0; i < listGames.size(); i++) {
                                        listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                        listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                        listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                                    }
                                }
                            } else {
                                for (int i = 0; i < listGames.size(); i++) {
                                    listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                    listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                    listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                                }
                            }
                        } else {
                            for (int i = 0; i < listGames.size(); i++) {
                                listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                            }
                        }
                        response.setListGames(listGames);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetDataById")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetDataById(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Id") String id,
            @WebParam(name = "SeqPage") String seqPage,
            @WebParam(name = "RowsOnPage") String rowsOnPage,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetDataById Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nid:").append(id).
                    append("\nseqPage:").append(seqPage).
                    append("\nrowsOnPage:").append(rowsOnPage).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (id == null || id.length() == 0) {
                logger.info("parentId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("parentId is null");
            } else if (seqPage == null || seqPage.length() == 0) {
                logger.info("seqPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("seqPage is null");
            } else if (rowsOnPage == null || rowsOnPage.length() == 0) {
                logger.info("rowsOnPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("rowsOnPage is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<Game> listGames = db.getDataById(id, seqPage, rowsOnPage, serviceId);
                    if (listGames == null || listGames.isEmpty()) {
                        response.setErrorCode(NO_DATA);
                    } else {
                        if (msisdn != null && !msisdn.equals("")) {
                            AccountInfo account = db.iGetAccountByMsisdn(msisdn, serviceId);
                            if (account != null) {
                                if (account.getLanguage().equals(Common.Constant.FOREIGN_LANGUAGE)) {
                                    for (int i = 0; i < listGames.size(); i++) {
                                        listGames.get(i).setName(listGames.get(i).getNameLocal());
                                        listGames.get(i).setIntroduction(listGames.get(i).getIntroductionLocal());
                                        listGames.get(i).setDescription(listGames.get(i).getDescriptionLocal());
                                    }
                                } else {
                                    for (int i = 0; i < listGames.size(); i++) {
                                        listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                        listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                        listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                                    }
                                }
                            } else {
                                for (int i = 0; i < listGames.size(); i++) {
                                    listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                    listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                    listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                                }
                            }
                        } else {
                            for (int i = 0; i < listGames.size(); i++) {
                                listGames.get(i).setName(listGames.get(i).getNameGlobal());
                                listGames.get(i).setIntroduction(listGames.get(i).getIntroductionGlobal());
                                listGames.get(i).setDescription(listGames.get(i).getDescriptionGlobal());
                            }
                        }
                        response.setListGames(listGames);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsUpdateUserPlaying")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsUpdateUserPlaying(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsUpdateUserPlaying Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    // check the number of turn of this user
                    AccountInfo accountInfo = db.iGetAccountByMsisdn(msisdn, serviceId);
                    if (Integer.parseInt(accountInfo.getFreeSpin()) > 0) {
                        Boolean reduce = db.reduceFreeSpin(msisdn, serviceId);
                        if (!reduce) {
                            response.setErrorCode(FAILURE);
                        } else {
                            response.setStatus(Common.Constant.IN_TURN);
                            response.setErrorCode(SUCCESS);
                        }
                    } else {
                        List<RegisterInfo> infos = db.getUserServices(msisdn, serviceId);
                        for (int i = 0; i < infos.size(); i++) {
                            logger.info("package: " + infos.get(i).getProductName());
                            logger.info("getPlayedTimes: " + infos.get(i).getPlayedTimes());
                            logger.info("getNumberSpin: " + infos.get(i).getNumberSpin());

                            if (infos.get(i).getNumberSpin() > infos.get(i).getPlayedTimes()) {
                                Boolean status = db.updateUserPlaying(infos.get(i).getRegisterId(), msisdn, serviceId);
                                if (!status) {
                                    response.setErrorCode(FAILURE);
                                } else {
                                    response.setStatus(Common.Constant.IN_TURN);
                                    response.setErrorCode(SUCCESS);
                                }
                                return response;
                            }
                        }
                        response.setErrorCode(SUCCESS);
                        response.setStatus(Common.Constant.OUT_OF_TURN);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsUpdateUserPoint")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsUpdateUserPoint(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "Point") String point,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsUpdateUserPlaying Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\npoint:").append(point).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (point == null || point.length() == 0) {
                logger.info("point is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("point is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);

                    // check the account
                    Boolean status = db.updateUserPoint(msisdn, point, serviceId);
                    if (!status) {
                        response.setErrorCode(FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetRanking")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetRanking(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "FromDate") String fromDate,
            @WebParam(name = "ToDate") String toDate,
            @WebParam(name = "SeqPage") String seqPage,
            @WebParam(name = "RowsOnPage") String rowsOnPage,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetRanking Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\nseqPage:").append(seqPage).
                    append("\nfromDate:").append(fromDate).
                    append("\ntoDate:").append(toDate).
                    append("\nrowsOnPage:").append(rowsOnPage).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (seqPage == null || seqPage.length() == 0) {
                logger.info("seqPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("seqPage is null");
            } else if (rowsOnPage == null || rowsOnPage.length() == 0) {
                logger.info("rowsOnPage is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("rowsOnPage is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    // get accounts in top 10 who played many times
                    List<AccountInfo> accountPlayed = db.getAccountsByPlayedTime(msisdn, seqPage, rowsOnPage, serviceId);

                    // get accounts in top 2 who won the minigame
                    List<AccountInfo> accountWon = new ArrayList<>();
                    List<MinigameReward> rewards = db.getUserRewardByTime(fromDate, toDate, serviceId);
                    if (rewards != null && !rewards.isEmpty()) {
                        // get account
                        for (int w = 0; w < rewards.size(); w++) {
//                            logger.error("msisdn: " + rewards.get(w).getMsisdn());
                            AccountInfo aw = db.iGetAccountByMsisdn(rewards.get(w).getMsisdn(), serviceId);
//                            logger.error("msisdn get: " + aw.getMsisdn());
                            if (aw != null && aw.getMsisdn() != null) {
                                accountWon.add(aw);
                            }
                        }
                    }

                    response.setTopPlayedTime(accountPlayed);
                    response.setTopWonMiniGame(accountWon);

                    // get accounts in top 3 who have highest points

//                    List<AccountInfo> listAccounts = db.getAccounts(msisdn, seqPage, rowsOnPage, serviceId);
//                    if (listAccounts == null || listAccounts.isEmpty()) {
//                        response.setErrorCode(NO_ACCOUNT_EXISTED);
//                    } else {
//                        List<AccountInfo> ranking = new ArrayList<AccountInfo>();
//                        int count = 0;
//                        if (Integer.parseInt(rowsOnPage) < listAccounts.size()) {
//                            count = Integer.parseInt(rowsOnPage);
//                        } else
//                            count = listAccounts.size();
//
//                        for (int i = 0; i < count; i++) {
//                            ranking.add(listAccounts.get(i));
//                        }
//                        // find the position of this msisdn
//                        int pos = 0;
//                        int len = ranking.size();
//                        int i = 0;
//                        while (i < len) {
////                            logger.info("pos " + i);
////                            logger.info("msisdn o " + listAccounts.get(i).getMsisdn() + " n " + msisdn);
//                            ranking.get(i).setRanking(String.valueOf(i));
//                            if (ranking.get(i).getMsisdn().equals(msisdn)) {
//                                pos = i;
//                                ranking.get(i).setRanking(String.valueOf(pos));
//                            }
//                            i++;
//                        }
//                        response.setListAccounts(ranking);
//                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsUpdateUserProfile")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsUpdateUserProfile(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "FullName") String fullName,
            @WebParam(name = "Picture") String picture,
            @WebParam(name = "Birthday") String birthday,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsUpdateUserProfile Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\nfullName:").append(fullName).
                    append("\npicture:").append(picture).
                    append("\nbirthday:").append(birthday).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (picture == null || picture.length() == 0) {
                logger.info("picture is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("picture is null");
            } else if (birthday == null || birthday.length() == 0) {
                logger.info("birthday is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("birthday is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    Boolean res = db.updateUserProfile(msisdn, fullName, picture, birthday, serviceId);
                    if (!res) {
                        response.setErrorCode(FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetUserProfile")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetUserProfile(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetUserProfile Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    AccountInfo account = db.getUserProfile(msisdn, serviceId);
                    if (account == null || account.getMsisdn() == null) {
                        response.setErrorCode(FAILURE);
                    } else {
                        List<AccountInfo> accountInfos = new ArrayList<>();
                        accountInfos.add(account);
                        response.setListAccounts(accountInfos);
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetUserServices")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetUserServices(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetUserServices Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    List<RegisterInfo> listRegs = db.getUserServicesRenew(msisdn, serviceId);
                    List<String> listServices = new ArrayList<>();
                    for (int c = 0; c < listRegs.size(); c++) {
                        logger.info(listRegs.get(c).getProductName());
                        if (listRegs.get(c).getStatus() == 1) {
                            listServices.add(listRegs.get(c).getProductName());
                        }
                    }
                    response.setListServices(listServices);
                    response.setErrorCode(SUCCESS);

//                    if (listRegs == null || listRegs.isEmpty()) {
//                        response.setErrorCode(FAILURE);
//                    } else {
//                        List<String> listServices = new ArrayList<>();
//                        for (int c = 0; c < listRegs.size(); c++) {
//                            if (listRegs.get(c).getStatus() == 1) {
//                                listServices.add(listRegs.get(c).getProductName());
//                            }
//                        }
//                        response.setListServices(listServices);
//                        response.setErrorCode(SUCCESS);
//                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetMiniGames")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetMiniGames(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "ParentId") String parentId,
            @WebParam(name = "IsPlayed") String isPlayed,
            @WebParam(name = "FromDate") String fromDate,
            @WebParam(name = "ToDate") String toDate,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetMiniGames Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nparentId:").append(parentId).
                    append("\nisPlayed:").append(isPlayed).
                    append("\nfromDate:").append(fromDate).
                    append("\ntoDate:").append(toDate).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (parentId == null || parentId.length() == 0) {
                logger.info("parentId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("parentId is null");
            } else if (isPlayed == null || isPlayed.length() == 0) {
                logger.info("isPlayed is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("isPlayed is null");
            } else if (fromDate == null || fromDate.length() == 0) {
                logger.info("fromDate is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("fromDate is null");
            } else if (toDate == null || toDate.length() == 0) {
                logger.info("toDate is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("toDate is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<Game> listGames = db.getMiniGames(parentId, isPlayed, fromDate, toDate, serviceId);
                    response.setListGames(listGames);
                    response.setErrorCode(SUCCESS);
//                    if (listRegs == null || listRegs.isEmpty()) {
//                        response.setErrorCode(FAILURE);
//                    } else {
//                        List<String> listServices = new ArrayList<>();
//                        for (int c = 0; c < listRegs.size(); c++) {
//                            if (listRegs.get(c).getStatus() == 1) {
//                                listServices.add(listRegs.get(c).getProductName());
//                            }
//                        }
//                        response.setListServices(listServices);
//                        response.setErrorCode(SUCCESS);
//                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsGetUsersWonMiniGames")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsGetUsersWonMiniGames(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "FromDate") String fromDate,
            @WebParam(name = "ToDate") String toDate,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetMiniGames Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nfromDate:").append(fromDate).
                    append("\ntoDate:").append(toDate).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (fromDate == null || fromDate.length() == 0) {
                logger.info("fromDate is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("fromDate is null");
            } else if (toDate == null || toDate.length() == 0) {
                logger.info("toDate is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("toDate is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    List<AccountInfo> listAccountWon = db.getAccountByUserWonMiniGame(fromDate, toDate, serviceId);
                    response.setListAccounts(listAccountWon);
                    response.setErrorCode(SUCCESS);
//                    if (listRegs == null || listRegs.isEmpty()) {
//                        response.setErrorCode(FAILURE);
//                    } else {
//                        List<String> listServices = new ArrayList<>();
//                        for (int c = 0; c < listRegs.size(); c++) {
//                            if (listRegs.get(c).getStatus() == 1) {
//                                listServices.add(listRegs.get(c).getProductName());
//                            }
//                        }
//                        response.setListServices(listServices);
//                        response.setErrorCode(SUCCESS);
//                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsUpdateMiniGameUserPlayAnswer")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsUpdateMiniGameUserPlayAnswer(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "MinigameId") String minigameId,
            @WebParam(name = "Msisdn") String msisdn,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetMiniGames Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nminigameId:").append(minigameId).
                    append("\nmsisdn:").append(msisdn).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (minigameId == null || minigameId.length() == 0) {
                logger.info("minigameId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("minigameId is null");
            } else if (msisdn == null || msisdn.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    msisdn = formatMsisdn(msisdn);
                    List<MinigameReward> listRewards = db.getMiniGameRewards(minigameId, serviceId);
                    if (listRewards != null && listRewards.size() >= Common.Constant.MAX_MINIGAME_REWARD) {
                        response.setErrorCode(OUT_OF_LIST_REWARD);
                    } else {
                        // check if this user has already answer this minigame
                        MinigameReward reward = db.getUserReward(minigameId, msisdn, serviceId);
                        if (reward != null && reward.getMsisdn() != null) {
                            response.setErrorCode(Common.ErrorCode.ALREADY_ANSWERED);
                        } else {
                            // update the list of mini game reward
                            Boolean insert = db.insertMiniGameReward(minigameId, msisdn, serviceId);
                            if (!insert) {
                                response.setErrorCode(FAILURE);
                            } else {
                                String enable = db.getConfiguration(Common.Constant.PLAYING_TIMES_ENABLE, Common.Constant.FUNQUIZ_SERVICEID);
                                if (enable.equals("1")) {
                                    // add money for this user
                                    int money = Common.Constant.MONEY_MINIGAME_REWARD;
                                    ws.addMoney(msisdn, money);

                                    ChargeLog chargeLog = new ChargeLog();
                                    chargeLog.setFee(-money);
                                    chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                                    chargeLog.setMsisdn(msisdn);
                                    chargeLog.setDescription("Exchange points to money");
                                    db.iInsertChargeLog(chargeLog);
                                }

                                String message = "";
                                message = MessageResponse.get(Common.Message.MINIGAME_REWARD_SUCCESS,
                                        Common.Message.MINIGAME_REWARD_SUCCESS, logger);

                                SmsMtObj mt = new SmsMtObj();
                                mt.setMsisdn(msisdn);
                                mt.setChannel(Common.CHANNEL);
                                mt.setMessage(message);
                                db.insertMt(mt);
                                response.setErrorCode(Common.ErrorCode.SUCCESS);
                                response.setContent(message);

                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }


    // FOR ADMIN -------------------------------------------------------------------------------------------------------
    @WebMethod(operationName = "wsAdminLogin")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminLogin(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Account") String account,
            @WebParam(name = "Password") String password,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsAdminLogin Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nServiceId:").append(serviceId).
                    append("\nAccount:").append(account);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (account == null || account.length() == 0) {
                logger.info("msisdn is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("msisdn is null");
            } else if (password == null || password.length() == 0) {
                logger.info("password is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("password is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // check login admin
                    Boolean login = db.checkAdminLogin(account, password, serviceId);
                    if (!login) {
                        response.setErrorCode(Common.ErrorCode.ADMIN_LOGIN_FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsAdminAddNewGame")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminAddNewGame(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "ParentId") String parentId,
            @WebParam(name = "Code") String code,
            @WebParam(name = "NameGlobal") String nameGlobal,
            @WebParam(name = "NameLocal") String nameLocal,
            @WebParam(name = "DescriptionGlobal") String descriptionGlobal,
            @WebParam(name = "DescriptionLocal") String descriptionLocal,
            @WebParam(name = "IntroductionGlobal") String introductionGlobal,
            @WebParam(name = "IntroductionLocal") String introductionLocal,
            @WebParam(name = "Icon") String icon,
            @WebParam(name = "Logo") String logo,
            @WebParam(name = "Content") String content,
            @WebParam(name = "ContentType") String contentType,
            @WebParam(name = "Level") String level,
            @WebParam(name = "FromDate") String fromDate,
            @WebParam(name = "ToDate") String toDate,
            @WebParam(name = "Status") String status,
            @WebParam(name = "IsPlayed") String isPlayed,
            @WebParam(name = "TypeOf") String typeOf,
            @WebParam(name = "Note") String note,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsAdminAddNewGame Params:\n").
                    append("WsUser:").append(wsUser).
                    append("parentId:").append(parentId).
                    append("code:").append(code).
                    append("nameGlobal:").append(nameGlobal).
                    append("nameLocal:").append(nameLocal).
                    append("descriptionGlobal:").append(descriptionGlobal).
                    append("descriptionLocal:").append(descriptionLocal).
                    append("introductionGlobal:").append(introductionGlobal).
                    append("introductionLocal:").append(introductionLocal).
                    append("icon:").append(icon).
                    append("logo:").append(logo).
                    append("content:").append(content).
                    append("contentType:").append(contentType).
                    append("fromDate:").append(fromDate).
                    append("toDate:").append(toDate).
                    append("status:").append(status).
                    append("isPlayed:").append(isPlayed).
                    append("typeOf:").append(typeOf).
                    append("note:").append(note).
                    append("\nServiceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (parentId == null || parentId.length() == 0) {
                logger.info("parentId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("parentId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // add to fq_games
                    Game game = new Game();
                    game.setParentId(parentId);
                    game.setDescriptionGlobal(descriptionGlobal);
                    game.setDescriptionLocal(descriptionLocal);
                    game.setNameGlobal(nameGlobal);
                    game.setNameLocal(nameLocal);
                    game.setIntroductionLocal(introductionLocal);
                    game.setIntroductionGlobal(introductionGlobal);
                    game.setLogo(logo);
                    game.setIcon(icon);
                    game.setContentType(contentType);
                    game.setContent(content);
                    game.setCode(code);
                    game.setFromDate(fromDate);
                    game.setToDate(toDate);
                    game.setIsPlayed(isPlayed);
                    game.setLevel(level);
                    game.setStatus(status);
                    game.setTypeOf(typeOf);
                    game.setServiceId(serviceId);
                    game.setNote(note);

                    Boolean add = db.insertNewGame(game);
                    // check add admin
                    if (!add) {
                        response.setErrorCode(Common.ErrorCode.ADMIN_ADD_FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsAdminUpdateNewGame")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminUpdateNewGame(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Id") String id,
            @WebParam(name = "Code") String code,
            @WebParam(name = "NameGlobal") String nameGlobal,
            @WebParam(name = "NameLocal") String nameLocal,
            @WebParam(name = "DescriptionGlobal") String descriptionGlobal,
            @WebParam(name = "DescriptionLocal") String descriptionLocal,
            @WebParam(name = "IntroductionGlobal") String introductionGlobal,
            @WebParam(name = "IntroductionLocal") String introductionLocal,
            @WebParam(name = "Icon") String icon,
            @WebParam(name = "Logo") String logo,
            @WebParam(name = "Content") String content,
            @WebParam(name = "ContentType") String contentType,
            @WebParam(name = "FromDate") String fromDate,
            @WebParam(name = "ToDate") String toDate,
            @WebParam(name = "Status") String status,
            @WebParam(name = "IsPlayed") String isPlayed,
            @WebParam(name = "TypeOf") String typeOf,
            @WebParam(name = "Note") String note,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsAdminUpdateNewGame Params:\n").
                    append("WsUser:").append(wsUser).
                    append("id:").append(id).
                    append("code:").append(code).
                    append("nameGlobal:").append(nameGlobal).
                    append("nameLocal:").append(nameLocal).
                    append("descriptionGlobal:").append(descriptionGlobal).
                    append("descriptionLocal:").append(descriptionLocal).
                    append("introductionGlobal:").append(introductionGlobal).
                    append("introductionLocal:").append(introductionLocal).
                    append("icon:").append(icon).
                    append("logo:").append(logo).
                    append("content:").append(content).
                    append("contentType:").append(contentType).
                    append("fromDate:").append(fromDate).
                    append("toDate:").append(toDate).
                    append("status:").append(status).
                    append("isPlayed:").append(isPlayed).
                    append("typeOf:").append(typeOf).
                    append("note:").append(note).
                    append("\nServiceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (id == null || id.length() == 0) {
                logger.info("id is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("id is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // add to fq_games
                    Game game = new Game();
                    game.setId(id);
                    game.setDescriptionGlobal(descriptionGlobal);
                    game.setDescriptionLocal(descriptionGlobal);
                    game.setNameGlobal(nameGlobal);
                    game.setNameLocal(nameLocal);
                    game.setIntroductionLocal(introductionLocal);
                    game.setIntroductionGlobal(introductionGlobal);
                    game.setLogo(logo);
                    game.setIcon(icon);
                    game.setContentType(contentType);
                    game.setContent(content);
                    game.setCode(code);
                    game.setFromDate(fromDate);
                    game.setToDate(toDate);
                    game.setIsPlayed(isPlayed);
                    game.setStatus(status);
                    game.setTypeOf(typeOf);
                    game.setServiceId(serviceId);
                    game.setNote(note);

                    Boolean update = db.updateGame(game);
                    // check add admin
                    if (!update) {
                        response.setErrorCode(Common.ErrorCode.ADMIN_UPDATE_FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsAdminDeleteGame")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminDeleteGame(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Id") String id,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsAdminDeleteGame Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nServiceId:").append(serviceId).
                    append("\nId:").append(id);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (id == null || id.length() == 0) {
                logger.info("id is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("id is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // check login admin
                    Boolean delete = db.deleteGame(id, serviceId);
                    if (!delete) {
                        response.setErrorCode(Common.ErrorCode.ADMIN_DELETE_FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            String message = MessageResponse.get(Common.Message.SYSTEM_FAIL, logger);
            response.setContent(message);
        }
        return response;
    }

    @WebMethod(operationName = "wsAdminUpdateConfiguration")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminUpdateConfiguration(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "Key") String key,
            @WebParam(name = "Value") String value,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetUserProfile Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nkey:").append(key).
                    append("\nvalue:").append(value).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            } else if (value == null || value.length() == 0) {
                logger.info("value is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("value is null");
            } else if (key == null || key.length() == 0) {
                logger.info("key is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("key is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // update
                    Boolean res = db.updateConfiguration(key, value, serviceId);
                    if (!res) {
                        response.setErrorCode(Common.ErrorCode.ADMIN_UPDATE_FAILURE);
                    } else {
                        response.setErrorCode(SUCCESS);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    @WebMethod(operationName = "wsAdminGetConfiguration")
    @SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
    public Response wsAdminGetConfiguration(
            @WebParam(name = "WsUser") String wsUser,
            @WebParam(name = "WsPass") String wsPass,
            @WebParam(name = "ServiceId") String serviceId) {

        Response response = new Response();
        response.setErrorCode(SUCCESS);

        try {
            String ip = getIpClient();
            UserInfo userInfo = null;
            br.setLength(0);
            br.append("wsGetUserProfile Params:\n").
                    append("WsUser:").append(wsUser).
                    append("\nserviceId:").append(serviceId);
            logger.info(br);

            Request request = new Request();
            request.setUser(wsUser);
//            request.setPass(password);
            if (wsUser == null || wsUser.length() == 0) {
                logger.info("wsUser is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsUser is null");
            } else if (wsPass == null || wsPass.length() == 0) {
                logger.info("wsPass is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("wsPass is null");
            } else if (serviceId == null || serviceId.length() == 0) {
                logger.info("serviceId is null");
                response.setErrorCode(PARAM_NOT_ENOUGH);
                response.setContent("serviceId is null");
            }

            if (SUCCESS.equals(response.getErrorCode())) {
                // check user and password
                userInfo = authenticate(wsUser, wsPass, ip);
                if (userInfo == null || userInfo.getWsUserId() <= 0) {
                    response.setErrorCode(WRONG_PASSWORD);
                    response.setContent("Authenticate fail!");
                } else {
                    // update
                    List<Configuration> configurations = db.getConfiguration(serviceId);
                    if (configurations == null || configurations.isEmpty()) {
                        response.setErrorCode(NO_DATA);
                    } else {
                        response.setErrorCode(SUCCESS);
                        response.setListConfigurations(configurations);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            response.setErrorCode(EXCEPTION);
            response.setContent("Transaction fail");
        }
        return response;
    }

    public void processAfterCharge(String msisdn, int fee) {
        List<RegisterInfo> listReg = db.getRegisterInfoAll(msisdn);
        if (listReg == null || listReg.isEmpty()) {
            // unsub
            int spinNum = (int) (fee / Common.SPIN_FEE);
            // update spin_gift
            List<SpinGift> listSg = db.getSpinGift(msisdn);
            if (listSg != null && listSg.size() > 0) {
                // update spin gift
                SpinGift sg = listSg.get(0);
                sg.setGiftMsisdn(msisdn);
                sg.setNumberSpin(sg.getNumberSpin() + (int) spinNum);
                db.iUpdateSpinGift(sg);
            } else {
                // insert new spin gift
                SpinGift sg = new SpinGift();
                sg.setMsisdn(msisdn);
                sg.setGiftMsisdn(msisdn);
                sg.setNumberSpin(spinNum);
                db.iInsertSpinGift(sg);
            }

            String message = MessageResponse.get(Common.Message.BUY_SPIN_UNSUB_SUCCESS, logger);
            message = message.replace("%fee%", fee + "");
            message = message.replace("%spin%", spinNum + "");
//            SmsMtObj mt = new SmsMtObj();
//            mt.setMsisdn(msisdn);
//            mt.setChannel(Common.CHANNEL);
//            mt.setMessage(message);
//            db.insertMt(mt);

            // update ranking
            updateRanking(msisdn, Common.SubType.UNSUB, spinNum, Common.RankType.BOUGHT, Common.PeriodPrize.WEEKLY);
            updateRanking(msisdn, Common.SubType.UNSUB, spinNum, Common.RankType.BOUGHT, Common.PeriodPrize.MONTHLY);
        } else {
            // for sub
            int spinInt = fee * Common.ADD_SPIN_TIMES / Common.ADD_SPIN_FEE;
            // update spin
            db.updateAddMoreTimes(listReg.get(0).getRegisterId(), spinInt);

            // send message
            String message = MessageResponse.get(Common.Message.BUY_SPIN_SUB_SUCCESS, logger);
            message = message.replaceAll("%fee%", fee + "");
            message = message.replaceAll("%spin%", spinInt + "");
//            SmsMtObj mt = new SmsMtObj();
//            mt.setMsisdn(msisdn);
//            mt.setChannel(Common.CHANNEL);
//            mt.setMessage(message);
//            db.insertMt(mt);

            // update ranking
            updateRanking(msisdn, Common.SubType.SUB, spinInt, Common.RankType.BOUGHT, Common.PeriodPrize.WEEKLY);
            updateRanking(msisdn, Common.SubType.SUB, spinInt, Common.RankType.BOUGHT, Common.PeriodPrize.MONTHLY);
        }
    }

    public RegisterInfo updateRegRenew(RegisterInfo reg, ProductInfo productInfo) {
        reg.setNumberSpin(productInfo.getNumberSpin());
        reg.setExtendStatus(0);
        reg.setPlayedTimes(0);
        reg.setRenew(productInfo.getRenew());
        return reg;
    }

    private String generateValidateCode(int length) {
        String output = "";
        String input = "1234567890";
        Random rnd = new Random();
        for (int i = 0; i < length; i++) {
            output = output + input.charAt(rnd.nextInt(input.length()));
        }
        return output;
    }

    private String formatMsisdn(String msisdn) {
        if (msisdn.startsWith(WebserviceManager.countryCode)) {
            return msisdn;
        } else if (msisdn.startsWith("0")) {
            return WebserviceManager.countryCode + msisdn.substring(1);
        }
        return WebserviceManager.countryCode + msisdn;
    }

    private void updateRanking(String msisdn, int subType, int count, int rankType, int period) {
        List<RankingObj> listRank = db.getRankObj(msisdn, rankType, period);
        if (listRank != null && listRank.size() > 0) {
            // update
            db.iUpdateRanking(listRank.get(0).getId(), subType, count);
        } else {
            // insert
            Calendar currentTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
            endTime.set(Calendar.HOUR_OF_DAY, 20);
            endTime.set(Calendar.MINUTE, 0);
            endTime.set(Calendar.SECOND, 0);
            endTime.set(Calendar.MILLISECOND, 0);

            if (currentTime.after(endTime)) {
                endTime.add(Calendar.DAY_OF_MONTH, 1);
            }

            Calendar startTime = Calendar.getInstance();
            startTime.setTimeInMillis(endTime.getTimeInMillis() - 86400000L + 1000);

            RankingObj ranking = new RankingObj();
            ranking.setMsisdn(msisdn);
            ranking.setSpintCount(count);
            ranking.setStartTime(new Timestamp(startTime.getTimeInMillis()));
            ranking.setEndTime(new Timestamp(endTime.getTimeInMillis()));
            ranking.setSubType(subType);
            ranking.setRankType(rankType);

            if (period == Common.PeriodPrize.DAILY) {
                db.iInsertRanking(ranking);
            } else if (period == Common.PeriodPrize.WEEKLY) {
                db.iInsertRankingWeekly(ranking);
            }
            if (period == Common.PeriodPrize.MONTHLY) {
                db.iInsertRankingMonth(ranking);
            }
        }
    }

    private PointTotalObj updatePoint(String msisdn, int point, int type) {
        List<PointTotalObj> listPoint = db.getPointTotal(msisdn);
        if (listPoint != null && listPoint.size() > 0) {
            PointTotalObj newPoint = listPoint.get(0);
            // update
            logger.info("Update point: id " + newPoint.getId() + ", msisdn " + msisdn + ", point " + point);
            if (type == 1) {
                // add
                newPoint.setAddedPoint(point);
                db.iUpdatePointTotal(newPoint.getId(), point);
                newPoint.setPoint(newPoint.getPoint() + point);
            } else if (type == 2) {
                // multiple
                newPoint.setAddedPoint(newPoint.getPoint() * (point - 1));
                db.iUpdatePointTotal(newPoint.getId(), newPoint.getPoint() * (point - 1));
                newPoint.setPoint(newPoint.getPoint() * point);
            }
            return newPoint;
        } else {
            // insert
            PointTotalObj pointObj = new PointTotalObj();
            pointObj.setMsisdn(msisdn);
            if (type == 1) {
                // added
                pointObj.setAddedPoint(point);
                pointObj.setPoint(point);
            } else {
                // multiple
                pointObj.setAddedPoint(0);
                pointObj.setPoint(0);
            }
            pointObj.setSubType(0);
            db.iInsertPointTotal(pointObj);
            return pointObj;
        }
    }

    private ProductInfo getProductByName(String packName) {
        logger.info(Common.listProduct);
        for (ProductInfo product : Common.listProduct) {
            logger.info(product.getProductName());
            logger.info("fee " + product.getFee());
//            logger.info("packName: " + packName);
            if (product.getProductName().equalsIgnoreCase(packName)) {
                return product;
            }
        }
        return null;
    }

    private Timestamp getExpireTime(ProductInfo productInfo) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, productInfo.getExpireDays() - 1);
        return new Timestamp(cal.getTimeInMillis());
    }

    protected Timestamp getExpireTimeInDay(int numDay) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, numDay - 1);
        return new Timestamp(cal.getTimeInMillis());
    }

    protected Timestamp getExactTimeInDay(int second, int minute, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    protected Timestamp getExactTimeInDay(int second, int minute, int hour, int numday) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, numday);
        return new Timestamp(cal.getTimeInMillis());
    }


    private Timestamp getTruncSysdate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    private String getHashCode(String securityCode, String salt, Logger logger) {
        MessageDigest md = null;
        try {
            String seq = salt + securityCode;
            md = MessageDigest.getInstance("SHA-256"); //step 2
            md.update(seq.getBytes("UTF-8")); //step 3
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
        byte raw[] = md.digest(); //step 4
        String hash = (new BASE64Encoder()).encode(raw); //step 5
        return hash;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//        MessageDigest md = null;
//        String seq = "123456a@" + "32323232";
//        md = MessageDigest.getInstance("SHA-256"); //step 2
//        md.update(seq.getBytes("UTF-8")); //step 3

//        byte raw[] = md.digest(); //step 4
//        String hash = (new BASE64Encoder()).encode(raw); //step 5
//        System.out.println("hash" + hash);
//        String pass = "1QAaz@123@";
//        Pattern pattern = Pattern.compile("abcdefghijklmnopqrstuvwxyz");
        //boolean match = list.matches(".");
//        System.out.println("hash " + pass.matches(".*[A-Z].*"));
//        String output = "";
//        String input = "1234567890";
//        Random rnd = new Random();
//        for (int i = 0; i < 8; i++) {
//            output = output + input.charAt(rnd.nextInt(input.length()));
//        }
//        System.out.println("output = " + output);
//        double x = 1.1;
//        System.out.println("output = " + (((x * 3 / 2) * 100) / 100));
//        System.out.println("output = " + (x * 3 / 2));
        int point = Integer.parseInt("116000.0");
        int money = point / 10;
        System.out.println("Poinnt " + point + ", money: " + money);
    }

    @Override
    public UserInfo authenticate(String userName, String password, String ipAddress) {
        // validate thong tin user
        // validate thong tin user
        UserInfo userInfo = db.iGetUser(userName);
        if (userInfo == null || userInfo.getWsUserId() <= 0) {
            logger.info("User not existed: " + userName);
            userInfo = new UserInfo();
            userInfo.setWsUserId(-1);
            return userInfo;
        }

        if (userInfo.getIp() != null && userInfo.getIp().length() > 0) {
            // Check IP
            String ip[] = userInfo.getIp().split(",");
            boolean pass = false;
            for (String ipConfig : ip) {
                if (pair(ipAddress, ipConfig.trim())) {
                    pass = true;
                    break;
                }
            }
            if (!pass) {
                logger.info("IP address not allowed: " + ipAddress);
                userInfo.setWsUserId(-2);
                return userInfo;
            }
        }
        // Check password
        if (userInfo.getPass() != null && userInfo.getPass().trim().length() > 0) {
            if (password.equals(userInfo.getPass())) {
                return userInfo;
            }

            String passEncript = "";
            passEncript = Encrypt.getHashCode(userName, password, logger);
            if (passEncript.equals(userInfo.getPass())) {
                return userInfo;
            }
            logger.info("Password incorrect: user=" + userName);
            userInfo.setWsUserId(-3);
            return userInfo;
        }
        return null;//tam fix
    }
}
