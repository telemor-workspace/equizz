package com.vas.wsfw.services;

import java.sql.Timestamp;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import com.vas.webservices.WsFunQuiz;
import com.vas.wsfw.common.Common;
import com.vas.wsfw.common.Encrypt;
import com.vas.wsfw.common.MessageResponse;
import com.vas.wsfw.obj.*;
import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;
import com.vas.wsfw.common.WebserviceAbstract;

import java.util.Date;

import static java.util.concurrent.TimeUnit.*;

public class Trigger extends WsFunQuiz {

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    public Trigger() throws Exception {
    }

    public void beepForAnHour() {
        final Runnable beeper = new Runnable() {
            public void run() {
//                System.out.println("beep");
                // check time
//                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
//                LocalDateTime now = LocalDateTime.now();
//                System.out.println(dtf.format(now));

//                int hour = now.getHour();

                Date date = new Date();
                int hour = date.getHours();
//                System.out.println(hour);
                logger.error("Loop playing times " + hour);
                // set condition to add money day by day
                // get playing time history by time in 1 -> 2 o'clock
                String enable = db.getConfiguration(Common.Constant.PLAYING_TIMES_ENABLE, Common.Constant.FUNQUIZ_SERVICEID);
                String timeAddMoney = db.getConfiguration(Common.Constant.TIME_ADD_MONEY, Common.Constant.FUNQUIZ_SERVICEID);
                String timeSendMT = db.getConfiguration(Common.Constant.TIME_SEND_MT, Common.Constant.FUNQUIZ_SERVICEID);
//                System.out.println(enable);
//                System.out.println(timeAddMoney);
//                System.out.println(timeSendMT);
                try {
                    if (hour == Integer.parseInt(timeAddMoney)) {
                        if (enable.equals("1")) {
                            //check da tra thuong chua
                            logger.error("Check add money, playing times reward is enable");
                            List<PlayingTimeHistory> playingTimeHistories = db.getPlayedTimeHistory(Common.Constant.FUNQUIZ_SERVICEID);
                            if (playingTimeHistories == null || playingTimeHistories.size() == 0) {
                                logger.error("Insert new history playing times");
                                // insert
                                db.insertPlayingTimesHistory(Common.Constant.FUNQUIZ_SERVICEID);
                                AddMoney();
                            } else {
                                // check day
                                Timestamp updateDate = playingTimeHistories.get(0).getUpdateDate();
                                Timestamp checkDate = getExactTimeInDay(0, 0, Common.Constant.HOUR_ADD_MONEY_HIGHEST_PLAYING_TIMES);
                                logger.error(updateDate);
                                logger.error(checkDate);
                                if (checkDate.after(updateDate)) {
                                    logger.error("Update old history playing times");

                                    // checkUpdate > updateDate
                                    AddMoney();

                                    // update playing times history
                                    db.updatePlayingTimesHistory(playingTimeHistories.get(0).getId());
                                } else {
                                    logger.error("Not add money, added before");
                                }
                            }
                        }
                        // reset total spin and last update time if old last update time is invalid
                        Timestamp lastUpdate = getExpireTimeInDay(1);
                        Boolean reset = db.resetSpins(Common.Constant.FUNQUIZ_SERVICEID, lastUpdate);
                        if (!reset) {
                            logger.error("Can not reset spin for all user");
                        }
                    } else if (hour == Integer.parseInt(timeSendMT)) {
                        if (enable.equals("1")) {
                            logger.error("Send MT playing times, playing times reward is enable");
                            // get highest account at last time
                            List<PlayingTimeRewrad> playingTimeRewrads = db.getHighestPlayedTime(Common.Constant.FUNQUIZ_SERVICEID);
                            if (playingTimeRewrads != null && playingTimeRewrads.size() > 0) {
                                logger.debug("Send MT playing times for each account");
                                for (int a = 0; a < playingTimeRewrads.size(); a++) {
                                    String msisdn = playingTimeRewrads.get(a).getMsisdn();

                                    String message = "";
                                    message = MessageResponse.get(Common.Message.PLAYING_TIMES_REWARD,
                                            Common.Message.PLAYING_TIMES_REWARD, logger);
                                    SmsMtObj mt = new SmsMtObj();
                                    mt.setMsisdn(msisdn);
                                    mt.setChannel(Common.CHANNEL);
                                    mt.setMessage(message);
                                    db.insertMt(mt);
                                }
                                // delete
                                db.deleteHighestPlayedTime(Common.Constant.FUNQUIZ_SERVICEID);
                            }
                        }
                    } else {
                        logger.error("Playing times continues");
                    }
                } catch (Exception e) {
                    logger.error("Error processing", e);
                }
            }
        };
        final ScheduledFuture<?> beeperHandle =
                scheduler.scheduleAtFixedRate(beeper, 0, 1, HOURS);
//        scheduler.schedule(new Runnable() {
//            public void run() {
//                beeperHandle.cancel(true);
//            }
//        }, 2 * 60, SECONDS);
    }

    public Boolean AddMoney() {
        try {
            // get 10 account which have highest playing times
            logger.error("add money");

            List<AccountInfo> accountInfos = db.getAccountsByHighestPlayedTime(10, Common.Constant.FUNQUIZ_SERVICEID);
            for (int i = 0; i < accountInfos.size(); i++) {
                String msisdn = accountInfos.get(i).getMsisdn();
                // add money for this user
                int money = Common.Constant.MONEY_HIGHEST_PLAYING_TIMES_REWARD;
                ws.addMoney(msisdn, money);

                ChargeLog chargeLog = new ChargeLog();
                chargeLog.setFee(-money);
                chargeLog.setChargeTime(new Timestamp(System.currentTimeMillis()));
                chargeLog.setMsisdn(msisdn);
                chargeLog.setDescription("Add money to top 10 highest playing times");
                db.iInsertChargeLog(chargeLog);

                // save to datebase to log this user has already added money to inform
                db.insertHighestPlayingTimes(msisdn, accountInfos.get(i).getTotalSpin(), accountInfos.get(i).getServiceId());
            }
        } catch (Exception e) {
            logger.error("Error processing", e);
            return false;
        }
        return true;
    }

    @Override
    public UserInfo authenticate(String userName, String password, String ipAddress) {
        UserInfo userInfo = db.iGetUser(userName);
        if (userInfo == null || userInfo.getWsUserId() <= 0) {
            logger.info("User not existed: " + userName);
            userInfo = new UserInfo();
            userInfo.setWsUserId(-1);
            return userInfo;
        }

        if (userInfo.getIp() != null && userInfo.getIp().length() > 0) {
            // Check IP
            String ip[] = userInfo.getIp().split(",");
            boolean pass = false;
            for (String ipConfig : ip) {
                if (pair(ipAddress, ipConfig.trim())) {
                    pass = true;
                    break;
                }
            }
            if (!pass) {
                logger.info("IP address not allowed: " + ipAddress);
                userInfo.setWsUserId(-2);
                return userInfo;
            }
        }
        // Check password
        if (userInfo.getPass() != null && userInfo.getPass().trim().length() > 0) {
            if (password.equals(userInfo.getPass())) {
                return userInfo;
            }

            String passEncript = "";
            passEncript = Encrypt.getHashCode(userName, password, logger);
            if (passEncript.equals(userInfo.getPass())) {
                return userInfo;
            }
            logger.info("Password incorrect: user=" + userName);
            userInfo.setWsUserId(-3);
            return userInfo;
        }
        return null;//tam fix
    }
}
